CFLAGS		+= --sysroot ${AGL_TOP}/build/tmp/sysroots/porter -Wall -I. -w -lpthread -lstdc++ -mfloat-abi=hard -O2

CC		:= ${CROSS_COMPILE}gcc
INCLUDE_PATHS = ${INCLUDE_PATH} -I./ABI-IPC -I./K2LABI/ABI -I./K2LABI/Common -I./K2LABI/Common/FlowDecoder -I./K2LABI/Common/SystemUtilities -I./K2LABI/IPC

# C source files
CSRC = \
	./ABI-IPC/abi-ipc.cpp							\
	./K2LABI/ABI/ABI.cpp							\
	./K2LABI/ABI/ABICommands.cpp					\
	./K2LABI/ABI/ABICommandsEx.cpp					\
	./K2LABI/ABI/ABIFilter.cpp						\
	./K2LABI/ABI/Ipc.cpp							\
	./K2LABI/ABI/K2LABI.cpp							\
	./K2LABI/ABI/ResultParser.cpp					\
	./K2LABI/Common/windows-adapter.cpp				\
	./K2LABI/Common/FlowDecoder/FlowDecoder.cpp		\
	./K2LABI/IPC/IPCFlowDecoder.cpp					\
	./K2LABI/IPC/K2LIPC.cpp							\
	./main.cpp

all:
	$(CC) -o OptoLyzerMoccaApp $(CFLAGS) $(PROJECT_C_FLAGS) $(CSRC) $(INCLUDE_PATHS) $(LD_LIBRARY_PATH) $(-Wall)

clean:
	rm -f `find -name '*.o'`
	rm -f `find -name '*.*~'`
	rm -f `find -name '*~'`
	rm -f `find -name '*.a'`
	rm -f `find -name '*.out'`
	rm -f `find -name '*.cmd'`
	rm -f `find -name '*.order'`
	rm -f `find -name '*.orig'`
	rm -f `find -name '*.tmp'`

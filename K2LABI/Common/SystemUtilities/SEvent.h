#ifndef S_EVENT_H
#define S_EVENT_H

#include "k2l-type.h"
#include <pthread.h>
#include <errno.h>
#include <stdio.h>

//-----------------------------------------------------------------------------
enum
{
    ERR_EVENT_BASE            = -56700,
    ERR_EVENT_ALREADY_CREATED = ERR_EVENT_BASE - 1,
    ERR_EVENT_INIT            = ERR_EVENT_BASE - 2,
    ERR_EVENT_TIMEOUT         = ERR_EVENT_BASE - 4,
};

//-----------------------------------------------------------------------------
class CEvent
{
public:
    CEvent(bool bCreate = false);
    ~CEvent();

    int create();
    int close();
    int wait(); // infinite
    int waitMsec   (long lMsecs);
    int waitSeconds(long lSeconds);
    int signal();
    int reset();

private:
    bool m_created;
    pthread_mutex_t m_mutex;
};

//-----------------------------------------------------------------------------
inline CEvent::CEvent(bool bCreate) : m_created(bCreate)
{
    if (bCreate)
        create();
}

//-----------------------------------------------------------------------------
inline CEvent::~CEvent()
{
    close();
}

//-----------------------------------------------------------------------------
inline int CEvent::create()
{
    if (m_created)
        return ERR_EVENT_ALREADY_CREATED;
    
    int err = pthread_mutex_init(&m_mutex, NULL);
    
    //Lock the mutex, so the next locking attempt will block
    if (!err)
    {
        err = pthread_mutex_lock (&m_mutex);
    }
    if (!err)
    {
        m_created = true;
    }
    else
    {
        fprintf(stderr, "Failed to create CEvent, last err: %d\n", err);
    }
    
    return err;
}

//-----------------------------------------------------------------------------
inline int CEvent::close()
{
    if (!m_created)
        return ERR_EVENT_INIT;

    
    return pthread_mutex_destroy (&m_mutex);
}

//-----------------------------------------------------------------------------
inline int CEvent::wait() // infinite
{
    return pthread_mutex_lock (&m_mutex);
}

//-----------------------------------------------------------------------------
inline int CEvent::waitMsec(long lMsecs)
{
    if (!m_created)
        return ERR_EVENT_INIT;
    
    // See: http://www.ibm.com/developerworks/linux/library/l-ipc2lin3/index.html
    struct timespec delay;  // structure for providing timeout
    long timeout = 0;
    int err = 0;
    while (timeout < lMsecs )
    {
        delay.tv_sec = 0;
        delay.tv_nsec = 1000000;  // 1 milli sec delay
        err = pthread_mutex_trylock(&m_mutex);
        if (!err)
        {
            break;
        }
        else {
            // check whether somebody else has the mutex
            if (err == EBUSY ) {
                // Yes, Resource already in use so sleep
                nanosleep(&delay, NULL);
                ++timeout;
            }
            else
            {
                fprintf(stderr, "Unexpected return value from pthread_mutex_trylock: %d \n", err);
            }
        }
    }
return err;
}

//-----------------------------------------------------------------------------
inline int CEvent::waitSeconds(long lSeconds)
{
    DWORD dwMsecs = DWORD(lSeconds) * 1000;
    return waitMsec(dwMsecs);
}

//-----------------------------------------------------------------------------
inline int CEvent::signal()
{
    if (!m_created)
        return ERR_EVENT_INIT;

    return pthread_mutex_unlock(&m_mutex);
}

//-----------------------------------------------------------------------------
inline int CEvent::reset()
{
    if (!m_created)
        return ERR_EVENT_INIT;

    return pthread_mutex_unlock(&m_mutex);
}

#endif  // S_EVENT_H

#ifndef S_LOCK_H
#define S_LOCK_H

#include "windows-adapter.h"

//-----------------------------------------------------------------------------
class CLock
{
public:
    //-----------------------------------------------------------------------------
    CLock(bool bCreate = true)
    {
        m_bCreated = false;
        if (bCreate)
        {
            create();
        }
    }
    //-----------------------------------------------------------------------------
    ~CLock()
    {
        destroy();
    }
    //-----------------------------------------------------------------------------
    virtual int create()
    {
        ::InitializeCriticalSection(&m_CS);
        m_bCreated = true;
        return 0;
    }
    //-----------------------------------------------------------------------------
    virtual int destroy()
    {
        if (m_bCreated)
        {
            ::DeleteCriticalSection(&m_CS);
            m_bCreated = false;
        }
        return 0;
    }
    //-----------------------------------------------------------------------------
    virtual int lock()
    {
        ::EnterCriticalSection(&m_CS);
        return 0;
    }
    //-----------------------------------------------------------------------------
    virtual int unlock()
    {
        ::LeaveCriticalSection(&m_CS);
        return 0;
    }
    //-----------------------------------------------------------------------------
private:
    CRITICAL_SECTION m_CS;
    bool m_bCreated;
};
//-----------------------------------------------------------------------------
class CSafeLock
{
public:
    CSafeLock(CLock* p) : m_pLock(p)
    {
        m_pLock->lock();
    }

    ~CSafeLock()
    {
        m_pLock->unlock();
        m_pLock = 0;
    }

private:
    CLock* m_pLock;
};
//-----------------------------------------------------------------------------

#endif // S_LOCK_H


#ifndef __FLOW_DECODER_H__
#define __FLOW_DECODER_H__

#include "IFlowDecoder.h"


class CFlowDecoder : public IFlowDecoder
{
public:
    CFlowDecoder();
    virtual ~CFlowDecoder() {}

    // Gets called to submit a new package to buffer
    void            DecodeFlow(BYTE* pBuffer, unsigned long nLen);
    virtual void    Reset();

protected:
    void            InitializeBuffer(BYTE* pBuffer, unsigned long nLen);
    unsigned long   GetNumberOfBytesInBuffer() const;
    unsigned long   GetNumberOfMemoryChunks() const;
    bool            GetMemoryChunk(unsigned long nNum, BYTE*& pReadPointer, unsigned long& nBytesAvailable) const;
    bool            AddBytesToBuffer(BYTE* pBuffer, unsigned long nLen);
    bool            CopyFromBuffer(BYTE* pTarget, unsigned long len) const;
    bool            RemoveBytesFromBuffer(unsigned long len);

protected:
    enum eStateRetVal
    {
        E_STATE_NEEDS_MORE_DATA,
        E_STATE_GOT_WRONG_DATA,
        E_STATE_OK,
        E_STATE_UNDEFINED
    };

    bool m_nSynchronized;

    virtual eStateRetVal ReceiveHeader() = 0;
    virtual eStateRetVal ReceivePacket() = 0;

private:
    enum eDecoderState
    {
        E_WAITING_FOR_HEADER,
        E_WAITING_FOR_PACKET_DATA,
    };

    unsigned long   m_nBytesInBuffer;
    BYTE*           m_pCurrentReadPointer;
    BYTE*           m_pBufferStart;
    BYTE*           m_pBufferEnd;
    eDecoderState   m_DecoderState;
};

#endif // __FLOW_DECODER_H__

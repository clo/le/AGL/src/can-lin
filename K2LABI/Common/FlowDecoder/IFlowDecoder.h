#ifndef __IFLOWDECODER_H__
#define __IFLOWDECODER_H__

#include "k2l-type.h"


class IFlowDecoder
{
public:
    IFlowDecoder() {}
    virtual ~IFlowDecoder() {}

    virtual void DecodeFlow(BYTE* pBuffer, unsigned long nLen) = 0;
};

#endif // __IFLOWDECODER_H__

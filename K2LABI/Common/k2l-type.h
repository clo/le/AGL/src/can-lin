//
//  k2l-type.h
//  AmbientLight
//
//  Created by Thorsten Kummermehr on 10/9/13.
//
//
#ifndef AmbientLight_k2l_type_h
#define AmbientLight_k2l_type_h


#include <stdint.h>
#include <stddef.h>

#define K2LABI_API

typedef uint8_t BYTE;
typedef uint16_t WORD;
typedef uint32_t DWORD;
typedef uint32_t HRESULT;

typedef char TCHAR;

typedef void* HANDLE;

#endif

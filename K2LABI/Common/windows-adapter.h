//
//  windows-adapter.h
//  AmbientLight
//
//  Created by Thorsten Kummermehr on 10/9/13.
//
//

#ifndef AmbientLight_windows_adapter_h
#define AmbientLight_windows_adapter_h

#include "k2l-type.h"
#include <pthread.h>

typedef struct
{
    pthread_mutex_t m_mutex;
} CRITICAL_SECTION;

typedef CRITICAL_SECTION * LPCRITICAL_SECTION;

void InitializeCriticalSection( LPCRITICAL_SECTION lpCriticalSection );
void DeleteCriticalSection(LPCRITICAL_SECTION lpCriticalSection);
void EnterCriticalSection(LPCRITICAL_SECTION lpCriticalSection);
void LeaveCriticalSection(LPCRITICAL_SECTION lpCriticalSection);

#endif

//
//  windows-adapter.cpp
//  AmbientLight
//
//  Created by Thorsten Kummermehr on 10/9/13.
//
//

#include "windows-adapter.h"

void InitializeCriticalSection( LPCRITICAL_SECTION lpCriticalSection )
{
    if (NULL == lpCriticalSection)
        return;
    pthread_mutexattr_t mutexattr;
    
    // Set the mutex as a recursive mutex
    pthread_mutexattr_settype(&mutexattr, PTHREAD_MUTEX_RECURSIVE);
    
    pthread_mutex_init(&lpCriticalSection->m_mutex, &mutexattr);
    
    // destroy the attribute
    pthread_mutexattr_destroy(&mutexattr);

}

void DeleteCriticalSection(LPCRITICAL_SECTION lpCriticalSection)
{
    if (NULL == lpCriticalSection)
        return;
    pthread_mutex_destroy (&lpCriticalSection->m_mutex);
}

void EnterCriticalSection(LPCRITICAL_SECTION lpCriticalSection)
{
    pthread_mutex_lock (&lpCriticalSection->m_mutex);
}

void LeaveCriticalSection(LPCRITICAL_SECTION lpCriticalSection)
{
    pthread_mutex_unlock (&lpCriticalSection->m_mutex);
}
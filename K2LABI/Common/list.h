#ifndef ILIST_H_
#define ILIST_H_

template<class T>

class IList
{
public:
    class IIterator
    {
    public:
        virtual ~IIterator(){}
        virtual bool hasNext() = 0;
        virtual T* next() = 0;
        virtual void release() = 0;
    };
    virtual ~IList(){}

    virtual IIterator* getIterator() = 0;
    virtual int add(T* pItem) = 0;
    virtual T* getAt(int nIndex) = 0;
    virtual void clear(bool bDelete = true, bool bArr = false) = 0;
    virtual bool isEmpty() = 0;
};

#endif
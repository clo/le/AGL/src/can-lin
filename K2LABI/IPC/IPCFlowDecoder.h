#ifndef __IPC_FLOW_DECODER_H__
#define __IPC_FLOW_DECODER_H__

#include "FlowDecoder.h"
#include "K2LIPC.h"

using namespace K2L::Automotive::IPC;
class CIpc;

class CIPCFlowDecoder : public CFlowDecoder
{
public:
    CIPCFlowDecoder(CIpc* pIpc);
    ~CIPCFlowDecoder();
    virtual void         Reset();

protected:
    eStateRetVal ReceiveHeader();
    eStateRetVal ReceivePacket();

private:
    BYTE*           m_pPacket;
    unsigned long   m_ReceiveIndex;
    int             m_ValidPacketCount;
    CIpc*           m_pIpc;
};

#endif // __IPC_FLOW_DECODER_H__

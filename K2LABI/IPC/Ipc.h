#pragma once
#include "K2LIPC.h"
#include "SLock.h"

using namespace K2L::Automotive::IPC;

class CIpc : public IIpc
{
public:
    CIpc();
    virtual ~CIpc();

    int             Init(IIPCListener* pListener);
    int             Deinit();
    int             FormatRequest(BYTE channel, BYTE ifaceId, BYTE funId, DWORD dwSenderID, const BYTE *pData, DWORD dataLen, BYTE* pResult, int nResultLen, int* nResultLenNeeded);
    IIPCListener*   GetListener();
    void            OnPacket(CIpcPacket* pPacket);
    void            Decode(BYTE* pBuffer, DWORD nLen);
    IIPCListener*   CreateListener(FCT_OnMessageEvent callback, FCT_OnEvent error, FCT_OnEvent overflow);
    void            DestroyListener();

private:
    CIPCFlowDecoder* m_pFlowDecoder;
    BYTE*            m_pResultBuffer;
    int              m_nResultLen;
    int              m_errorCode;
    IIPCListener*    m_pIpcListener;
    CLock            m_lock;
};
#include "windows-adapter.h"
#include "K2LIPC.h"
#include "IPCFlowDecoder.h"
#include "Ipc.h"

IIpc* CIpcFactory::CreateIpc()
{
    return new CIpc();
}

//-----------------------------------------------------------------------------
void CIpcFactory::DestroyIpc(IIpc* pIpc)
{
    delete pIpc;
}

//-----------------------------------------------------------------------------
K2LABI_API IPC_HANDLE CreateIPC()
{
    CIpc* pIpc = new CIpc;
    return reinterpret_cast<IPC_HANDLE>(pIpc);
}

//-----------------------------------------------------------------------------
K2LABI_API void DestroyIPC(IPC_HANDLE hIpc)
{
    CIpc* pIpc = reinterpret_cast<CIpc*>(hIpc);
    delete pIpc;
}

//-----------------------------------------------------------------------------
K2LABI_API int IPCInit(IPC_HANDLE hIpc, FCT_OnMessageEvent callback, FCT_OnEvent error, FCT_OnEvent overflow)
{
    CIpc* pIpc = reinterpret_cast<CIpc*>(hIpc);
    if (!pIpc)
        return -1;

    IIPCListener* pListener = pIpc->CreateListener(callback, error, overflow);
    return pIpc->Init(pListener);
}

//-----------------------------------------------------------------------------
K2LABI_API int IPCDeinit(IPC_HANDLE hIpc)
{
    CIpc* pIpc = reinterpret_cast<CIpc*>(hIpc);
    if (!pIpc)
        return -1;
    
    pIpc->DestroyListener();
    return pIpc->Deinit();
}

//-----------------------------------------------------------------------------
K2LABI_API int IPCFormatRequest(IPC_HANDLE hIpc, BYTE channel, BYTE ifaceId, BYTE funId, DWORD dwSenderID, const BYTE *pReqData, DWORD dataLen, BYTE* pResult, int nResultLen, int* nNeededResultLen)
{
    CIpc* pIpc = reinterpret_cast<CIpc*>(hIpc);
    if (!pIpc)
        return -1;

    return pIpc->FormatRequest(channel, ifaceId, funId, dwSenderID, pReqData, dataLen, pResult, nResultLen, nNeededResultLen);
}

//-----------------------------------------------------------------------------
K2LABI_API void IPCDecode(IPC_HANDLE hIpc, BYTE* pData, int nDataLen)
{
    CIpc* pIpc = reinterpret_cast<CIpc*>(hIpc);

    if (pIpc)
        pIpc->Decode(pData, nDataLen);
}

/*=== K2L GmbH ===============================================================*/
/**
\file
            This file contains the API of the \ref IPC interface.

\ingroup    IPC

\par        COPYRIGHT (c) 2000-2011 by K2L GmbH
            All rights reserved.
*/
/*==============================================================================
            Alle Rechte an der Software verbleiben bei der K2L GmbH.

            Die K2L GmbH raeumt dem Lizenznehmer nach vollstaendiger
            Bezahlung der Lizenzgebuehr ein nicht ausschliessliches,
            unwiderrufliches, nicht uebertragbares, zeitlich und geografisch
            nicht beschraenktes Recht zur Nutzung der jeweils
            auftragsgegenstaendlichen Software innerhalb des Lizenznehmers ein.

            Die Weitergabe des Nutzungsrechts durch den Lizenznehmer ist auf
            dessen Zulieferer beschraenkt. Die Zulieferer sind zu verpflichten,
            die Software nur im Rahmen von Projekten fuer den Lizenznehmer
            zu verwenden; weitere Rechte der Zulieferer sind auszuschliessen.
===HEADER END=================================================================*/
//@{

#pragma once

/// \defgroup IPC Interprocess communication
/// The IPC group ...
namespace K2L
{
    namespace Automotive
    {
        namespace IPC
        {

class IIpc;

/// Identifies the head of the packet.
#define ABI_IPC_PACKET_HEAD     0x04

/// Identifies the IPC channel 7.
#define ABI_IPC_CHANNEL7        0x07

/// Identifies the IPC channel 8.
#define ABI_IPC_CHANNEL8        0x08

#pragma pack(push, 1)
/// \brief Class holding all informations to create a IPC message.
/// \ingroup IPC
class CIpcPacket
{
public:
    /// The header of the IPC packet.
    BYTE m_head;

    /// The channel of the IPC packet.
    BYTE m_channel;

    /// The length of the IPC packet.
    DWORD m_length;

    /// The error code of the IPC packet.
    BYTE m_errorCode;

    /// Checksum of the IPC header.
    BYTE m_hdrCrc;
};
#pragma pack(pop)

#pragma pack(push, 1)
/// \brief Class holding all informations to create a IPC request.
/// \ingroup IPC
class CRequest
{
public:
    /// Message processing group in the ABI device.
    BYTE m_interface;

    /// Function id of the request.
    BYTE m_function;

    /// Request Id.
    DWORD m_requestId;
};
#pragma pack(pop)

/*============================================================================*/
/// \brief Interface of IPC event listener.
/// \note Abstract base class. So it has to be implemented.
/// \ingroup IPC
/*============================================================================*/
class K2LABI_API IIPCListener
{
public:
    virtual ~IIPCListener() {}

    /// Called if packet arrived.
    /// \param[in] pIpc Pointer to IPC that has occured event.
    /// \param[in] pPacket Pointer to data package.
    /// \param[in] nLen Length of package.
    virtual void OnMessage(IIpc* pIpc, BYTE channel, BYTE errorCode, BYTE* pPacket, int nLen) = 0;

    /// Called if error occured.
    /// \param[in] pIpc Pointer to IPC that has occured event.
    virtual void OnError(IIpc* pIpc) = 0;
    
    /// Called if message lost caused by an overflow.
    /// \param[in] pIpc Pointer to IPC that has occured event.
    virtual void OnMessageLost(IIpc* pIpc) = 0;
};

/*============================================================================*/
/// \brief Interface of interprocess connection.
/// \ingroup IPC
/*============================================================================*/
class K2LABI_API IIpc
{
public:
    virtual ~IIpc() {}

    /// Initializes IPC connection.
    /// \param[in] Device to connect.
    /// \param[in] Event listener.
    /// \return 0 if operation successful.
    /// \return !0 if operation failed.
    virtual int Init(IIPCListener* pListener) = 0;

    /// Deinitializes IPC connection.
    /// \return 0 if operation successful.
    /// \return !0 if operation failed.
    virtual int Deinit() = 0;

    /// Sends a request to the device.
    /// \param[in] Namespace ID of command.
    /// \param[in] Function ID of command.
    /// \param[in] Pointer to data that should be send.
    /// \param[in] Length of data.
    /// \param[out] Length of result.
    /// \param[out] Error code of operation.
    /// \return Response of request. NULL if no response returned.
    virtual int FormatRequest(BYTE channel, BYTE ifaceId, BYTE funId, DWORD dwSenderID, const BYTE *pData, DWORD dataLen, BYTE* pResult, int nResultLen, int* nResultLenNeeded) = 0;
    
    /// Decodes a IPC message from the buffer.
    /// \param[in] pPipe Pipe to check if an overflow occurr.
    /// \param[in] pBuffer Pointer to the first byte of message that should be decoded.
    /// \param[in] nLen Length of message.
    virtual void Decode(BYTE* pBuffer, DWORD nLen) = 0;

    /// Gets a pointer to the registered event listener.
    /// \return Pointer to event listener.
    virtual IIPCListener* GetListener() = 0;
};

/*============================================================================*/
/// \brief Factory to create IPC connections.
/// \ingroup IPC
/*============================================================================*/
class K2LABI_API CIpcFactory
{
public:
    /// Create new IPC connection.
    /// \return Pointer to new IPC connection.
    static IIpc* CreateIpc();

    /// Destroy IPC connection.
    /// \param[in] Pointer to IPC connection that should be destroied.
    static void DestroyIpc(IIpc* pIpc);
};
        }
    }
}

/*============================================================================*/
/* C interface                                                                */
/*============================================================================*/

extern "C"
{
#define IPC_HANDLE HANDLE

typedef void (*FCT_OnMessageEvent)    (IPC_HANDLE, BYTE, BYTE, BYTE*, int);
typedef void (*FCT_OnEvent)    (IPC_HANDLE);

K2LABI_API IPC_HANDLE CreateIPC             ();
K2LABI_API void       DestroyIPC            (IPC_HANDLE hIpc);

K2LABI_API int        IPCInit               (IPC_HANDLE hIpc, FCT_OnMessageEvent callback, FCT_OnEvent error, FCT_OnEvent overflow);
K2LABI_API int        IPCDeinit             (IPC_HANDLE hIpc);
K2LABI_API int        IPCFormatRequest      (IPC_HANDLE hIpc, BYTE channel, BYTE ifaceId, BYTE funId, DWORD dwSenderID, const BYTE *pReqData, DWORD dataLen, BYTE* pResult, int nResultLen, int* nNeededResultLen);
K2LABI_API void       IPCDecode             (IPC_HANDLE hIpc, BYTE* pData, int nDataLen);
K2LABI_API void       IPCDecodeFlow         (IPC_HANDLE hIpc, HANDLE hReaderPipe, BYTE* pData, int nDataLen);
}

//@}
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/

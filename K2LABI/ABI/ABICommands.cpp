#include <string.h>
#include "ABICommands.h"
#include "ResultParser.h"
#include "ABIFunctionDefinition.h"

using namespace K2L::Automotive::ABI;

int CABICommands::GetSupportedFeatures(IABI* pAbi, char*& result)
{   
    return GetInformation(pAbi, SupportedFeatures, result);
}

int CABICommands::AddReceiverEx(IABI* pAbi, unsigned short handle, Bus bus, unsigned int param1, unsigned int param2)
{
    switch (bus)
    {
        case CAN1:
        case CAN2:
        case CAN3:
        case CAN4:
        case CAN5:
        case CAN6:
        case XCAN1:
        case XCAN2:
        case XCAN3:
        case XCAN4:
        case XCAN5:
        case XCAN6:
            if (param1 > param2)
            {
                unsigned int temp = param1;
                param1 = param2;
                param2 = temp;
            }
            pAbi->AddIdIntervalFilter(bus, handle, param1, param2);
            break;
        case LIN1:
        case LIN2:
        case LIN3:
        case LIN4:
        case LINTP1:
        case LINTP2:
        case LINTP3:
        case LINTP4:
            if (param1 == 0xFF)
            {
                pAbi->AddMaskedIdFilter(bus, handle, param1, 0);
            }
            else
            {
                pAbi->AddMaskedIdFilter(bus, handle, param1, 0xFFFFFFFF);
            }
            param2 = 0;
            break;
        default:
            pAbi->AddMaskedIdFilter(bus, handle, param2, param1);
            break;

    }
    return CABICommands::AddReceiver(pAbi, handle, bus, param1, param2);
}

int CABICommands::RemoveReceiverEx(IABI* pAbi, unsigned short handle)
{
    pAbi->RemoveFilter(handle);
    return CABICommands::Delete(pAbi, handle);
}

int CABICommands::SendMostHighMessage(IABI* pAbi, unsigned short handle, unsigned short targetAddress, BYTE fblockID, BYTE instanceID, unsigned short functionID, BYTE opType, unsigned short payloadLength, BYTE* payload)
{
    unsigned int messageId = fblockID << 24;
    messageId |= instanceID << 16;
    messageId |= (functionID & 0xFFFu) << 4;
    messageId |= opType & 0x0Fu;
    return SendMessage(pAbi, handle, MOST_ASYNC1_HIGH, targetAddress, messageId, payloadLength, payload);
}

int CABICommands::SendMostADSMessage(IABI* pAbi, unsigned short handle, unsigned short targetAddress, unsigned short payloadLength, BYTE* payload)
{
    return SendMessage(pAbi, handle, MOST_ASYNC1_RAW, targetAddress, 0, payloadLength, payload);
}

int CABICommands::SendMostAMSMessage(IABI* pAbi, unsigned short handle, unsigned short targetAddress, BYTE fblockID, BYTE instanceID, unsigned short functionID, BYTE opType, unsigned short payloadLength, BYTE* payload)
{
    unsigned int messageId = fblockID << 24;
    messageId |= instanceID << 16;
    messageId |= (functionID & 0xFFFu) << 4;
    messageId |= opType & 0x0Fu;
    return SendMessage(pAbi, handle, MOST_CONTROL1_AMS, targetAddress, messageId, payloadLength, payload);
}

int CABICommands::SendMostCMSMessage(IABI* pAbi, unsigned short handle, unsigned short targetAddress, BYTE fblockID, BYTE instanceID, unsigned short functionID, BYTE opType, unsigned short payloadLength, BYTE* payload)
{
    unsigned int messageId = fblockID << 24;
    messageId |= instanceID << 16;
    messageId |= (functionID & 0xFFFu) << 4;
    messageId |= opType & 0x0Fu;
    return SendMessage(pAbi, handle, MOST_CONTROL1_RAW, targetAddress, messageId, payloadLength, payload);
}

//int CABICommands::SendCmsMessage(IABI* pAbi, unsigned short handle, unsigned short targetAddress, BYTE fblockId, BYTE instanceId, unsigned short functionId, BYTE opType, BYTE telId, BYTE telLen, bool isMost150, BYTE frameCount, bool isSegmented, unsigned short payloadLength, BYTE* payload)
//{ 
//    if (!pAbi) { return 0; }
//     
//    int resultLen = 0; 
//    BYTE error = 0;
//        
//    unsigned int messageId = fblockId << 24;
//    messageId |= instanceId << 16;
//    messageId |= (functionId & 0xFFFu) << 4;
//    messageId |= opType & 0x0Fu;
//    
//    int i = 0;
//    int parameterCnt = 6;
//
//    payloadLength += isMost150 ? 2 : 1;
//    payloadLength += isSegmented ? 1 : 0; 
//    BYTE bus = MOST_CONTROL1_RAW;
//    
//    ParamDef* pParams = NULL;
//    ParamDef params[9];
//
//    params[0].m_pData = &handle; 
//	params[0].m_nLength = 2;
//	params[1].m_pData = &bus; 
//	params[1].m_nLength = 1;
//	params[2].m_pData = &targetAddress; 
//	params[2].m_nLength = 2;
//	params[3].m_pData = &messageId; 
//	params[3].m_nLength = 4;
//	params[4].m_pData = &payloadLength; 
//	params[4].m_nLength = 2;
//        
//    if(isMost150)
//    {
//        BYTE highTel = (BYTE) ((telId << 4) | ((telLen >> 8) & 0xF));
//        BYTE lowTel = (BYTE) (telLen & 0xFF);
//        
//        params[5].m_pData = &highTel; 
//	    params[5].m_nLength = 1;
//        params[6].m_pData = &lowTel; 
//	    params[6].m_nLength = 1;
//    }
//    else
//    {        
//        BYTE tel = (BYTE)((telId << 4) | (telLen & 0xF));
//                
//        params[5].m_pData = &tel; 
//	    params[5].m_nLength = 1;
//        params[6].m_pData = 0; 
//	    params[6].m_nLength = 0;
//    }
//    if(isSegmented)
//    {
//        params[7].m_pData = &frameCount; 
//	    params[7].m_nLength = 1;
//    }
//    else
//    {
//        params[7].m_pData = 0; 
//	    params[7].m_nLength = 0;
//    }
//
//	params[8].m_pData = payload; 
//	params[8].m_nLength = payloadLength;
//	
//    pParams = params;
//    
//    int bufferLength = 0;
//    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 6, bufferLength);
//
//    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__SendMessage, pBuffer, bufferLength, resultLen, error);
//	delete pBuffer;
//    
//    if (!pRes){ return -1; }
//            
//    return error;
//}

int CABICommands::SendCANMessage(IABI* pAbi, unsigned short handle, BYTE channelNumber, unsigned short id, unsigned short payloadLength, BYTE* payload)
{
    Bus canChannel = CAN1;
    switch (channelNumber)
    {
        case 1:
            canChannel = CAN1;
            break;
        case 2:
            canChannel = CAN2;
            break;
        case 3:
            canChannel = CAN3;
            break;
        case 4:
            canChannel = CAN4;
            break;
        case 5:
            canChannel = CAN5;
            break;
        case 6:
            canChannel = CAN6;
            break;
        default:
            return tmRetValBadBusId;
    }
            
    return SendMessage(pAbi, handle, canChannel, 0, id, payloadLength, payload);
}

int CABICommands::SendXCANMessage(IABI* pAbi, unsigned short handle, BYTE channelNumber, unsigned int id, unsigned short payloadLength, BYTE* payload)
{
    Bus canChannel = XCAN1;
    switch (channelNumber)
    {
        case 1:
            canChannel = XCAN1;
            break;
        case 2:
            canChannel = XCAN2;
            break;
        case 3:
            canChannel = XCAN3;
            break;
        case 4:
            canChannel = XCAN4;
            break;
        case 5:
            canChannel = XCAN5;
            break;
        case 6:
            canChannel = XCAN6;
            break;
        default:
            return tmRetValBadBusId;
    }
            
    return SendMessage(pAbi, handle, canChannel, 0, id, payloadLength, payload);
}

int CABICommands::SendLINMessage(IABI* pAbi, unsigned short handle, BYTE channelNumber, BYTE id, unsigned short payloadLength, BYTE* payload)
{
    Bus linChannel = LIN1;
    switch (channelNumber)
    {
        case 1:
            linChannel = LIN1;
            break;
        case 2:
            linChannel = LIN2;
            break;
        case 3:
            linChannel = LIN3;
            break;
        case 4:
            linChannel = LIN4;
            break;
        default:
            return tmRetValBadBusId;
    }
    
    return SendMessage(pAbi, handle, linChannel, 0, id, payloadLength, payload);
}
int CABICommands::SendLINTpMessage(IABI* pAbi, unsigned short handle, BYTE channelNumber, BYTE frameId, BYTE nodeAddress, BYTE serviceId, unsigned short payloadLength, BYTE* payload)
{
    Bus linChannel = LINTP1;
    switch (channelNumber)
    {
        case 1:
            linChannel = LINTP1;
            break;
        case 2:
            linChannel = LINTP2;
            break;
        case 3:
            linChannel = LINTP3;
            break;
        case 4:
            linChannel = LINTP4;
            break;
        default:
            return tmRetValBadBusId;
    }

    unsigned int messageId = frameId;
    messageId += (unsigned int)nodeAddress << 8;
    messageId += (unsigned int)serviceId << 16;
    
    return SendMessage(pAbi, handle, linChannel, 0, messageId, payloadLength, payload);
}

int CABICommands::SendFRMessage(IABI* pAbi, unsigned short handle, FrChannel frChannel, BYTE cycleOffset, BYTE cycleRepetition, unsigned short frameID, unsigned short payloadLength, BYTE* payload)
{
    unsigned int messageId = frameID;
    messageId |= cycleOffset << 16;
    if(0 < cycleRepetition)
    {
        messageId |= (cycleRepetition - 1) << 24;
    }
    
    switch(frChannel)
    {
        case A:
             messageId |= (1 << 12);
             break;
        case B:
             messageId |= (2 << 12);
             break;
        case AB:
             messageId |= (3 << 12);
             break;
        default:
            break;
    }
    
    return SendMessage(pAbi, handle, FLEXRAY1, 0, messageId, payloadLength, payload);
}

int CABICommands::ReadBusConfiguration(IABI* pAbi, Bus bus, BYTE*& configuration, WORD& length)
{
    if (pAbi == NULL) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &bus; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__ReadBusConfiguration, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
    { 
        pAbi->EndRequest();
        return -1; 
    }   

    configuration = new BYTE[resultLen];
    memcpy(configuration, pRes, resultLen);
    length = resultLen;
    
    pAbi->EndRequest();

    return error;
}

int CABICommands::GetAllUsedHandles(IABI* pAbi, BYTE*& handles, WORD& length)
{
    if (pAbi == NULL) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__GetAllUsedHandles, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
    { 
        pAbi->EndRequest();
        return -1; 
    }  

    handles = new BYTE[resultLen];
    memcpy(handles, pRes, resultLen);
    length = resultLen;
    
    pAbi->EndRequest();

    return error;
}

int CABICommands::GetInformationOfObject( IABI* pAbi, WORD handle, ObjectType& type, bool& isStarted, BYTE& groupId, Bus& bus)
{
    if (pAbi == NULL) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &handle; 
    params[0].m_nLength = 2;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__GetInformationOfObject, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes || ((resultLen < 4) && (error != 0)))
    { 
        type = NullObject;
        isStarted = false;
        groupId = 0;
        bus = UnknownBusId;
        pAbi->EndRequest();
        return -1; 
    }  

    type = (ObjectType) pRes[0];
    isStarted = pRes[1] > 0 ? true : false;
    groupId = pRes[2];
    bus = (Bus) pRes[3];
    
    pAbi->EndRequest();
    
    return error;
}

int CABICommands::GetSupportedCanSpeeds( IABI* pAbi, Bus bus, CanTransceiver type, CanSpeed*& speeds, WORD& length)
{
    if (pAbi == NULL) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[2];
    params[0].m_pData = &bus; 
    params[0].m_nLength = 1;
    params[1].m_pData = &type; 
    params[1].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 2, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__GetSupportedCanSpeeds, pBuffer, bufferLength, resultLen, error);
        delete pBuffer;
    
    if (!pRes)
    { 
        pAbi->EndRequest();
        return -1; 
    }  

    BYTE tmpLength = pRes[0];
    speeds = new CanSpeed[tmpLength];
    
    for (int i = 0; i < tmpLength; i++)
    {
        speeds[i] = (CanSpeed) pRes[i + 1];
    }

    length = tmpLength;

    pAbi->EndRequest();
    
    return error;
}

int CABICommands::GetSupportedCanTransceivers( IABI* pAbi, Bus bus, CanTransceiver*& types, WORD& length)
{
    if (pAbi == NULL) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &bus; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__GetSupportedCanTransceivers, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
    { 
        pAbi->EndRequest();
        return -1; 
    }  

    BYTE tmpLength = pRes[0];
    types = new CanTransceiver[tmpLength];
    
    for (int i = 0; i < tmpLength; i++)
    {
        types[i] = (CanTransceiver) pRes[i + 1];
    }

    length = tmpLength;
    
    pAbi->EndRequest();
    
    return error;
}

int CABICommands::GetMostAllocationTable( IABI* pAbi, WORD& length, AllocationTableEntry*& allocationTable)
{
    if (pAbi == NULL) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__GetMostAllocationTable, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
    { 
        pAbi->EndRequest();
        return -1; 
    }  

    length = *(unsigned short*)pRes;
    allocationTable = NULL;
    if(length > 0)
    {
        allocationTable = new AllocationTableEntry[length];
    
        for (int i = 0; i < length; i++)
        {
            allocationTable[i].connectionLabel = *(WORD*) (pRes + 2 + i * 4);
            allocationTable[i].channelWidth = *(WORD*) (pRes + 2 + i * 4 + 2);
        }
    }
    
    pAbi->EndRequest();
        
    return error;
}

int CABICommands::SendEchoCommand( IABI* pAbi, WORD requestLength, BYTE* request, WORD& answerLength, BYTE*& answer)
{
    if (!pAbi) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[2];
    params[0].m_pData = &requestLength; 
    params[0].m_nLength = 2;
    params[1].m_pData = request; 
    params[1].m_nLength = requestLength;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 2, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__SendEchoCommand, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
    { 
        pAbi->EndRequest();
        return -1; 
    }  

    answerLength = *(WORD*) pRes;
    
    answer = new BYTE[answerLength];
    memcpy(answer, pRes + 2, answerLength);
    
    pAbi->EndRequest();

    return error;
}

int CABICommands::GetMostAudioTxLabels( IABI* pAbi, WORD& length, LabelEntry*& labels)
{
    if (pAbi == NULL) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__GetMostAudioTxLabels, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
    { 
        pAbi->EndRequest();
        return -1; 
    }  

    length = resultLen / 4;
    labels = NULL;
    if(length > 0)
    {
        labels = new LabelEntry[length];
    
        for (int i = 0; i < length; i++)
        {
            labels[i] = *(LabelEntry*) (pRes + i * 4);
            labels[i].connectionLabel = *(WORD*) (pRes + i * 4);
            labels[i].connectorType = (AudioConnectorType) *(pRes + i * 4 + 2);
            labels[i].dummy = 0u;
        }
    }

    pAbi->EndRequest();
        
    return error;
}

int CABICommands::GetMostAudioRxLabels( IABI* pAbi, WORD& length, LabelEntry*& labels)
{
    if (pAbi == NULL) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__GetMostAudioRxLabels, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
    { 
        pAbi->EndRequest();
        return -1; 
    }  

    length = resultLen / 4;
    labels = NULL;
    if(length > 0)
    {
        labels = new LabelEntry[length];
    
        for (int i = 0; i < length; i++)
        {
            labels[i] = *(LabelEntry*) (pRes + i * 4);
            labels[i].connectionLabel = *(WORD*) (pRes + i * 4);
            labels[i].connectorType = (AudioConnectorType) *(pRes + i * 4 + 2);
            labels[i].dummy = 0u;
        }
    }
    
    pAbi->EndRequest();
        
    return error;
}

void CABICommands::ReleaseBuffer(LabelEntry** ptr)
{
    if(NULL != *ptr)
    {
        delete[] *ptr;
        *ptr = NULL;
    }
}

void CABICommands::ReleaseBuffer(char** ptr)
{
    if(NULL != *ptr)
    {
        delete[] *ptr;
        *ptr = NULL;
    }
}

void CABICommands::ReleaseBuffer(BYTE** ptr)
{
    if(NULL != *ptr)
    {
        delete[] *ptr;
        *ptr = NULL;
    }
}

void CABICommands::ReleaseBuffer(AllocationTableEntry** ptr)
{
    if(NULL != *ptr)
    {
        delete[] *ptr;
        *ptr = NULL;
    }
}

void CABICommands::ReleaseBuffer(CanSpeed** ptr)
{
    if(NULL != *ptr)
    {
        delete[] *ptr;
        *ptr = NULL;
    }
}

void CABICommands::ReleaseBuffer(CanTransceiver** ptr)
{
    if(NULL != *ptr)
    {
        delete[] *ptr;
        *ptr = NULL;
    }
}

void CABICommands::ReleaseBuffer(AudioConnectionType** ptr)
{
    if(NULL != *ptr)
    {
        delete[] *ptr;
        *ptr = NULL;
    }
}

int CABICommands::MostAudioRxConnectSync(IABI* pAbi, AudioBandwidth bandwidth, unsigned int label, bool isSwitchForced, WORD& errorCode)
{
    if (!pAbi) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[3];
    params[0].m_pData = &bandwidth; 
    params[0].m_nLength = 1;
    params[1].m_pData = &label; 
    params[1].m_nLength = 4;
    params[2].m_pData = &isSwitchForced; 
    params[2].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 3, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__MostAudioRxConnectSync, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
    { 
        pAbi->EndRequest();
        return -1; 
    }  
     
    unsigned int value = *(unsigned int*)pRes;
    
    if(error == tmRetErrorDuringExecution)
    {
        errorCode = (unsigned short) value;
    }
    else
    {
        errorCode = 0u;
    }
    
    pAbi->EndRequest();
    
    return error;
}
    
int CABICommands::MostAudioRxDisconnectSync(IABI* pAbi, unsigned int label, WORD& errorCode)
{
    if (!pAbi) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &label; 
    params[0].m_nLength = 4;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__MostAudioRxDisconnectSync, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
    { 
        pAbi->EndRequest();
        return -1; 
    }  
     
    unsigned int value = *(unsigned int*)pRes;
    
    if(error == tmRetErrorDuringExecution)
    {
        errorCode = (unsigned short) value;
    }
    else
    {
        errorCode = 0u;
    }

    pAbi->EndRequest();
    
    return error;
}
    
int CABICommands::MostAudioTxAllocateConnectSync(IABI* pAbi, AudioBandwidth bandwidth, unsigned int& connectionLabel, WORD& errorCode)
{
    if (!pAbi) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &bandwidth; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__MostAudioTxAllocateConnectSync, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
    { 
        pAbi->EndRequest();
        return -1; 
    }  
     
    unsigned int value = *(unsigned int*)pRes;
    
    if(error == tmRetErrorDuringExecution)
    {
        connectionLabel = 0u;
        errorCode = (unsigned short) value;
    }
    else
    {
        connectionLabel = value;
        errorCode = 0u;
    }
    
    pAbi->EndRequest();
    
    return error;
}

int CABICommands::MostAudioTxAllocateConnectSync(IABI* pAbi, AudioBandwidth bandwidth, AudioConnectorType connector, unsigned int& connectionLabel, WORD& errorCode)
{
    if (!pAbi) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[2];
    params[0].m_pData = &bandwidth; 
    params[0].m_nLength = 1;
    params[1].m_pData = &connector; 
    params[1].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 2, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__MostAudioTxAllocateConnectSync, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
    { 
        pAbi->EndRequest();
        return -1; 
    }  
     
    unsigned int value = *(unsigned int*)pRes;
    
    if(error == tmRetErrorDuringExecution)
    {
        connectionLabel = 0u;
        errorCode = (unsigned short) value;
    }
    else
    {
        connectionLabel = value;
        errorCode = 0u;
    }

    pAbi->EndRequest();
    
    return error;
}
    
int CABICommands::MostAudioTxDisconnectSync(IABI* pAbi, unsigned int label, WORD& errorCode)
{
    if (!pAbi) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &label; 
    params[0].m_nLength = 4;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__MostAudioTxDisconnectSync, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
    { 
        pAbi->EndRequest();
        return -1; 
    }  
     
    unsigned int value = *(unsigned int*)pRes;
    
    if(error == tmRetErrorDuringExecution)
    {
        errorCode = (unsigned short) value;
    }
    else
    {
        errorCode = 0u;
    }
    
    pAbi->EndRequest();

    return error;
}

int CABICommands::GetExtendedBaudrate(IABI* pAbi, Bus busId
        , unsigned int& prescaler, unsigned int& tseg1, unsigned int& tseg2, unsigned int& sjw)
{
    if (!pAbi) { return 0; }
         
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &busId; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__GetExtendedBaudrate, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
        
    if (!pRes)
    { 
        prescaler = 0;
        tseg1 = 0;
        tseg2 = 0;
        sjw = 0;
        pAbi->EndRequest();
        return -1; 
    }

    prescaler = *(unsigned int*) (pRes);
    tseg1 = *(unsigned int*) (pRes + 4);
    tseg2 = *(unsigned int*) (pRes + 8);
    sjw = *(unsigned int*) (pRes + 12);
    
    pAbi->EndRequest();

    return error;
}

int CABICommands::GetHardwareRevision(IABI* pAbi, BYTE& hwType, BYTE& major, BYTE& minor, BYTE& bugfix)
{
    if (!pAbi) { return 0; }
         
    int resultLen = 0; 
    BYTE error = 0;
    ParamDef* pParams = NULL;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__GetHardwareRevision, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
    { 
        hwType = 0;
        major = 0;
        minor = 0;
        bugfix = 0;
        pAbi->EndRequest();
        return -1; 
    }

    hwType = pRes[0];
    major = pRes[1];
    minor = pRes[2];
    bugfix = pRes[3];
    
    pAbi->EndRequest();

    return error;
}

int CABICommands::MostSpyEnabled(IABI* pAbi, bool& status, bool& control, bool& async, bool& sync)
{
    if (!pAbi) { return 0; }
         
    int resultLen = 0; 
    BYTE error = 0;
    ParamDef* pParams = NULL;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__MostSpyEnabled, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
    { 
        status = false;
        control = false;
        async = false;
        sync = false;
        pAbi->EndRequest();
        return -1; 
    }

    status = pRes[0] > 0 ? true : false;
    control = pRes[1] > 0 ? true : false;
    async = pRes[2] > 0 ? true : false;
    sync = pRes[3] > 0 ? true : false;
    
    pAbi->EndRequest();

    return error;
}

int CABICommands::GetCanTpRxConfig(IABI* pAbi, BYTE& blockSize, BYTE& separationTime, WORD& consecutiveFrameTimeout)
{
    if (!pAbi) { return 0; }
         
    int resultLen = 0; 
    BYTE error = 0;
    ParamDef* pParams = NULL;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__GetCanTpRxConfig, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
    { 
        blockSize = 0;
        separationTime = 0;
        consecutiveFrameTimeout = 0;
        pAbi->EndRequest();
        return -1; 
    }

    blockSize = pRes[0];
    separationTime = pRes[1];
    consecutiveFrameTimeout = *(unsigned short*) (pRes + 2);
    
    pAbi->EndRequest();

    return error;
}

int CABICommands::GetCanTpTxConfig(IABI* pAbi, WORD& flowControlTimeout, WORD& clearToSendTimeout)
{
    if (!pAbi) { return 0; }
         
    int resultLen = 0; 
    BYTE error = 0;
    ParamDef* pParams = NULL;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__GetCanTpTxConfig, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
    { 
        flowControlTimeout = 0;
        clearToSendTimeout = 0;
        pAbi->EndRequest();
        return -1; 
    }
    
    flowControlTimeout = *(unsigned short*) (pRes);
    clearToSendTimeout = *(unsigned short*) (pRes + 2);
    
    pAbi->EndRequest();

    return error;
}

int CABICommands::GetCanTpPattern(IABI* pAbi, BYTE& pattern)
{
    if (!pAbi) { return 0; }
         
    int resultLen = 0; 
    BYTE error = 0;
    ParamDef* pParams = NULL;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__GetCanTpPattern, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
    { 
        pattern = 0;
        pAbi->EndRequest();
        return -1; 
    }

    pattern = pRes[0];
    
    pAbi->EndRequest();

    return error;
}

int CABICommands::GetMostAudioSupportedTxConnectors(IABI* pAbi, WORD& length, AudioConnectorType*& types)
{
    if (pAbi == NULL) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__GetMostAudioSupportedTxConnectors, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
    { 
        pAbi->EndRequest();
        return -1; 
    }  

    length = *(unsigned short*) (pRes);
    types = NULL;
    if(length > 0)
    {
        types = new AudioConnectorType[length];
    
        for (int i = 0; i < length; i++)
        {
            types[i] = (AudioConnectorType) *(pRes + i + 2);
        }
    }

    pAbi->EndRequest();
        
    return error;
}

int CABICommands::StressNicFunctionGet(IABI* pAbi, BYTE fblockId, unsigned short functionId, WORD& length, BYTE*& payload)
{
    if (pAbi == NULL) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[2];
    params[0].m_pData = &fblockId; 
    params[0].m_nLength = 1;
    params[1].m_pData = &functionId; 
    params[1].m_nLength = 2;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 2, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)StressNic, (BYTE)FunId__StressNicFunctionGet, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
    { 
        pAbi->EndRequest();
        return -1; 
    }  

    length = *(WORD*) pRes;
    
    payload = new BYTE[length];
    memcpy(payload, pRes + 2, length);
    
    pAbi->EndRequest();

    return error;
}

int CABICommands::StressNicFunctionGetExtended(IABI* pAbi, BYTE fblockId, unsigned short functionId, WORD requestLength, BYTE* request, WORD& responseLength, BYTE*& response)
{
    if (pAbi == NULL) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[4];
    params[0].m_pData = &fblockId; 
    params[0].m_nLength = 1;
    params[1].m_pData = &functionId; 
    params[1].m_nLength = 2;
    params[2].m_pData = &requestLength; 
    params[2].m_nLength = 2;
    params[3].m_pData = request; 
    params[3].m_nLength = requestLength;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 4, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)StressNic, (BYTE)FunId__StressNicFunctionGetExtended, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
    { 
        pAbi->EndRequest();
        return -1; 
    }  

    responseLength = *(WORD*) pRes;
    
    response = new BYTE[responseLength];
    memcpy(response, pRes + 2, responseLength);
    
    pAbi->EndRequest();

    return error;
}

int CABICommands::GetVolume(IABI* pAbi, AudioConnectorType connector, bool isInput, int& volumeLeft, int& volumeRight)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[2];
    params[0].m_pData = &connector; 
    params[0].m_nLength = 1;
    params[1].m_pData = &isInput; 
    params[1].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 2, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__GetVolume, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
        volumeLeft = 0;
        volumeRight = 0;
		pAbi->EndRequest();
		return -1; 
	}
    
    volumeLeft = *(int*) pRes;
    volumeRight = *(int*) (pRes + 4);
    
	pAbi->EndRequest();

    return error;
}

int CABICommands::GetSupportedVolumeRange(IABI* pAbi, AudioConnectorType connector, bool isInput, int& minVolume, int& maxVolume)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[2];
    params[0].m_pData = &connector; 
    params[0].m_nLength = 1;
    params[1].m_pData = &isInput; 
    params[1].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 2, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__GetSupportedVolumeRange, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
        minVolume = 0;
        maxVolume = 0;
		pAbi->EndRequest();
		return -1; 
	}
    
    minVolume = *(int*) pRes;
    maxVolume = *(int*) (pRes + 4);
    
	pAbi->EndRequest();

    return error;
}

int CABICommands::SendAbsoluteTriggerSequence(IABI* pAbi, WORD length, AbsoluteTriggerCommand* sequence)
{
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;

    if(length == 0)
        return 0;
    
    ParamDef* pParams = new ParamDef[length * 2];
    for(int i = 0; i < length; i++)
    {
        pParams[i * 2].m_pData = &sequence[i].isActive; 
        pParams[i * 2].m_nLength = 1;
        pParams[i * 2 + 1].m_pData = &sequence[i].timeStamp; 
        pParams[i * 2 + 1].m_nLength = 8;
    }
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, length * 2, bufferLength);
    delete[] pParams;

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__SendAbsoluteTriggerSequence, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommands::SendRelativeTriggerSequence(IABI* pAbi, WORD length, RelativeTriggerCommand* sequence)
{
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;

    if(length == 0)
        return 0;
    
    ParamDef* pParams = new ParamDef[length * 2];
    for(int i = 0; i < length; i++)
    {
        pParams[i * 2].m_pData = &sequence[i].isActive; 
        pParams[i * 2].m_nLength = 1;
        pParams[i * 2 + 1].m_pData = &sequence[i].delay; 
        pParams[i * 2 + 1].m_nLength = 2;
    }
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, length * 2, bufferLength);
    delete[] pParams;

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__SendRelativeTriggerSequence, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}
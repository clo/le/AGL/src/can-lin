#include <string.h>
#include "k2l-type.h"
#include "ABICommands.h"
#include "K2LABI.h"
#include "ResultParser.h"
#include "ABIFunctionDefinition.h"

using namespace K2L::Automotive::ABI;

int CABICommandsEx::AddCanTpReceiver(IABI* pAbi, unsigned short handle, Bus bus, unsigned int messageIdSrc, unsigned int messageIdDst, TPAddressingType addressingType)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[5];
    params[0].m_pData = &handle; 
    params[0].m_nLength = 2;
    params[1].m_pData = &bus; 
    params[1].m_nLength = 1;
    params[2].m_pData = &messageIdSrc; 
    params[2].m_nLength = 4;
    params[3].m_pData = &messageIdDst; 
    params[3].m_nLength = 4;
    params[4].m_pData = &addressingType; 
    params[4].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 5, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__AddCanTpReceiver, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::AddCanTpTransmitter(IABI* pAbi, unsigned short handle, Bus bus, unsigned int srcMessageId, unsigned int dstMessageId, TPAddressingType addressingType, unsigned short cycleTime, unsigned int totalNumber, unsigned short payloadLength, BYTE* payload)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[9];
    params[0].m_pData = &handle; 
    params[0].m_nLength = 2;
    params[1].m_pData = &bus; 
    params[1].m_nLength = 1;
    params[2].m_pData = &srcMessageId; 
    params[2].m_nLength = 4;
    params[3].m_pData = &dstMessageId; 
    params[3].m_nLength = 4;
    params[4].m_pData = &addressingType; 
    params[4].m_nLength = 1;
    params[5].m_pData = &cycleTime; 
    params[5].m_nLength = 2;
    params[6].m_pData = &totalNumber; 
    params[6].m_nLength = 4;
    params[7].m_pData = &payloadLength; 
    params[7].m_nLength = 2;
    params[8].m_pData = payload; 
    params[8].m_nLength = payloadLength;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 9, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__AddCanTpTransmitter, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::AddReceiver(IABI* pAbi, unsigned short handle, Bus bus, unsigned int param1, unsigned int param2)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[4];
    params[0].m_pData = &handle; 
    params[0].m_nLength = 2;
    params[1].m_pData = &bus; 
    params[1].m_nLength = 1;
    params[2].m_pData = &param1; 
    params[2].m_nLength = 4;
    params[3].m_pData = &param2; 
    params[3].m_nLength = 4;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 4, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__AddReceiver, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::AddTransmitterEx(IABI* pAbi, unsigned short handle, Bus bus, unsigned short targetAddress, unsigned int messageId, unsigned int delay, unsigned short cycle, unsigned int totalNumber, unsigned short payloadLength, BYTE* payload)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[9];
    params[0].m_pData = &handle; 
    params[0].m_nLength = 2;
    params[1].m_pData = &bus; 
    params[1].m_nLength = 1;
    params[2].m_pData = &targetAddress; 
    params[2].m_nLength = 2;
    params[3].m_pData = &messageId; 
    params[3].m_nLength = 4;
    params[4].m_pData = &delay; 
    params[4].m_nLength = 4;
    params[5].m_pData = &cycle; 
    params[5].m_nLength = 2;
    params[6].m_pData = &totalNumber; 
    params[6].m_nLength = 4;
    params[7].m_pData = &payloadLength; 
    params[7].m_nLength = 2;
    params[8].m_pData = payload; 
    params[8].m_nLength = payloadLength;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 9, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__AddTransmitterEx, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::AddTxSweepSignal(IABI* pAbi, unsigned short handle, SignalType signalType, unsigned short firstBitPos, unsigned short lastBitPos, unsigned short stepsNum, int step, unsigned int initialValue, unsigned int repetitionsNumber)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[8];
    params[0].m_pData = &handle; 
    params[0].m_nLength = 2;
    params[1].m_pData = &signalType; 
    params[1].m_nLength = 1;
    params[2].m_pData = &firstBitPos; 
    params[2].m_nLength = 2;
    params[3].m_pData = &lastBitPos; 
    params[3].m_nLength = 2;
    params[4].m_pData = &stepsNum; 
    params[4].m_nLength = 2;
    params[5].m_pData = &step; 
    params[5].m_nLength = 4;
    params[6].m_pData = &initialValue; 
    params[6].m_nLength = 4;
    params[7].m_pData = &repetitionsNumber; 
    params[7].m_nLength = 4;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 8, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__AddTxSweepSignal, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::ChangeCycleTime(IABI* pAbi, unsigned short handle, unsigned short cycle)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[2];
    params[0].m_pData = &handle; 
    params[0].m_nLength = 2;
    params[1].m_pData = &cycle; 
    params[1].m_nLength = 2;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 2, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__ChangeCycleTime, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::ChangeTransmitterData(IABI* pAbi, unsigned short handle, unsigned short payloadLength, BYTE* payload)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[3];
    params[0].m_pData = &handle; 
    params[0].m_nLength = 2;
    params[1].m_pData = &payloadLength; 
    params[1].m_nLength = 2;
    params[2].m_pData = payload; 
    params[2].m_nLength = payloadLength;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 3, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__ChangeTransmitterData, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::ClearCanTpPattern(IABI* pAbi)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__ClearCanTpPattern, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::Delete(IABI* pAbi, unsigned short handle)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &handle; 
    params[0].m_nLength = 2;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__Delete, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::DeleteTxSweepSignals(IABI* pAbi, unsigned short handle)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &handle; 
    params[0].m_nLength = 2;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__DeleteTxSweepSignals, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::EnableFlexRayTx(IABI* pAbi, bool enabled)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &enabled; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__EnableFlexRayTx, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::EnableMepEthernetBridge(IABI* pAbi, bool isEnabled)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &isEnabled; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__EnableMepEthernetBridge, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::EnableMepEthernetBridgeExtended(IABI* pAbi, bool isEnabled, MepBridgeOptions options)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[2];
    params[0].m_pData = &isEnabled; 
    params[0].m_nLength = 1;
    params[1].m_pData = &options; 
    params[1].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 2, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__EnableMepEthernetBridgeExtended, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::EnableMostSpy(IABI* pAbi, bool status, bool control, bool async, bool sync)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[4];
    params[0].m_pData = &status; 
    params[0].m_nLength = 1;
    params[1].m_pData = &control; 
    params[1].m_nLength = 1;
    params[2].m_pData = &async; 
    params[2].m_nLength = 1;
    params[3].m_pData = &sync; 
    params[3].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 4, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__EnableMostSpy, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetBoundary(IABI* pAbi, BYTE& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__GetBoundary, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = *(BYTE*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetCanBaseFrequency(IABI* pAbi, Bus bus, unsigned int& result)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &bus; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__GetCanBaseFrequency, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = *(unsigned int*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetCanBusCount(IABI* pAbi, unsigned int& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__GetCanBusCount, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = *(unsigned int*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetCanSpeed(IABI* pAbi, Bus bus, CanSpeed& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &bus; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__GetCanSpeed, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = (K2L::Automotive::ABI::CanSpeed)*pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetCanTransceiver(IABI* pAbi, Bus bus, CanTransceiver& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &bus; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__GetCanTransceiver, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = (K2L::Automotive::ABI::CanTransceiver)*pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetDeviceMode(IABI* pAbi, MostMode& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__GetDeviceMode, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = (K2L::Automotive::ABI::MostMode)*pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetFirmwareVersion(IABI* pAbi, char*& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__GetFirmwareVersion, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = new char[resultLen+1];
    memcpy(result, pRes, resultLen+1);
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetFlexRayBusCount(IABI* pAbi, unsigned int& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__GetFlexRayBusCount, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = *(unsigned int*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetGroupAddr(IABI* pAbi, BYTE& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__GetGroupAddr, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = *(BYTE*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetGwAddress(IABI* pAbi, char*& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__GetGwAddress, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = new char[resultLen+1];
    memcpy(result, pRes, resultLen+1);
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetHardwareVersion(IABI* pAbi, char*& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__GetHardwareVersion, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = new char[resultLen+1];
    memcpy(result, pRes, resultLen+1);
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetInformation(IABI* pAbi, BoardInformation info, char*& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &info; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__GetInformation, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = new char[resultLen+1];
    memcpy(result, pRes, resultLen+1);
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetInternalState(IABI* pAbi, Parameter parameter, unsigned int& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &parameter; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__GetInternalState, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = *(unsigned int*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetIpAddress(IABI* pAbi, char*& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__GetIpAddress, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = new char[resultLen+1];
    memcpy(result, pRes, resultLen+1);
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetLinBusCount(IABI* pAbi, unsigned int& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__GetLinBusCount, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = *(unsigned int*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetLinConfigurationName(IABI* pAbi, Bus bus, char*& result)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &bus; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__GetLinConfigurationName, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = new char[resultLen+1];
    memcpy(result, pRes, resultLen+1);
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetLinSpeed(IABI* pAbi, Bus bus, unsigned int& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &bus; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__GetLinSpeed, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = *(unsigned int*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetLogicalNodeAddr(IABI* pAbi, unsigned short& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__GetLogicalNodeAddr, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = *(unsigned short*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetMostControlChannelHighLevelRetry(IABI* pAbi, BYTE& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__GetMostControlChannelHighLevelRetry, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = *(BYTE*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetMostControlChannelLowLevelRetry(IABI* pAbi, BYTE& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__GetMostControlChannelLowLevelRetry, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = *(BYTE*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetMostControlChannelMidLevelRetry(IABI* pAbi, BYTE& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__GetMostControlChannelMidLevelRetry, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = *(BYTE*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetMostFrequency(IABI* pAbi, MostSamplingFrequency& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__GetMostFrequency, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = (K2L::Automotive::ABI::MostSamplingFrequency)*pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetMostIlluminationLevel(IABI* pAbi, MostIlluminationLevel& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__GetMostIlluminationLevel, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = (K2L::Automotive::ABI::MostIlluminationLevel)*pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetMostMacAddress(IABI* pAbi, unsigned long long& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__GetMostMacAddress, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = *(unsigned long long*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetMostShutdownReason(IABI* pAbi, BYTE& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__GetMostShutdownReason, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = *(BYTE*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetMostSpeedGrade(IABI* pAbi, unsigned int& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__GetMostSpeedGrade, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = *(unsigned int*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetMPR(IABI* pAbi, BYTE& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__GetMPR, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = *(BYTE*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetMuteState(IABI* pAbi, AudioConnectorType connector, bool isInput, bool& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[2];
    params[0].m_pData = &connector; 
    params[0].m_nLength = 1;
    params[1].m_pData = &isInput; 
    params[1].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 2, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__GetMuteState, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = 0 != *(BYTE*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetNetMask(IABI* pAbi, char*& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__GetNetMask, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = new char[resultLen+1];
    memcpy(result, pRes, resultLen+1);
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetNPR(IABI* pAbi, BYTE& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__GetNPR, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = *(BYTE*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetParameter(IABI* pAbi, char* key, char*& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = (void*) key; 
    params[0].m_nLength = strlen(key) + 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__GetParameter, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = new char[resultLen+1];
    memcpy(result, pRes, resultLen+1);
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetParameterKeys(IABI* pAbi, char*& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__GetParameterKeys, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = new char[resultLen+1];
    memcpy(result, pRes, resultLen+1);
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetParameterValues(IABI* pAbi, char* key, char*& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = (void*) key; 
    params[0].m_nLength = strlen(key) + 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__GetParameterValues, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = new char[resultLen+1];
    memcpy(result, pRes, resultLen+1);
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetRunningTime(IABI* pAbi, unsigned long long& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__GetRunningTime, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = *(unsigned long long*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetSerialNumber(IABI* pAbi, char*& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__GetSerialNumber, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = new char[resultLen+1];
    memcpy(result, pRes, resultLen+1);
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetTimeStamp(IABI* pAbi, unsigned long long& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__GetTimeStamp, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = *(unsigned long long*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetTimingSynchronisationMode(IABI* pAbi, TimingSynchronisationMode& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__GetTimingSynchronisationMode, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = (K2L::Automotive::ABI::TimingSynchronisationMode)*pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::GetTriggerLevel(IABI* pAbi, bool& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__GetTriggerLevel, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = 0 != *(BYTE*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::HasAllocationTableSupport(IABI* pAbi, bool& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__HasAllocationTableSupport, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = 0 != *(BYTE*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::HasMultipleStreamInSupport(IABI* pAbi, bool& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__HasMultipleStreamInSupport, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = 0 != *(BYTE*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::HasRedirectStreamInToOutSupport(IABI* pAbi, bool& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__HasRedirectStreamInToOutSupport, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = 0 != *(BYTE*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::Identify(IABI* pAbi)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__Identify, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::IsBusStarted(IABI* pAbi, Bus bus, bool& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &bus; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__IsBusStarted, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = 0 != *(BYTE*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::IsCanTransceiverSupported(IABI* pAbi, Bus bus, CanTransceiver transceiver, bool& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[2];
    params[0].m_pData = &bus; 
    params[0].m_nLength = 1;
    params[1].m_pData = &transceiver; 
    params[1].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 2, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__IsCanTransceiverSupported, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = 0 != *(BYTE*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::IsFlexRaySynchronized(IABI* pAbi, Bus bus, bool& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &bus; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__IsFlexRaySynchronized, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = 0 != *(BYTE*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::IsFlexRayTxEnabled(IABI* pAbi, bool& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__IsFlexRayTxEnabled, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = 0 != *(BYTE*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::IsLinMaster(IABI* pAbi, Bus bus, bool& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &bus; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__IsLinMaster, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = 0 != *(BYTE*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::IsMepEthernetBridgeEnabled(IABI* pAbi, bool& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__IsMepEthernetBridgeEnabled, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = 0 != *(BYTE*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::IsMostNodeSupported(IABI* pAbi, bool& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__IsMostNodeSupported, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = 0 != *(BYTE*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::IsMostSpySupported(IABI* pAbi, bool& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__IsMostSpySupported, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = 0 != *(BYTE*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::IsMostStressSupported(IABI* pAbi, bool& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__IsMostStressSupported, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = 0 != *(BYTE*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::IsNetOn(IABI* pAbi, bool& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__IsNetOn, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = 0 != *(BYTE*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::IsRelayClosed(IABI* pAbi, bool& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__IsRelayClosed, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = 0 != *(BYTE*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::IsStableLock(IABI* pAbi, bool& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__IsStableLock, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = 0 != *(BYTE*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::IsTxPayloadFilterEnabled(IABI* pAbi, Bus bus, bool& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &bus; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__IsTxPayloadFilterEnabled, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = 0 != *(BYTE*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::MostAudioAllocate(IABI* pAbi, AudioBandwidth bandwidth)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &bandwidth; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__MostAudioAllocate, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::MostAudioRxConnect(IABI* pAbi, AudioBandwidth bandwidth, unsigned int label)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[2];
    params[0].m_pData = &bandwidth; 
    params[0].m_nLength = 1;
    params[1].m_pData = &label; 
    params[1].m_nLength = 4;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 2, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__MostAudioRxConnect, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::MostAudioRxConnectExt(IABI* pAbi, AudioBandwidth bandwidth, unsigned int label)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[2];
    params[0].m_pData = &bandwidth; 
    params[0].m_nLength = 1;
    params[1].m_pData = &label; 
    params[1].m_nLength = 4;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 2, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__MostAudioRxConnectExt, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::MostAudioRxDisconnect(IABI* pAbi, unsigned int label)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &label; 
    params[0].m_nLength = 4;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__MostAudioRxDisconnect, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::MostAudioTxAllocateConnect(IABI* pAbi, AudioBandwidth bandwidth)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &bandwidth; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__MostAudioTxAllocateConnect, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::MostAudioTxAllocateConnect(IABI* pAbi, AudioBandwidth bandwidth, AudioConnectorType connector)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[2];
    params[0].m_pData = &bandwidth; 
    params[0].m_nLength = 1;
    params[1].m_pData = &connector; 
    params[1].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 2, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__MostAudioTxAllocateConnect, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::MostAudioTxConnect(IABI* pAbi, AudioBandwidth bandwidth, unsigned int label)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[2];
    params[0].m_pData = &bandwidth; 
    params[0].m_nLength = 1;
    params[1].m_pData = &label; 
    params[1].m_nLength = 4;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 2, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__MostAudioTxConnect, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::MostAudioTxDisconnect(IABI* pAbi, unsigned int label)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &label; 
    params[0].m_nLength = 4;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__MostAudioTxDisconnect, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::RebootFirmware(IABI* pAbi)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__RebootFirmware, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::ResetMostShutdownReason(IABI* pAbi)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__ResetMostShutdownReason, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::ResetTimeStamp(IABI* pAbi)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__ResetTimeStamp, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::SelectGroup(IABI* pAbi, BYTE group)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &group; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__SelectGroup, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::SendCanTpMessage(IABI* pAbi, unsigned short handle, Bus bus, unsigned int source, unsigned int destination, TPAddressingType type, unsigned short payloadLength, BYTE* payload)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[7];
    params[0].m_pData = &handle; 
    params[0].m_nLength = 2;
    params[1].m_pData = &bus; 
    params[1].m_nLength = 1;
    params[2].m_pData = &source; 
    params[2].m_nLength = 4;
    params[3].m_pData = &destination; 
    params[3].m_nLength = 4;
    params[4].m_pData = &type; 
    params[4].m_nLength = 1;
    params[5].m_pData = &payloadLength; 
    params[5].m_nLength = 2;
    params[6].m_pData = payload; 
    params[6].m_nLength = payloadLength;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 7, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__SendCanTpMessage, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::SendMessage(IABI* pAbi, unsigned short handle, Bus bus, unsigned short targetAddress, unsigned int messageId, unsigned short payloadLength, BYTE* payload)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[6];
    params[0].m_pData = &handle; 
    params[0].m_nLength = 2;
    params[1].m_pData = &bus; 
    params[1].m_nLength = 1;
    params[2].m_pData = &targetAddress; 
    params[2].m_nLength = 2;
    params[3].m_pData = &messageId; 
    params[3].m_nLength = 4;
    params[4].m_pData = &payloadLength; 
    params[4].m_nLength = 2;
    params[5].m_pData = payload; 
    params[5].m_nLength = payloadLength;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 6, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__SendMessage, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::SendMostHighExtendedMessage(IABI* pAbi, unsigned short handle, unsigned short targetAddress, BYTE fblockID, BYTE instanceID, unsigned short functionID, BYTE opType, unsigned short minDelay, unsigned short maxDelay, BYTE priority, MostMhpOptions options, unsigned int totalPacketLength, unsigned short payloadLength, BYTE* payload)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[13];
    params[0].m_pData = &handle; 
    params[0].m_nLength = 2;
    params[1].m_pData = &targetAddress; 
    params[1].m_nLength = 2;
    params[2].m_pData = &fblockID; 
    params[2].m_nLength = 1;
    params[3].m_pData = &instanceID; 
    params[3].m_nLength = 1;
    params[4].m_pData = &functionID; 
    params[4].m_nLength = 2;
    params[5].m_pData = &opType; 
    params[5].m_nLength = 1;
    params[6].m_pData = &minDelay; 
    params[6].m_nLength = 2;
    params[7].m_pData = &maxDelay; 
    params[7].m_nLength = 2;
    params[8].m_pData = &priority; 
    params[8].m_nLength = 1;
    params[9].m_pData = &options; 
    params[9].m_nLength = 1;
    params[10].m_pData = &totalPacketLength; 
    params[10].m_nLength = 4;
    params[11].m_pData = &payloadLength; 
    params[11].m_nLength = 2;
    params[12].m_pData = payload; 
    params[12].m_nLength = payloadLength;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 13, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__SendMostHighExtendedMessage, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::SendMostHighExtMessage(IABI* pAbi, unsigned short handle, unsigned short targetAddress, BYTE fblockID, BYTE instanceID, unsigned short functionID, BYTE opType, unsigned int totalPacketLength, unsigned short payloadLength, BYTE* payload)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[9];
    params[0].m_pData = &handle; 
    params[0].m_nLength = 2;
    params[1].m_pData = &targetAddress; 
    params[1].m_nLength = 2;
    params[2].m_pData = &fblockID; 
    params[2].m_nLength = 1;
    params[3].m_pData = &instanceID; 
    params[3].m_nLength = 1;
    params[4].m_pData = &functionID; 
    params[4].m_nLength = 2;
    params[5].m_pData = &opType; 
    params[5].m_nLength = 1;
    params[6].m_pData = &totalPacketLength; 
    params[6].m_nLength = 4;
    params[7].m_pData = &payloadLength; 
    params[7].m_nLength = 2;
    params[8].m_pData = payload; 
    params[8].m_nLength = payloadLength;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 9, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__SendMostHighExtMessage, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::SendMostMepExtMessage(IABI* pAbi, unsigned short handle, BYTE retryCount, BYTE priority, unsigned short payloadLength, BYTE* payload)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[5];
    params[0].m_pData = &handle; 
    params[0].m_nLength = 2;
    params[1].m_pData = &retryCount; 
    params[1].m_nLength = 1;
    params[2].m_pData = &priority; 
    params[2].m_nLength = 1;
    params[3].m_pData = &payloadLength; 
    params[3].m_nLength = 2;
    params[4].m_pData = payload; 
    params[4].m_nLength = payloadLength;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 5, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__SendMostMepExtMessage, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::SendMostMepMessage(IABI* pAbi, unsigned short handle, unsigned short payloadLength, BYTE* payload)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[3];
    params[0].m_pData = &handle; 
    params[0].m_nLength = 2;
    params[1].m_pData = &payloadLength; 
    params[1].m_nLength = 2;
    params[2].m_pData = payload; 
    params[2].m_nLength = payloadLength;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 3, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__SendMostMepMessage, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::SetBoundary(IABI* pAbi, BYTE boundary)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &boundary; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__SetBoundary, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::SetCanTpPattern(IABI* pAbi, BYTE pattern)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &pattern; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__SetCanTpPattern, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::SetCanTpRxConfig(IABI* pAbi, BYTE blockSize, BYTE separationTime, unsigned short consecutiveFrameTimeout)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[3];
    params[0].m_pData = &blockSize; 
    params[0].m_nLength = 1;
    params[1].m_pData = &separationTime; 
    params[1].m_nLength = 1;
    params[2].m_pData = &consecutiveFrameTimeout; 
    params[2].m_nLength = 2;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 3, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__SetCanTpRxConfig, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::SetCanTpTxConfig(IABI* pAbi, unsigned short flowControlTimeout, unsigned short clearToSendTimeout)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[2];
    params[0].m_pData = &flowControlTimeout; 
    params[0].m_nLength = 2;
    params[1].m_pData = &clearToSendTimeout; 
    params[1].m_nLength = 2;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 2, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__SetCanTpTxConfig, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::SetDeviceMode(IABI* pAbi, MostMode deviceMode, DeviceModeOptions options)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[2];
    params[0].m_pData = &deviceMode; 
    params[0].m_nLength = 1;
    params[1].m_pData = &options; 
    params[1].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 2, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__SetDeviceMode, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::SetGroupAddr(IABI* pAbi, unsigned short groupAddress)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &groupAddress; 
    params[0].m_nLength = 2;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__SetGroupAddr, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::SetLogicalNodeAddr(IABI* pAbi, unsigned short nodeAddress)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &nodeAddress; 
    params[0].m_nLength = 2;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__SetLogicalNodeAddr, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::SetMostControlChannelHighLevelRetry(IABI* pAbi, BYTE retryCount)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &retryCount; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__SetMostControlChannelHighLevelRetry, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::SetMostControlChannelLowLevelRetry(IABI* pAbi, BYTE retryCount)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &retryCount; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__SetMostControlChannelLowLevelRetry, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::SetMostControlChannelMidLevelRetry(IABI* pAbi, BYTE retryCount)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &retryCount; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__SetMostControlChannelMidLevelRetry, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::SetMostFrequency(IABI* pAbi, MostSamplingFrequency frequency)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &frequency; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__SetMostFrequency, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::SetMostIlluminationLevel(IABI* pAbi, MostIlluminationLevel level)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &level; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__SetMostIlluminationLevel, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::SetMostMacAddress(IABI* pAbi, unsigned long long macAddress, bool isPersistent)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[2];
    params[0].m_pData = &macAddress; 
    params[0].m_nLength = 8;
    params[1].m_pData = &isPersistent; 
    params[1].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 2, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__SetMostMacAddress, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::SetMuteState(IABI* pAbi, AudioConnectorType connector, bool isInput, bool mute)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[3];
    params[0].m_pData = &connector; 
    params[0].m_nLength = 1;
    params[1].m_pData = &isInput; 
    params[1].m_nLength = 1;
    params[2].m_pData = &mute; 
    params[2].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 3, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__SetMuteState, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::SetParameter(IABI* pAbi, char* key, char* newValue)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[2];
    params[0].m_pData = (void*) key; 
    params[0].m_nLength = strlen(key) + 1;
    params[1].m_pData = (void*) newValue; 
    params[1].m_nLength = strlen(newValue) + 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 2, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__SetParameter, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::SetRelay(IABI* pAbi, bool switchOn, unsigned long long& result)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &switchOn; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__SetRelay, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = *(unsigned long long*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::SetTimeStamp(IABI* pAbi, unsigned long long timestamp)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &timestamp; 
    params[0].m_nLength = 8;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__SetTimeStamp, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::SetTimingSynchronisationMode(IABI* pAbi, TimingSynchronisationMode mode)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &mode; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__SetTimingSynchronisationMode, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::SetTriggerLevel(IABI* pAbi, bool isActive)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &isActive; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__SetTriggerLevel, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::SetTxPayloadFilter(IABI* pAbi, Bus bus, bool isEnabled)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[2];
    params[0].m_pData = &bus; 
    params[0].m_nLength = 1;
    params[1].m_pData = &isEnabled; 
    params[1].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 2, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__SetTxPayloadFilter, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::SetVolume(IABI* pAbi, AudioConnectorType connector, bool isInput, bool modifyLeft, bool modifyRight, int volume)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[5];
    params[0].m_pData = &connector; 
    params[0].m_nLength = 1;
    params[1].m_pData = &isInput; 
    params[1].m_nLength = 1;
    params[2].m_pData = &modifyLeft; 
    params[2].m_nLength = 1;
    params[3].m_pData = &modifyRight; 
    params[3].m_nLength = 1;
    params[4].m_pData = &volume; 
    params[4].m_nLength = 4;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 5, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__SetVolume, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::ShutdownMost(IABI* pAbi)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__ShutdownMost, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::Start(IABI* pAbi, unsigned short handle)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &handle; 
    params[0].m_nLength = 2;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__Start, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::StartCan(IABI* pAbi, Bus bus, CanTransceiver transceiverType, CanSpeed speed)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[3];
    params[0].m_pData = &bus; 
    params[0].m_nLength = 1;
    params[1].m_pData = &transceiverType; 
    params[1].m_nLength = 1;
    params[2].m_pData = &speed; 
    params[2].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 3, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__StartCan, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::StartCanExt(IABI* pAbi, Bus bus, CanTransceiver transceiverType, BYTE brp, BYTE tseg1, BYTE tseg2, BYTE sjw)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[6];
    params[0].m_pData = &bus; 
    params[0].m_nLength = 1;
    params[1].m_pData = &transceiverType; 
    params[1].m_nLength = 1;
    params[2].m_pData = &brp; 
    params[2].m_nLength = 1;
    params[3].m_pData = &tseg1; 
    params[3].m_nLength = 1;
    params[4].m_pData = &tseg2; 
    params[4].m_nLength = 1;
    params[5].m_pData = &sjw; 
    params[5].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 6, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__StartCanExt, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::StartFlexRay(IABI* pAbi, unsigned short payloadLength, BYTE* blob)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[2];
    params[0].m_pData = &payloadLength; 
    params[0].m_nLength = 2;
    params[1].m_pData = blob; 
    params[1].m_nLength = payloadLength;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 2, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__StartFlexRay, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::StartLin(IABI* pAbi, Bus bus, unsigned short payloadLength, BYTE* blob)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[3];
    params[0].m_pData = &bus; 
    params[0].m_nLength = 1;
    params[1].m_pData = &payloadLength; 
    params[1].m_nLength = 2;
    params[2].m_pData = blob; 
    params[2].m_nLength = payloadLength;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 3, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__StartLin, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::StartLinSpy(IABI* pAbi, Bus bus, unsigned int baudrate)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[2];
    params[0].m_pData = &bus; 
    params[0].m_nLength = 1;
    params[1].m_pData = &baudrate; 
    params[1].m_nLength = 4;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 2, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__StartLinSpy, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::StartMost(IABI* pAbi, MostMode mode)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &mode; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__StartMost, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::StartupMost(IABI* pAbi)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__StartupMost, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::Stop(IABI* pAbi, unsigned short handle)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &handle; 
    params[0].m_nLength = 2;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__Stop, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::StopCan(IABI* pAbi, Bus bus)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &bus; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__StopCan, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::StopFlexRay(IABI* pAbi)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__StopFlexRay, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::StopLin(IABI* pAbi, Bus bus)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &bus; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__StopLin, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::StopLinSpy(IABI* pAbi, Bus bus)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &bus; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)Board, (BYTE)FunId__StopLinSpy, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::StopMost(IABI* pAbi)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__StopMost, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::StressNicDeviceModeGet(IABI* pAbi, StressNicMode& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)StressNic, (BYTE)FunId__StressNicDeviceModeGet, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = (K2L::Automotive::ABI::StressNicMode)*pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::StressNicDeviceModeSet(IABI* pAbi, StressNicMode mode)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &mode; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)StressNic, (BYTE)FunId__StressNicDeviceModeSet, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::StressNicFunctionSet(IABI* pAbi, BYTE fblockId, unsigned short functionId, unsigned short payloadLength, BYTE* payload)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[4];
    params[0].m_pData = &fblockId; 
    params[0].m_nLength = 1;
    params[1].m_pData = &functionId; 
    params[1].m_nLength = 2;
    params[2].m_pData = &payloadLength; 
    params[2].m_nLength = 2;
    params[3].m_pData = payload; 
    params[3].m_nLength = payloadLength;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 4, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)StressNic, (BYTE)FunId__StressNicFunctionSet, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::StressNicGroupAddressGet(IABI* pAbi, unsigned short& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)StressNic, (BYTE)FunId__StressNicGroupAddressGet, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = *(unsigned short*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::StressNicGroupAddressSet(IABI* pAbi, unsigned short groupAddress)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &groupAddress; 
    params[0].m_nLength = 2;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)StressNic, (BYTE)FunId__StressNicGroupAddressSet, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::StressNicNodeAddressGet(IABI* pAbi, unsigned short& result)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    //ParamDef params[0];
    
    //pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 0, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)StressNic, (BYTE)FunId__StressNicNodeAddressGet, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    result = *(unsigned short*)pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::StressNicNodeAddressSet(IABI* pAbi, unsigned short nodeAddress)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &nodeAddress; 
    params[0].m_nLength = 2;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)StressNic, (BYTE)FunId__StressNicNodeAddressSet, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::StressNicSendMessage(IABI* pAbi, unsigned short deviceId, BYTE fblockId, BYTE instance, unsigned short functionId, BYTE opType, unsigned short payloadLength, BYTE* payload)
{   
    if (!pAbi) { return 0; }
    
    //if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[7];
    params[0].m_pData = &deviceId; 
    params[0].m_nLength = 2;
    params[1].m_pData = &fblockId; 
    params[1].m_nLength = 1;
    params[2].m_pData = &instance; 
    params[2].m_nLength = 1;
    params[3].m_pData = &functionId; 
    params[3].m_nLength = 2;
    params[4].m_pData = &opType; 
    params[4].m_nLength = 1;
    params[5].m_pData = &payloadLength; 
    params[5].m_nLength = 2;
    params[6].m_pData = payload; 
    params[6].m_nLength = payloadLength;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 7, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)StressNic, (BYTE)FunId__StressNicSendMessage, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::SwitchBypassMode(IABI* pAbi, BypassMode bypassMode)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[1];
    params[0].m_pData = &bypassMode; 
    params[0].m_nLength = 1;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 1, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)NetServices, (BYTE)FunId__SwitchBypassMode, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

int CABICommandsEx::WriteBusConfiguration(IABI* pAbi, Bus bus, unsigned short payloadLength, BYTE* configuration)
{   
    if (!pAbi) { return 0; }
    
    if (pAbi->IsSpyMode()) { return 0; }
     
    int resultLen = 0; 
    BYTE error = 0;
    
    ParamDef* pParams = NULL;
    ParamDef params[3];
    params[0].m_pData = &bus; 
    params[0].m_nLength = 1;
    params[1].m_pData = &payloadLength; 
    params[1].m_nLength = 2;
    params[2].m_pData = configuration; 
    params[2].m_nLength = payloadLength;
    
    pParams = params;
    
    int bufferLength = 0;
    BYTE* pBuffer = CResultParser::ToByteArray(pParams, 3, bufferLength);

    BYTE* pRes = pAbi->SendRequest((BYTE)TestMaster, (BYTE)FunId__WriteBusConfiguration, pBuffer, bufferLength, resultLen, error);
    delete pBuffer;
    
    if (!pRes)
	{ 
		pAbi->EndRequest();
		return -1; 
	}
    
    //result = pRes;
    
	pAbi->EndRequest();

    return error;
}

#pragma once

typedef struct ParamDef_t
{
    void* m_pData;
    size_t m_nLength;
} ParamDef;

class CResultParser
{
public:
    static BYTE* ToByteArray(ParamDef* pParams, int nParamCount, int& nResultLength);
};

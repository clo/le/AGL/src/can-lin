#pragma once
#include "windows-adapter.h"
#include "K2LABI.h"
#include "K2LIPC.h"
#include "SLock.h"
#include "ABIFilter.h"
#include "SEvent.h"
#define MAX_CONTEXT_OBJECT 256

using namespace K2L::Automotive::ABI;
using namespace K2L::Automotive::IPC;

class CABI : public IABI, public IIPCListener
{
public:
    CABI();
    ~CABI();

    int   Init                 (AbiIpcDevice* pInitData);
    int   Init                 (AbiIpcDevice* pInitData, int prio);
    int   Deinit               (void);
    void  Start                (IABIListener* pListener);
    void  Stop                 (void);
    void  RegisterBusContext   (Bus bus, void* context);
    void  UnregisterBusContext (Bus bus);
    void* GetBusContext        (Bus bus);
    bool  IsSpyMode            (void);
    void  SetSpyMode           (bool bSpyMode);
    BYTE* SendRequest(BYTE ifc, BYTE function, BYTE* data, int len, int& resultLen, BYTE& errorCode);
    BYTE* SendRequest(BYTE ifc, BYTE function, BYTE* data, int len, int& resultLen, BYTE& errorCode, int timeout);
    void EndRequest();

    void AddIdIntervalFilter    (Bus bus, unsigned short handle, unsigned int minId, unsigned int maxId);
    void AddMaskedIdFilter      (Bus bus, unsigned short handle, unsigned int id, unsigned int mask);
    void RemoveFilter           (unsigned short handle);
    
    void OnMessage(IIpc* pIpc, BYTE channel, BYTE errorCode, BYTE* pPacket, int nLen);
    void OnError(IIpc* pIpc);
    void OnMessageLost(IIpc* pIpc);
public:
    //Will be accessed by the IPC receive callback, which is in static context
    IIpc*         m_pIpcCommands;
    IIpc*         m_pIpcEvents;

private:
    inline void OnCommandResponse(IIpc* pIpc, BYTE channel, BYTE errorCode, BYTE* pPacket, int nLen);
    inline void OnEvent(IIpc* pIpc, BYTE channel, BYTE errorCode, BYTE* pPacket, int nLen);

    inline void DecodeCommon(BYTE* pPacket, int nLen, BYTE channel);
    inline void DecodeCanChannels(BYTE* pPacket, int nLen, Bus bus, BYTE protocol, BYTE eventType);
    inline void DecodeMostChannels(BYTE* pPacket, int nLen, Bus bus, BYTE protocol, BYTE eventType);
    inline void DecodeLinChannels(BYTE* pPacket, int nLen, Bus bus, BYTE protocol, BYTE eventType);
    inline void DecodeFlexRayChannels(BYTE* pPacket, int nLen, Bus bus, BYTE protocol, BYTE eventType);
    inline void DecodeRelayChannels(BYTE* pPacket, int nLen, Bus bus, BYTE protocol, BYTE eventType);
    inline void DecodeMostEthernetChannels(BYTE* pPacket, int nLen, Bus bus, BYTE protocol, BYTE eventType);
    
    inline int Init();
    inline BYTE ReadByte(BYTE*& pBuffer);
    inline unsigned short ReadUInt16(BYTE*& pBuffer);
    inline unsigned long ReadUInt32(BYTE*& pBuffer);
    inline unsigned long long ReadUInt64(BYTE*& pBuffer);
private:
    AbiIpcDevice* m_abiDevice;

    IABIListener* m_pAbiListener;
    bool          m_bStarted;
    bool          m_bSpyMode;
    void*         m_objectMap[MAX_CONTEXT_OBJECT];

    CEvent        m_requestEvent;
    CLock         m_lock;

    BYTE          m_errorCode;
    BYTE*         m_pResult;
    int           m_nResultLength;
    
    CABIFilter    m_filter;

    DWORD         m_dwUniqueId;
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// K2L GmbH
///
/// \file
///          This file contains the api of all abi commands.
///
/// \ingroup ABICommands
///
/// \par     COPYRIGHT (c) 2000-2010 by K2L GmbH
///          All rights reserved.
///
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///          Alle Rechte an der Software verbleiben bei der K2L GmbH.
///
///          Die K2L GmbH raeumt dem Lizenznehmer nach vollstaendiger
///          Bezahlung der Lizenzgebuehr ein nicht ausschliessliches,
///          unwiderrufliches, nicht uebertragbares, zeitlich und geografisch
///          nicht beschraenktes Recht zur Nutzung der jeweils
///          auftragsgegenstaendlichen Software innerhalb des Lizenznehmers ein.
///
///          Die Weitergabe des Nutzungsrechts durch den Lizenznehmer ist auf
///          dessen Zulieferer beschraenkt. Die Zulieferer sind zu verpflichten,
///          die Software nur im Rahmen von Projekten fuer den Lizenznehmer
///          zu verwenden; weitere Rechte der Zulieferer sind auszuschliessen.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//@{
#pragma once

#include "ABICommandsEx.h"

namespace K2L
{
    namespace Automotive
    {
        namespace ABI
        {

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \defgroup OBJ Object control
/// \ingroup ABICommands
/// This group combines all commands needed to manipulate Sender or Receiver Objects in the device.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \defgroup HW Hardware Access
/// \ingroup ABICommands
/// This group combines all commands which belongs directly to the hardware.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \defgroup MOST MOST
/// \ingroup ABICommands
/// This group combines all commands which belongs to the MOST bus of the ABI device.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \defgroup CAN CAN
/// \ingroup ABICommands
/// This group combines all commands which belongs to the CAN bus of the ABI device.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \defgroup CANTP CAN Transport Protocol
/// \ingroup ABICommands
/// This group combines all commands which belongs to the CAN transport protocol, which is located on top of the CAN protocol. 
/// Therefor make sure the CAN bus is already started with the commands in CAN.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \defgroup FR FlexRay
/// \ingroup ABICommands
/// This group combines all commands which belongs to the FlexRay bus of the ABI device.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \defgroup LIN LIN
/// \ingroup ABICommands
/// This group combines all commands which belongs to the LIN bus of the ABI device.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \defgroup EXT ABI Extended Commands
/// \ingroup ABICommands
/// This group provides a generic way to set or get properties to the device. Like a hashtable, the key / value pair is a String.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \defgroup ABICommands ABI Commands
/// This class is responsible for all the communication between the PC and the device. For better handling the commands of this class are grouped by their functionality.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief This class is responsible for all the communication between the PC and the device. For better handling the commands of this class are grouped by their functionality.
/// \ingroup ABICommands
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class K2LABI_API CABICommands : public CABICommandsEx
{
public:
    /// Adds a receiving rule into the current group. Only the added messages will be reported to the PC.
    /// \param[in] abi Used IABI instance.
    /// \param[in] handle Handle of the receiving rule.
    /// \param[in] bus Receiving bus.
    /// \param[in] param1 Parameter 1.
    /// \param[in] param2 Parameter 2.        
    /// \note MOST: param1: mask (0 means all ids; 0xFFFFFFFF means this id). param2: message identifier value.
    /// \note MOST ADS: param1 and param2 are not used. It is only a receiver required.
    /// \note FlexRay: param1: mask (0 means all ids; 0xFFFFFFFF means this id). param2: message identifier value.
    /// \note CAN: param1: minimal message identifier. param2: maximal identifier.
    /// \note LIN: param1: message identifier (0xFF means all ids). param2: Not used.
    /// \note LINTP: param1: Node address (0xFF means all NADs). param2: Not used.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup OBJ
    static int AddReceiverEx(IABI* pAbi, unsigned short handle, Bus bus, unsigned int param1, unsigned int param2);
    
    /// Removes a receiving rule from the current group.
    /// \param[in] abi"> Used IABI instance.
    /// \param[in] handle"> Handle of the receiving rule.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup OBJ
    static int RemoveReceiverEx(IABI* pAbi, unsigned short handle);
    
    /// Sends a message immediately to MOST asynchronous channel with MOST HIGH protocol.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] handle Handle of tx message.
    /// \param[in] targetAddress Target address.
    /// \param[in] fblockID The Function Block identifier.
    /// \param[in] instanceID The Function Block instance number.
    /// \param[in] functionID The function identifier of the FBlock.
    /// \param[in] opType The Function Block operation type identifier.
    /// \param[in] payloadLength Length of payload.
    /// \param[in] payload Payload data.
    /// \note The maximum length of message is 65535 bytes.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int SendMostHighMessage(IABI* pAbi, unsigned short handle, unsigned short targetAddress, BYTE fblockID, BYTE instanceID, unsigned short functionID, BYTE opType, unsigned short payloadLength, BYTE* payload);

    /// Sends a message immediately to MOST asynchronous channel with ADS protocol.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] handle Handle of tx message.
    /// \param[in] targetAddress Target address.
    /// \param[in] payloadLength Length of payload.
    /// \param[in] payload Payload data.
    /// \note The maximum length of message is 1014 bytes.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int SendMostADSMessage(IABI* pAbi, unsigned short handle, unsigned short targetAddress, unsigned short payloadLength, BYTE* payload);

    /// Sends a message immediately to MOST control channel with AMS protocol.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] handle Handle of tx message.
    /// \param[in] targetAddress Target address.
    /// \param[in] fblockID The Function Block identifier.
    /// \param[in] instanceID The Function Block instance number.
    /// \param[in] functionID The function identifier of the FBlock.
    /// \param[in] opType The Function Block operation type identifier.
    /// \param[in] payloadLength Length of payload.
    /// \param[in] payload Payload data.
    /// \note The maximum length of message is 4096 bytes.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int SendMostAMSMessage(IABI* pAbi, unsigned short handle, unsigned short targetAddress, BYTE fblockID, BYTE instanceID, unsigned short functionID, BYTE opType, unsigned short payloadLength, BYTE* payload);

    /// Sends a message immediately to MOST control channel with CMS protocol.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] handle Handle of tx message.
    /// \param[in] targetAddress Target address.
    /// \param[in] fblockID The Function Block identifier.
    /// \param[in] instanceID The Function Block instance number.
    /// \param[in] functionID The function identifier of the FBlock.
    /// \param[in] opType The Function Block operation type identifier.
    /// \param[in] payloadLength Length of payload.
    /// \param[in] payload The first byte (MOST25/50) / The first 2 bytes (MOST150) have to be the TelId and TelLen. Afterwards following the payload of the message.
    /// \note The maximum size of payload (all after TelLen) is 12 bytes (MOST25/50) or 45 bytes (MOST150).
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int SendMostCMSMessage(IABI* pAbi, unsigned short handle, unsigned short targetAddress, BYTE fblockID, BYTE instanceID, unsigned short functionID, BYTE opType, unsigned short payloadLength, BYTE* payload);

    /// Sends a message immediately to MOST control channel with CMS protocol.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] handle Handle of tx message.
    /// \param[in] targetAddress Target address.
    /// \param[in] fblockId The Function Block identifier.
    /// \param[in] instanceId The Function Block instance number.
    /// \param[in] functionId The function identifier of the FBlock.
    /// \param[in] opType The Function Block operation type identifier.
    /// \param[in] telId The telegram identifier.
    /// \param[in] telLen The telegram length (including the frame count).
    /// \param[in] isMost150 true if MOST 150 is used (Telegram information is 2 bytes long).
    /// \param[in] frameCount The frame count.
    /// \param[in] isSegmented true if its a message segment. If set to false frameCount is ignored.
    /// \param[in] payloadLength Length of payload.
    /// \param[in] payload The payload of the message.
    /// \note The maximum size of payload (all after TelLen) is 12 bytes (MOST25/50) or 45 bytes (MOST150).
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    //static int SendCmsMessage(IABI* pAbi, unsigned short handle, unsigned short targetAddress, BYTE fblockId, BYTE instanceId, unsigned short functionId, BYTE opType, BYTE telId, BYTE telLen, bool isMost150, BYTE frameCount, bool isSegmented, unsigned short payloadLength, BYTE* payload);
    
    /// Sends a message immediately to CAN.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] handle Handle of tx message.
    /// \param[in] channelNumber Selected CAN channel number.
    /// \param[in] id Object identifier (11 bit).
    /// \param[in] payloadLength Length of payload.
    /// \param[in] payload Payload data.
    /// \note The maximum length of message is 8 bytes.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup CAN
    static int SendCANMessage(IABI* pAbi, unsigned short handle, BYTE channelNumber, unsigned short id, unsigned short payloadLength, BYTE* payload);

    /// Sends a message immediately to CAN with extended identifiers.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] handle Handle of tx message.
    /// \param[in] channelNumber Selected CAN channel number.
    /// \param[in] id Object identifier (29 bit).
    /// \param[in] payloadLength Length of payload.
    /// \param[in] payload Payload data.
    /// \note The maximum length of message is 8 bytes.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup CAN
    static int SendXCANMessage(IABI* pAbi, unsigned short handle, BYTE channelNumber, unsigned int id, unsigned short payloadLength, BYTE* payload);

    /// Sends a message immediately to LIN.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] handle Handle of tx message.
    /// \param[in] channelNumber Selected LIN channel number.
    /// \param[in] id Object identifier.
    /// \param[in] payloadLength Length of payload.
    /// \param[in] payload Payload data.
    /// \note The maximum length of message is 8 bytes.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup LIN
    static int SendLINMessage(IABI* pAbi, unsigned short handle, BYTE channelNumber, BYTE id, unsigned short payloadLength, BYTE* payload);

    /// Sends a message immediately to LIN TP.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] handle Handle of tx message.
    /// \param[in] channelNumber Selected LIN channel number.
    /// \param[in] frameId Identifier of message. Request is 60 and response is 61.
    /// \param[in] nodeAddress Address of the target node.
    /// \param[in] serviceId Identifier of the service.
    /// \param[in] payloadLength Length of payload.
    /// \param[in] payload Payload data.
    /// \note The maximum length of message is 4095 bytes.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup LIN
    static int SendLINTpMessage(IABI* pAbi, unsigned short handle, BYTE channelNumber, BYTE frameId, BYTE nodeAddress, BYTE serviceId, unsigned short payloadLength, BYTE* payload);

    /// Sends a message immediately to FlexRay.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] handle Handle of tx message.
    /// \param[in] frChannel Channel identifier.
    /// \param[in] cycleOffset Offset of cycle.
    /// \param[in] cycleRepetition Repetition of cycle.
    /// \param[in] frameID Frame identifier.
    /// \param[in] payloadLength Length of payload.
    /// \param[in] payload Payload data.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup FR
    static int SendFRMessage(IABI* pAbi, unsigned short handle, FrChannel frChannel, BYTE cycleOffset, BYTE cycleRepetition, unsigned short frameID, unsigned short payloadLength, BYTE* payload);
    
    /// Reads out the startup configuration stored in the EEPROM for the selected bus.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bus The bus, which configuration should be read out.
    /// \param[out] configuration Blob containing the startup configuration.
    /// \param[out] length Length of the configuration blob.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup HW
    static int ReadBusConfiguration(IABI* pAbi, Bus bus, BYTE*& configuration, WORD& length);

    /// Gets all used object handles.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] handles All used object handles. Each handle consists of two bytes (low|high).
    /// \param[out] length Length of list.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup OBJ
    static int GetAllUsedHandles(IABI* pAbi, BYTE*& handles, WORD& length);

    /// Gets information of an object.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] handle The handle that should be read out.
    /// \param[out] type Type of object.
    /// \param[out] isStarted If set to true then is started.
    /// \param[out] groupId The group id of the object.
    /// \param[out] bus The bus id of the object.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup OBJ
    static int GetInformationOfObject( IABI* pAbi, WORD handle, ObjectType& type, bool& isStarted, BYTE& groupId, Bus& bus);
    
    /// Gets all supported can speed of this channel/transceiver combination.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bus The bus that should be read out.
    /// \param[in] type The type that should be read out.
    /// \param[out] speeds List of all supported speeds of this channel/transceiver combination.
    /// \param[out] length Length of list.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup CAN
    static int GetSupportedCanSpeeds( IABI* pAbi, Bus bus, CanTransceiver type, CanSpeed*& speeds, WORD& length);

    /// Gets all supported can transceivers of a CAN channel.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bus The bus that should be read out.
    /// \param[out] types List of all supported can transceivers of a CAN channel.
    /// \param[out] length Length of list.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup CAN
    static int GetSupportedCanTransceivers( IABI* pAbi, Bus bus, CanTransceiver*& types, WORD& length);

    /// Gets the most allocation table from the SPYNIC.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] length Length of arrays connectionLabels and channelWidths.
    /// \param[out] connectionLabels List of connection labels.
    /// \param[out] channelWidths List of channel widths. Each entry in list are coppeled to the same position in connectionLabels.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \remarks Only available for MOST50.
    /// \ingroup MOST
    static int GetMostAllocationTable( IABI* pAbi, WORD& length, AllocationTableEntry*& allocationTable);
    
    /// Sends a byte array to the firmware. The firmware will answer with the same array.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] requestLength Length of request array.
    /// \param[in] request The request byte array.
    /// \param[out] answerLength Length of answer array.
    /// \param[out] answer The answer byte array.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup HW
    static int SendEchoCommand( IABI* pAbi, WORD requestLength, BYTE* request, WORD& answerLength, BYTE*& answer);
    
    /// Gets used connection labels on transmission side.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] length Length of label array.
    /// \param[out] labels List of all used transmission labels on tx side of audio streaming. Length is 0 if no connection label is used.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int GetMostAudioTxLabels( IABI* pAbi, WORD& length, LabelEntry*& labels);

    /// Gets used connection labels on receive side.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] length Length of label array.
    /// \param[out] labels List of all used transmission labels on tx side of audio streaming. Length is 0 if no connection label is used.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int GetMostAudioRxLabels( IABI* pAbi, WORD& length, LabelEntry*& labels);
    
    /// Frees the memory which was internaly allocated by the ABI library to report a string.
    /// \param[in] ptr Pointer to a string returned by the ABI library. (Like GetSupportedFeature, e.g.)
    /// \ingroup OBJ
    static void ReleaseBuffer(char** ptr);

    /// Frees the memory which was internaly allocated by the ABI library to report a word array.
    /// \param[in] ptr Pointer to a word returned by the ABI library. (Like GetMostTxLabels, e.g.)
    /// \ingroup OBJ
    static void ReleaseBuffer(LabelEntry** ptr);
    
    /// Frees the memory which was internaly allocated by the ABI library to report a blob.
    /// \param[in] ptr Pointer to a byte array returned by the ABI library. (Like ReadBusConfiguration, e.g.)
    /// \ingroup OBJ
    static void ReleaseBuffer(BYTE** ptr);

    /// Frees the memory which was internaly allocated by the ABI library to report an allocation table.
    /// \param[in] ptr Pointer to a AllocationTableEntry array returned by the ABI library. (Like GetMostAllocationTable, e.g.)
    /// \ingroup OBJ
    static void ReleaseBuffer(AllocationTableEntry** ptr);

    /// Frees the memory which was internaly allocated by the ABI library.
    /// \param[in] ptr Pointer to a CanSpeed array returned by the ABI library. (Like GetSupportedCanSpeeds, e.g.)
    /// \ingroup OBJ
    static void ReleaseBuffer(CanSpeed** ptr);

    /// Frees the memory which was internaly allocated by the ABI library.
    /// \param[in] ptr Pointer to a CanTransceiver array returned by the ABI library. (Like GetSupportedCanTransceivers, e.g.)
    /// \ingroup OBJ
    static void ReleaseBuffer(CanTransceiver** ptr);
    
    /// Frees the memory which was internaly allocated by the ABI library.
    /// \param[in] ptr Pointer to a AudioConnectionType array returned by the ABI library. (Like GetMostAudioSupportedTxConnectors, e.g.)
    /// \ingroup OBJ
    static void ReleaseBuffer(AudioConnectionType** ptr);

    /// Stream an audio stream from the MOST ring to the SPDIF out connector of the MOCCA compact. If the SPDIF connector is already used for another label it is disconnected and connected to the given label.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bandwidth Desired bandwidth of the audio channel.
    /// \param[in] label Label that is used.
    /// \param[in] If set to true a switch is forced. Otherwise the function will return an error if the rx connector is currently in use.
    /// \param[out] errorCode The error code. This code is only valid if return value is TMErrorCode tmRetErrorDuringExecution.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note This function will first return if the whole command is executed.
    /// \ingroup MOST
    static int MostAudioRxConnectSync(IABI* pAbi, AudioBandwidth bandwidth, unsigned int label, bool isSwitchForced, WORD& errorCode);
    
    /// Disconnect the audio streaming from SPDIF out from channel.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] label Label that is used.
    /// \param[out] errorCode The error code. This code is only valid if return value is TMErrorCode tmRetErrorDuringExecution.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note This function will first return if the whole command is executed.
    /// \ingroup MOST
    static int MostAudioRxDisconnectSync(IABI* pAbi, unsigned int label, WORD& errorCode);
    
    /// Allocate a new audio streaming channel and stream the SPDIF in connector of the MOCCA compact to the MOST ring.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bandwidth Desired bandwidth of the allocated audio channel.
    /// \param[out] connectionLabel The connection label. The connection label is only valid if return value is tmRetValOk.
    /// \param[out] errorCode The error code. This code is only valid if return value is tmRetErrorDuringExecution.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note This function will first return if the whole command is executed.
    /// \ingroup MOST
    static int MostAudioTxAllocateConnectSync(IABI* pAbi, AudioBandwidth bandwidth, unsigned int& connectionLabel, WORD& errorCode);
        
    /// Allocate a new audio streaming channel and stream the SPDIF in connector of the MOCCA compact to the MOST ring.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bandwidth Desired bandwidth of the allocated audio channel.
    /// \param[in] connector The connector that should be used.
    /// \param[out] connectionLabel The connection label. The connection label is only valid if return value is tmRetValOk.
    /// \param[out] errorCode The error code. This code is only valid if return value is tmRetErrorDuringExecution.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note This function will first return if the whole command is executed.
    /// \ingroup MOST
    static int MostAudioTxAllocateConnectSync(IABI* pAbi, AudioBandwidth bandwidth, AudioConnectorType connector, unsigned int& connectionLabel, WORD& errorCode);

    /// Disconnect the audio streaming from SPDIF in from channel.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] label Label that is used.
    /// \param[out] errorCode The error code. This code is only valid if return value is TMErrorCode tmRetErrorDuringExecution.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note This function will first return if the whole command is executed.
    /// \ingroup MOST
    static int MostAudioTxDisconnectSync(IABI* pAbi, unsigned int label, WORD& errorCode);

    /// Gets the extended baudrate of a CAN channel.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] busId The bus that should be read out.
    /// \param[out] prescaler The prescaler parameter.
    /// \param[out] tseg1 The phase1 segment parameter.
    /// \param[out] tseg2 The phase2 segment parameter.
    /// \param[out] sjw The synchronization jump width parameter.
    /// \return Status message if operation succeeded.
    /// \ingroup CAN
    static int GetExtendedBaudrate(IABI* pAbi, Bus busId, unsigned int& prescaler, unsigned int& tseg1, unsigned int& tseg2, unsigned int& sjw);
            
    /// Gets the hardware revision.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] hwType Hardware Type. (0 is undefined, 1 is MOCCA compact)
    /// \param[out] major Major version.
    /// \param[out] minor Minor version.
    /// \param[out] bugfix Bugfix version.
    /// \return Status message if operation succeeded.
    /// \ingroup HW
    static int GetHardwareRevision(IABI* pAbi, BYTE& hwType, BYTE& major, BYTE& minor, BYTE& bugfix);

    /// Get the channels of MOST Spy that are activated.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] status Status channel.
    /// \param[out] control Control channel.
    /// \param[out] async Asynchronous channel.
    /// \param[out] sync Synchronous channel.
    /// \return Status message if operation succeeded.
    /// \ingroup MOST
    static int MostSpyEnabled(IABI* pAbi, bool& status, bool& control, bool& async, bool& sync);
    
    /// Gets the global parameters for CAN TP Rx.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] blockSize Flow Control block size.
    /// \param[out] separationTime Minimum time gap between the transmission of CF.
    /// \param[out] consecutiveFrameTimeout Timeout for the next consecutive frame.
    /// \return Status message if operation succeeded.
    /// \ingroup CAN
    static int GetCanTpRxConfig(IABI* pAbi, BYTE& blockSize, BYTE& separationTime, WORD& consecutiveFrameTimeout);

    /// Gets the global parameters for CAN TP Tx.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] flowControlTimeout Timeout interval for flow control.
    /// \param[out] clearToSendTimeout Minimum time between two tx messages.
    /// \return Status message if operation succeeded.
    /// \ingroup CAN
    static int GetCanTpTxConfig(IABI* pAbi, WORD& flowControlTimeout, WORD& clearToSendTimeout);

    /// Gets CAN TP pattern to fill CAN telegrams up to 8 bytes.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] pattern Content of fill bytes.
    /// \return Status message if operation succeeded.
    /// \ingroup CAN
    static int GetCanTpPattern(IABI* pAbi, BYTE& pattern);

    /// Get the supported audio connectors for TX.
    /// \param[in] abi Used IABI instance.
    /// \param[out] length Length of connector array.
    /// \param[out] types Supported connectors.
    /// Status message if operation succeeded.
    /// \ingroup HW
    static int GetMostAudioSupportedTxConnectors(IABI* pAbi, WORD& length, AudioConnectorType*& types);

    /// Retrieves all supported features in a space seperated string.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result Feature list.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup HW
    static int GetSupportedFeatures(IABI* pAbi, char*& result);
    
    /// Sends a get command to the StressNIC.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] fblockId FBlock identifier of command.
    /// \param[in] functionId Function identifier of command.
    /// \param[out] length Payloadlength.
    /// \param[out] payload Payload of function.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int StressNicFunctionGet(IABI* pAbi, BYTE fblockId, unsigned short functionId, WORD& length, BYTE*& payload);
    
    /// Sends a get command to the StressNIC.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] fblockId FBlock identifier of command.
    /// \param[in] functionId Function identifier of command.
    /// \param[in] requestLength Payload length of request.
    /// \param[in] request Payload of request.
    /// \param[out] responseLength Payload length of response.
    /// \param[out] response Payload of response.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int StressNicFunctionGetExtended(IABI* pAbi, BYTE fblockId, unsigned short functionId, WORD requestLength, BYTE* request, WORD& responseLength, BYTE*& response);

    /// Get the volume of the selected audio connector.
    /// \param[in] abi Used IABI instance.
    /// \param[in] connector The selected audio connector type.
    /// \param[in] isInput true if input volume should be modified, otherwise output volume is modified.
    /// \param[out] volumeLeft Attenuation(-)/Gain(+) in db of left channel.
    /// \param[out] volumeRight Attenuation(-)/Gain(+) in db of right channel.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int GetVolume(IABI* pAbi, AudioConnectorType connector, bool isInput, int& volumeLeft, int& volumeRight);
    
    /// Get the supported volume range of the selected audio connector.</summary>
    /// \param[in] abi Used IABI instance.
    /// \param[in] connector The selected audio connector type.
    /// \param[in] isInput true if input volume should be modified, otherwise output volume is modified.
    /// \param[out] minVolume Minimum supported Attenuation(-)/Gain(+) in db.
    /// \param[out] maxVolume Maximum supported Attenuation(-)/Gain(+) in db.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int GetSupportedVolumeRange(IABI* pAbi, AudioConnectorType connector, bool isInput, int& minVolume, int& maxVolume);

    /// Sends a sequence to the ECL/Trigger line. Timestamps are absolute.</summary>
    /// \param[in] abi Used IABI instance.
    /// \param[in]length Length of sequence.
    /// \param[in]sequence Sequence of high and low commands.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup HW
    static int SendAbsoluteTriggerSequence(IABI* pAbi, WORD length, AbsoluteTriggerCommand* sequence);

    /// Sends a sequence to the ECL/Trigger line. Timestamps are relative to the start of transmission.</summary>
    /// \param[in] abi Used IABI instance.
    /// \param[in]length Length of sequence.
    /// \param[in]sequence Sequence of high and low commands.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup HW
    static int SendRelativeTriggerSequence(IABI* pAbi, WORD length, RelativeTriggerCommand* sequence);
    };
        }
    }
}
//@}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// END OF FILE                                                                
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "k2l-type.h"
#include "K2LIPC.h"
#include "IPCFlowDecoder.h"
#include "Ipc.h"
#include <string.h>

//-----------------------------------------------------------------------------
class CIpcListener : public IIPCListener
{
public:
    CIpcListener() : messageCallback(0)
                        , errorCallback(0)
                        , messageLostCallback(0)
    {
    }

    virtual ~CIpcListener() {}

    virtual void OnMessage(IIpc* pIpc, BYTE channel, BYTE errorCode, BYTE* pPacket, int nLen)
    {
        if (messageCallback)
        {
            messageCallback(pIpc, channel, errorCode, pPacket, nLen);
        }
    }
    
    virtual void OnError(IIpc* pIpc)
    {
        if (errorCallback)
        {
            errorCallback(pIpc);
        }
    }
    
    virtual void OnMessageLost(IIpc* pIpc)
    {
        if (messageLostCallback)
        {
            messageLostCallback(pIpc);
        }
    }

    FCT_OnMessageEvent messageCallback;
    FCT_OnEvent errorCallback;
    FCT_OnEvent messageLostCallback;
};

//-----------------------------------------------------------------------------
CIpc::CIpc() : m_pResultBuffer(0)
                   , m_nResultLen(0)
                   , m_pIpcListener(0)
                   , m_pFlowDecoder(NULL)
{
}

//-----------------------------------------------------------------------------
CIpc::~CIpc()
{
}

//-----------------------------------------------------------------------------
IIPCListener* CIpc::GetListener()
{
    return m_pIpcListener;
}

//-----------------------------------------------------------------------------
int CIpc::Init(IIPCListener* pListener)
{
    CSafeLock safeLock(&m_lock);
    
    m_pFlowDecoder = new CIPCFlowDecoder(this);
    m_pIpcListener = pListener;

    return 0;
}

//-----------------------------------------------------------------------------
int CIpc::Deinit()
{
    CSafeLock safeLock(&m_lock);

    m_pIpcListener = NULL;
    delete m_pFlowDecoder;
    m_pFlowDecoder = NULL;

    return 0;
}

//-----------------------------------------------------------------------------
void CIpc::Decode(BYTE* pBuffer, DWORD nLen)
{
    CSafeLock safeLock(&m_lock);

    if (m_pFlowDecoder)
        m_pFlowDecoder->DecodeFlow(pBuffer, nLen);
}

//-----------------------------------------------------------------------------
int CIpc::FormatRequest(BYTE channel, BYTE ifaceId, BYTE funId, DWORD dwSenderID, const BYTE *pData, DWORD dataLen, BYTE* pResult, int nResultLen, int* nResultLenNeeded)
{
    int nRet = -1;

    if (!nResultLenNeeded)
        return -1;

    *nResultLenNeeded = 0;
    int nTotalLength = sizeof(CIpcPacket) + sizeof(CRequest) + dataLen;

    if (nTotalLength <= nResultLen)
    {
        if ((dataLen & 1) == 0 && pResult)
        {
            BYTE* buffer = pResult;
            memset(buffer, 0, nTotalLength);
            CIpcPacket* pIpcPacket = reinterpret_cast<CIpcPacket*>(buffer);
            CRequest* pRequest = reinterpret_cast<CRequest*>(pIpcPacket + 1);

            pIpcPacket->m_head = 0x04;
            pIpcPacket->m_channel = channel;

            pIpcPacket->m_length = sizeof(CRequest) + dataLen;

            pRequest->m_interface = ifaceId;
            pRequest->m_function = funId;
            
            if (pData != NULL && dataLen > 0)
            {
                memcpy(reinterpret_cast<BYTE*>(pRequest + 1), pData, dataLen);
            }
            
            pRequest->m_requestId = dwSenderID;

            BYTE checkSum = 0;
            for (int i = 0; i < sizeof(CIpcPacket) - 1; i++)
            {
                checkSum += buffer[i];
            }
            pIpcPacket->m_hdrCrc = checkSum;
            nRet = nTotalLength;
        }
        else
        {
            nRet = -1;
        }
    }
    else
    {
        nRet = -1;
        *nResultLenNeeded = nTotalLength;
    }
    
    return nRet;
}

//-----------------------------------------------------------------------------
void CIpc::OnPacket(CIpcPacket* pPacket)
{
    if (!m_pIpcListener)
        return;

    BYTE* pBuffer = (BYTE*)(pPacket + 1);
    m_pIpcListener->OnMessage(this, pPacket->m_channel, pPacket->m_errorCode, pBuffer, pPacket->m_length);
}

//-----------------------------------------------------------------------------
IIPCListener* CIpc::CreateListener(FCT_OnMessageEvent callback, FCT_OnEvent error, FCT_OnEvent overflow)
{
    CIpcListener* pListener = new CIpcListener;
    pListener->messageCallback = callback;
    pListener->errorCallback = error;
    pListener->messageLostCallback = overflow;
    return pListener;
}

//-----------------------------------------------------------------------------
void CIpc::DestroyListener()
{
    delete m_pIpcListener;
    m_pIpcListener = NULL;
}

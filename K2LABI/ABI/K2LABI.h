/*=== K2L GmbH ===============================================================*/
/**
\file
            This file contains the API of the \ref ABI interface.

\ingroup    ABI

\par        COPYRIGHT (c) 2000-2011 by K2L GmbH
            All rights reserved.
*/
/*==============================================================================
            Alle Rechte an der Software verbleiben bei der K2L GmbH.

            Die K2L GmbH raeumt dem Lizenznehmer nach vollstaendiger
            Bezahlung der Lizenzgebuehr ein nicht ausschliessliches,
            unwiderrufliches, nicht uebertragbares, zeitlich und geografisch
            nicht beschraenktes Recht zur Nutzung der jeweils
            auftragsgegenstaendlichen Software innerhalb des Lizenznehmers ein.

            Die Weitergabe des Nutzungsrechts durch den Lizenznehmer ist auf
            dessen Zulieferer beschraenkt. Die Zulieferer sind zu verpflichten,
            die Software nur im Rahmen von Projekten fuer den Lizenznehmer
            zu verwenden; weitere Rechte der Zulieferer sind auszuschliessen.
===HEADER END=================================================================*/
//@{
#pragma once

#include "k2l-type.h"

/// \defgroup ABI Automotive Bus Interface
/// Detailed overview of the ABI library.

namespace K2L
{
    namespace Automotive
    {
        namespace ABI
        {
    
/// Bus types.
/// \ingroup ABI
typedef enum
{
    UnknownBusId = 0x00,        ///< Unknown channel.

    CAN1 = 0x10,                ///< CAN Channel 1.
    CAN2 = 0x11,                ///< CAN Channel 2.
    CAN3 = 0x12,                ///< CAN Channel 3.
    CAN4 = 0x13,                ///< CAN Channel 4.
    CAN5 = 0x14,                ///< CAN Channel 5.
    CAN6 = 0x15,                ///< CAN Channel 6.

    MOST1 = 0x20,               ///< MOST Channel 1 (DEPRECATED).
    MOST_CONTROL1_AMS = 0x20,   ///< MOST Channel 1 of control channel (AMS). It's also used for all MOST bus events.
    
    MOSTHIGH1 = 0x30,           ///< MOST High Channel 1 (DEPRECATED).
    MOST_ASYNC1_HIGH = 0x30,    ///< MOST Channel 1 of asynchronous channel (MOST HIGH).
    
    FLEXRAY1 = 0x40,            ///< FlexRay Channel 1.
    
    LIN1 = 0x50,                ///< LIN Channel 1.
    LIN2 = 0x51,                ///< LIN Channel 2.
    LIN3 = 0x52,                ///< LIN Channel 3.
    LIN4 = 0x53,                ///< LIN Channel 4.
    LIN5 = 0x54,                ///< LIN Channel 5.
    LIN6 = 0x55,                ///< LIN Channel 6.
    
    XCAN1 = 0x60,               ///< CAN Extended Channel 1.
    XCAN2 = 0x61,               ///< CAN Extended Channel 2.
    XCAN3 = 0x62,               ///< CAN Extended Channel 3.
    XCAN4 = 0x63,               ///< CAN Extended Channel 4.
    XCAN5 = 0x64,               ///< CAN Extended Channel 5.
    XCAN6 = 0x65,               ///< CAN Extended Channel 6.

    LINTP1 = 0x70,              ///< LIN TP Channel 1.
    LINTP2 = 0x71,              ///< LIN TP Channel 2. 
    LINTP3 = 0x72,              ///< LIN TP Channel 3.
    LINTP4 = 0x73,              ///< LIN TP Channel 4. 
    LINTP5 = 0x74,              ///< LIN TP Channel 5.
    LINTP6 = 0x75,              ///< LIN TP Channel 6.

    MOST_CONTROL1_RAW = 0x80,   ///< MOST Channel 1 of raw control channel (CMS).

    MOST_ASYNC1_RAW = 0x90,     ///< MOST Channel 1 of raw asynchronous channel (ADS).,

    MOST_SYNC1 = 0xA0,          ///< MOST Synchronous Channel 1.

    RELAY1 = 0xB0,              ///< Relay 1.

    LINRAW1 = 0xC0,             ///< LIN Raw Channel 1.
    LINRAW2 = 0xC1,             ///< LIN Raw Channel 2.
    LINRAW3 = 0xC2,             ///< LIN Raw Channel 3.
    LINRAW4 = 0xC3,             ///< LIN Raw Channel 4.
    LINRAW5 = 0xC4,             ///< LIN Raw Channel 3.
    LINRAW6 = 0xC5,             ///< LIN Raw Channel 4.
    
    MOST_ASYNC1_MEP = 0xD0      ///< MOST Channel 1 of asynchronous ethernet channel (MEP).
} Bus;

/// Bus types.
/// \ingroup ABI
typedef enum 
{
    CAN = 1,                ///< Controller Area Network.
    MOST = 2,               ///< MOST (DEPRECATED).
    MOST_CONTROL = 2,       ///< MOST control channel.
    MOSTHIGH = 3,           ///< MOST high (DEPRECATED).
    MOST_ASYNC = 3,         ///< MOST asynchronous channel.
    FLEXRAY = 4,            ///< FlexRay.
    LIN = 5,                ///< Local Interconnect Network.
    XCAN = 6,               ///< CAN Extended.
    LINTP = 7,              ///< LIN Transport Protocol. 
    MOST_CONTROL_RAW = 8,   ///< MOST raw control channel.
    MOST_ASYNC_RAW = 9,     ///< MOST raw asnychronous channel.
    MOST_SYNC = 10,         ///< MOST synchronous channel.
    RELAY = 11,             ///< Relay.
    LIN_RAW = 12,           ///< LIN raw channel.
    MOST_ASYNC_MEP = 13     ///< MOST MEP asnychronous channel.
} BusType;

/// Signal types for sweeping tx data.
/// \ingroup ABI
typedef enum
{
    sigLines = 0,   ///< The signal looks like sequence of ascending/descending values.
    sigZigzag = 1,  ///< The signals look like sequence of ascending and then descending values.
    sigBitShift = 2 ///< First sequence value of this signal type is 0, then goes value with all bit set to 1. And then goes values, that are degree of 2, i.e. 1, 2, 4, 8 and so on. After this the sequence repeats from the beginning.
} SignalType;

/// Object type of an ABI object.
/// \ingroup ABI
typedef enum
{
    NullObject = 0x00,              ///< NULL object.
    ReceiverObject = 0x01,          ///< Common receiver object.
    TransmitterObject = 0x02,       ///< Common transmitter object.
    CanTpReceiverObject = 0x03,     ///< CAN TP receiver object.
    CanTpTransmitterObject = 0x04   ///< CAN TP transmitter object.
} ObjectType;

/// Object state of an ABI object.
/// @ingroup ABI
typedef enum
{
    Created = 0x00, ///< Object has been created.
    Started = 0x01, ///< Object has been started.
    Stopped = 0x02, ///< Object has been stopped.
    Deleted = 0x03  ///< Object has been deleted.
} ObjectState;

/// Describes what kind of internal event occured.
/// \ingroup ABI
typedef enum
{
    paramTxMostQueueOverflow = 0,               ///< The MOST TX queue has overflowed.
    paramRxMostQueueOverflow = 1,               ///< The MOST RX queue has overflowed.
    paramTxCan1QueueOverflow = 2,               ///< The CAN1 TX queue has overflowed.
    paramTxCan2QueueOverflow = 3,               ///< The CAN2 TX queue has overflowed.
    paramTxCan3QueueOverflow = 4,               ///< The CAN3 TX queue has overflowed.
    paramTxCan4QueueOverflow = 5,               ///< The CAN4 TX queue has overflowed.
    paramRxCan1QueueOverflow = 6,               ///< The CAN1 RX queue has overflowed.
    paramRxCan2QueueOverflow = 7,               ///< The CAN2 RX queue has overflowed.
    paramRxCan3QueueOverflow = 8,               ///< The CAN3 RX queue has overflowed.
    paramRxCan4QueueOverflow = 9,               ///< The CAN4 RX queue has overflowed.
    paramTxIpcQueueOverflow = 10,               ///< The IPC TX (from ABI device to PC) queue has overflowed.
    paramOutOfMemory = 11,                      ///< The heap memory is full. This can happen if the PC is sending much faster, as the automotive bus can transport the data.
    paramTxFlexrayQueueOverflow = 12,           ///< The FlexRay TX queue has overflowed.
    paramRxFlexrayQueueOverflow = 13,           ///< The FlexRay RX queue has overflowed.
    paramTxCan5QueueOverflow = 14,              ///< The CAN5 TX queue has overflowed.
    paramTxCan6QueueOverflow = 15,              ///< The CAN6 RX queue has overflowed.
    paramRxCan5QueueOverflow = 16,              ///< The CAN5 TX queue has overflowed.
    paramRxCan6QueueOverflow = 17,              ///< The CAN6 RX queue has overflowed.
    paramTimingSynchronisationModeChanged = 18, ///< Timing synchronisation mode changed (param1: mode).
    paramTimeStampBaseChanged = 19              ///< TimeStamp base changed (param1: new timestamp (high) param2: new timestamp (low).
} Parameter;

/// Bus events.
/// \ingroup ABI
typedef enum
{
    beMOSTStressNicMode = 10,               ///< MOST StressNIC: Mode Changed (param1: mode).
    beMOSTNetOn = 11,                       ///< MOST: Net on.
    beMOSTNetOff = 12,                      ///< MOST: Net off.
    beMOSTMPRChanged = 13,                  ///< MOST: MPR changed (param1: new MPR param2: old MPR).
    beMOSTShortUnlock = 14,                 ///< MOST: Short unlock.
    beMOSTCriticalUnlock = 15,              ///< MOST: Critical unlock.
    beMOSTLock = 16,                        ///< MOST: Stable lock.
    beMOSTUnlock = 17,                      ///< MOST: Undefined unlock.
    beMOSTNPRChanged = 18,                  ///< MOST: NPR changed (param1: new NPR). 
    beMOSTSBCChanged = 19,                  ///< MOST: SBC changed (param1: 1Byte none 1Byte Preset 1Byte Current 1Byte Total param2: 2Byte Free 1Byte Packet 1Byte Used).
    beMOSTShutdownReasonChanged = 20,       ///< MOST: Shutdown reason (MOST 150) changed (param1: Shutdown Reason).

    beFlexRayOnline = 21,                   ///< FlexRay: Bus online.
    beFlexRayOffline = 22,                  ///< FlexRay: Bus offline.
    beFlexRayBadHardware = 23,              ///< FlexRay: Bad hardware.
    beFlexRayBadConfiguration = 24,         ///< FlexRay: Bad configuration.
    beFlexRayTxChanged = 25,                ///< FlexRay: Tx messages enabling changed (param1: enabled).

    beCANBusError = 31,                     ///< CAN: Bus error.
    
    beLINBusError = 41,                     ///< LIN: Bus error.
    
    beMOSTVolumeChanged = 50,               ///< MOST: MOST: Volume changed (param1: 1Byte connector type 1Byte isInput 1Byte modifyLeft 1Byte modifyRight param2: volume).
    beMOSTDiagnostic = 51,                  ///< MOST: Diagnostic event (param1: type param2: optional parameter).
    beMOSTInitError = 52,                   ///< MOST: Init error.
    beMOSTStartupFailed = 53,               ///< MOST: Startup failed.
    beMOSTStartupBusy = 54,                 ///< MOST: Startup busy.
    beMOSTShutdownFailed = 55,              ///< MOST: Shutdown failed.
    beMOSTShutdownBusy = 56,                ///< MOST: Shutdown busy.
    beMOSTNetworkInterface = 57,            ///< MOST: Network Interface status (param1: State).
    beMOSTSystemErrorMonitor = 58,          ///< MOST: System error monitor status (param1: Result).
    beMOSTGroupAddressChanged = 59,         ///< MOST: Group address changed (param1: New group address).
    beMOSTNodeAddressChanged = 61,          ///< MOST: Node Address changed (param1: New node address).
    beMOSTDeviceModeChanged = 62,           ///< MOST: Device Mode changed (param1: New device mode param2: Options).
    beMOSTFrequencyChanged = 63,            ///< MOST: MOST frequency changed (param1: new frequency).
    beMOSTLowLevelRetryCountChanged = 64,   ///< MOST: MOST low level retry count changed (param1: new count).
    beMOSTMidLevelRetryCountChanged = 65,   ///< MOST: MOST mid level retry count changed (param1: new count).
    beMOSTHighLevelRetryCountChanged = 66,  ///< MOST: MOST high level retry count changed (param1: new count).
    beMOSTMPREqual = 67,                    ///< MOST: Maximum position range (MPR) equal (param1: mpr).
    beMOSTIlluminationLevelChanged = 68,    ///< MOST: MOST illumination level changed (param1: level).
    beMOSTSpyChange = 69,                   ///< MOST SPY: State of MOST spy channels (param1: 1Byte status 1Byte control 1Byte async 1Byte sync (0 means false, >0 means true).
    
    beLINTPBusError = 70,                   ///< LINTP: Bus error.
    
    beCANTPRxConfigChanged = 80,            ///< CANTP: Rx config changed (param1: 1Byte block size 1Byte separation time 2Byte consecutive frame timeout).
    beCANTPTxConfigChanged = 81,            ///< CANTP: Tx config changed (param1: 2Byte flow control timeout 2Byte clear to send timeout).
    beCANTPPatternChanged = 82,             ///< CANTP: Pattern changed (param1: pattern param2: 1 if pattern generator is active; otherwise 0).
    
    beMOSTMuteStateChanged = 90,            ///< Audio: Mute state changed (param1: 1Byte connector type 1Byte isInput 1Byte reserved 1Byte reserved param2: muteState
} TBusEvent;

/// This errors are returned from \ref ABICommands 
/// and signalize if operation was executed successful.
/// \ingroup ABI
typedef enum
{
    tmRetValOk = 0,                     ///< Operation successful.
    tmRetValNotImplemented = 1,         ///< Function is not implemented.
    tmRetValSystemNotReady = 2,         ///< System is not ready.
    tmRetValObjectAlreadyExists = 3,    ///< Object already exists.
    tmRetValObjectNotFound = 4,         ///< Object not found.
    tmRetValCannotExecute = 5,          ///< Operation can not be executed.
    tmRetValTooManyObjects = 6,         ///< The maximum amount of transmitter / receiver objects are already allocated in the ABI device.
    tmRetValTooManySignals = 7,         ///< The maximum amount of sweep signals are already assigned to the transmitter in the ABI device.
    tmRetValBadBusId = 8,               ///< Bad bus ID.
    tmRetValBadDataLength = 9,          ///< Bad data length.
    tmRetValBadBitPosition = 10,        ///< Bad bit position.
    tmRetValBadSignalType = 11,         ///< Bad signal type.
    tmRetValBadStepsNumber = 12,        ///< Bad steps number.
    tmRetValOutOfMemory = 13,           ///< Device is out of memory.
    tmRetValBadRepetitionsNumber = 14,  ///< Bad repetition number.
    tmRetValBadHandle = 15,             ///< Bad handle passed.
    tmRetValBadMessageId = 16,          ///< Message ID was not correct.
    tmRetValFunctionObsolete = 17,      ///< Function is obsolete and not supported.
    tmRetValBadParameter1 = 21,         ///< Parameter 1 is not valid.
    tmRetValBadParameter2 = 22,         ///< Parameter 2 is not valid.
    tmRetValBadParameter3 = 23,         ///< Parameter 3 is not valid.
    tmRetValBadParameter4 = 24,         ///< Parameter 4 is not valid.
    tmRetValBadParameter5 = 25,         ///< Parameter 5 is not valid.
    tmRetValBadParameter6 = 26,         ///< Parameter 6 is not valid.
    tmRetValBadParameter7 = 27,         ///< Parameter 7 is not valid.
    tmRetValBadParameter8 = 28,         ///< Parameter 8 is not valid.
    tmRetValBadParameter9 = 29,         ///< Parameter 9 is not valid.
    tmRetErrorDuringExecution = 30,     ///< Error during executing the function.,
    tmRetErrorInvalidMessageFormat = 31 ///< Message format is invalid (e.g. CMS payload is malformed).
} TMErrorCode;

/// Enum to select the desired informations.
/// \ingroup ABI
typedef enum
{
    FirmwareVersion = 0x01u,        ///< Firmware version of MOCCA compact firmware.
    HardwareVersion = 0x02u,        ///< Hardware version of MOCCA compact.
    SerialNumber = 0x03u,           ///< Serialnumber of MOCCA compact mainboard.
    SupportedFeatures = 0x04u,      ///< Supported Features of abi firmware.
    MacAddress = 0x05u,             ///< MAC Address of MOCCA compact.
    FpgaVersion = 0x06u,            ///< FPGA firmware version.
    CpldVersion = 0x07u,            ///< CPLD firmware version.
    SerialNumberPhyBoard = 0x40u,   ///< Serialnumber of MOST phy board.
    InicFirmwareVersion = 0x41u,    ///< Firmware version of INIC.
    InicConfigString = 0x42u,       ///< Config string version of INIC firmware.
    NetServicesVersion = 0x43u,     ///< NetServices version.,
    SpynicFirmwareVersion = 0x44,   ///< Firmware version of SPYNIC.
    SpynicConfigString = 0x45,      ///< Config string version of SPYNIC firmware.  
    SpynicHardwareVersion = 0x46,   ///< Hardware version of SPYNIC.
    SpynicApiVersion = 0x47,        ///< API version of SPYNIC. 
    SpynicProductDate = 0x48,       ///< Product Date of SPYNIC.    
    StressnicFirmwareVersion = 0x49,///< Firmware version of StressNIC.
    StressnicConfigString = 0x4A,   ///< Config string version of StressNIC firmware.
    StressnicHardwareVersion = 0x4B,///< Hardware version of StressNIC.
    StressnicApiVersion = 0x4C,     ///< API version of StressNIC.
    StressnicProductDate = 0x4D,    ///< Product Date of StressNIC.
    StressnicApiVersionInic = 0x4E  ///< API version of StressNIC integrated INIC.
} BoardInformation;

/// Synchronisation mode of hardware timestamp synchronisation.
/// \ingroup ABI
typedef enum 
{
    SynchronisationStandalone = 0x00u,  ///< Standalone mode.
    SynchronisationMaster = 0x01u,      ///< Master mode.
    SynchronisationSlave = 0x02u        ///< Slave mode.
} TimingSynchronisationMode;

/// Absolute trigger command.
/// \ingroup ABI
typedef struct
{
    bool isActive;                  ///< Gets or sets a value indicating whether this instance is active.
    unsigned long long timeStamp;   ///< Gets or sets the timestamp when the command shall be executed (in ticks of 100ns).
} AbsoluteTriggerCommand;

/// Relative trigger command.
/// \ingroup ABI
typedef struct
{
    bool isActive;                  ///< Gets or sets a value indicating whether this instance is active.
    WORD delay;                     ///< Gets or sets the delay of the next command (in milliseconds).
} RelativeTriggerCommand;

/// FlexRay channels.
/// \ingroup ABI
typedef enum 
{
    A = 1,  ///< Channel A.
    B = 2,  ///< Channel B.
    AB = 3  ///< Channel A+B.
} FrChannel;

/// TX Status of FlexRay.
/// \ingroup ABI
typedef enum
{
    flexraySuccess = 0x00,  ///< Message was sent successful.
    flexrayFailed = 0x01    ///< Message sending has failed.
} FlexrayTxStatus;

/// MOST protocols.
/// \ingroup ABI
typedef enum 
{
    CMS,    ///< Control Message Service.
    AMS,    ///< Application Message Service.
    ADS,    ///< Asynchronous Data Service.
    HIGH    ///< MOST High protocol.
} MostProtocol;

/// MOST device mode.
/// \ingroup ABI
typedef enum
{
    mostSlave = 0,                  ///< Slave mode.
    mostStaticMaster = 1,           ///< Static master mode.
    mostBypass = 2,                 ///< Bypass mode.
    mostNormalMaster = 3,           ///< Master mode.
    mostConstantLightMaster = 4     ///< Static master mode with constant light (MOST 150 only).
} MostMode;

/// MOST device mode options.
/// @ingroup ABI
typedef enum
{
    NoOption = 0x00,        ///< No option.
    ForceDeviceMode = 0x01  ///< If the device is in a master mode and the network is in NetOn a Shutdown will be forced.
} DeviceModeOptions;

/// Role of device in MOST ring. 
/// \ingroup ABI
typedef enum
{
    bypassOff = 0,  ///< Device participate in MOST ring and switch to the last valid mode of device.
    bypassOn = 1    ///< Device is not participate in MOST ring.
} BypassMode;  

/// Determinates the destination addressing type of a received message.
/// \ingroup ABI
typedef enum
{
    logicaladdress = 0x00,  ///< Message was sent to a logical node address.
    nodeaddress = 0x01,     ///< Message was sent to a node position address (0x0400 - 0x043F).
    broadcast = 0x02,       ///< Message was sent to a broadcast address (0x03C8 or 0x03FF).
    groupcast = 0x03,       ///< Message was sent to a group address (0x0300 - 0x03FF).
    unknown = 0xFF          ///< Receiver type is not available.
} ReceiveType;

/// Bandwith of audio channel.
/// \ingroup ABI
typedef enum
{
    Mono16Bit   = 0x00, ///< 16 Bit mono channel.
    Stereo16Bit = 0x01, ///< 16 Bit stereo channel.
    Stereo24Bit = 0x02  ///< 24 Bit stereo channel.
} AudioBandwidth;

/// Frequency of MOST network.
/// \ingroup ABI
typedef enum
{
    Frequency44100Hz = 0x00,    ///< 44,1 kHz.
    Frequency48000Hz = 0x01     ///< 48 kHz.
} MostSamplingFrequency;

/// Type of audio streaming event.
/// \ingroup ABI
typedef enum
{
    txAllocate,             ///< Allocates new resources on MOST.
    txAllocateConnect,      ///< Allocates new resources on MOST and transmit the audio stream to them.
    txConnect,              ///< Transmits the audio stream on preallocated resources.
    rxConnect               ///< Receives an Audiostream from preallocated resources.
} AudioConnectionType;

/// Entry in allocation table.
/// \ingroup ABI
typedef struct
{
    WORD connectionLabel;   ///< Connection label.
    WORD channelWidth;      ///< Width of allocated channel in bytes.
} AllocationTableEntry;

/// Type of audio streaming connector.
/// @ingroup ABI
typedef enum
{
    AudioConnectorSpdif = 0x00,     ///< SPDIF connector.
    AudioConnectorAnalog = 0x01,    ///< Analog connector.
    AudioConnectorAll = 0xFF        ///< All supported connectors.
} AudioConnectorType;

/// Entry in local label table.
/// \ingroup ABI
typedef struct
{
    WORD connectionLabel;               ///< Connection label.
    AudioConnectorType connectorType;   ///< Audio connector type.
    BYTE dummy;                         ///< Unused byte.
} LabelEntry;

/// Delivers more detailed information, why the transmission of the MOST message failed.
/// \ingroup ABI
typedef enum
{
    mostControlWrongTarget = 0x0100,          ///< Transmission failed. No response under chosen target address.
    mostControlSuccess = 0x0110,              ///< Transmission of all telegrams successful.
    mostControlCrc = 0x0120,                  ///< Transmission of a telegram failed. Incorrect CRC. 
    mostControlBuf = 0x0121,                  ///< Transmission of a telegram failed. Receive buffer of target device was full.
    mostControlSuccessAndCrc = 0x0130,        ///< Combination in case of a group-/broadcast transmission. At least one device got the message correctly. At least one device rejected the message due to an incorrect CRC value.
    mostControlSuccessAndBuf = 0x0131,        ///< Combination in case of a group-/broadcast transmission. At least one device got the message correctly. At least one device didn not receive it due to a full receive buffer.
    mostControlTimeout = 0x0180,              ///< Timeout error. No response from lower layer.
    mostControlNetOff = 0x0181,               ///< Transmission failed, since MOST network is switched off.
    mostControlFrmt = 0x0182,                 ///< Protocol error in the Port Message.
    mostControlSync = 0x0183,                 ///< Transmission failed due to an synchronization failure between INIC and PMS. The PMS has been re-synchronized.
    mostControlInternal = 0x0184,             ///< Internal error. Undefined status identifier.
    mostControlNoIcm = 0x0185,                ///< You tried to send a ICM message (Tgt_Adr = 1 and FBlock INIC), which is reserved for the services of the MOST NetServices only.
    mostControlProtectedMode = 0x0187,        ///< You have called MsgSend() while the EHC is not attached to the INIC. It is not allowed to call any API function beside the Kernel function MostService() and the timer management functions until the re-initialization is completed.
    mostControlSuccessAndTimeout = 0x0190,    ///< Combination in case of a group-/broadcast transmission. Internal FBlocks received the message, while FBlocks on the network are not accessible due to a timeout error from the lower layer.
    mostControlSuccessAndNetOff = 0x0191,     ///< Combination in case of a group-/broadcast transmission. Internal FBlocks received the message, while FBlocks on the network are not accessible while the network is switched off.
    mostControlSuccessAndSync = 0x0193,       ///< Combination in case of a group-/broadcast transmission. Internal FBlocks received the message, while FBlocks on the network are not accessible due to a synchronization failure between INIC and PMS.
    mostControlInvalidMessage = 0x01A0,       ///< Not a valid CMS message.
    mostControlRetry = 0x01A1,                ///< Reserved.

    mostAsyncSuccess = 0x0200,                ///< The packet was delivered successfully. The connection is held on until timeout (MHP_TIME_DELAY _END) or until connection is terminated by application.
    mostAsyncErrorNac = 0x0201,               ///< Error during transmission: At least one frame was not acknowledged after considering timeouts and retries.  The connection was terminated automatically. 
    mostAsyncErrorKilled = 0x0202,            ///< Error during transmission: The receiver device terminated the connection, before the packet has been transmitted completely.
    mostAsyncErrorBuildCon = 0x0203,          ///< Error during establishing the connection: The Start Connection telegram was not received.
    mostAsyncErrorPrio = 0x0204,              ///< Error during establishing the connection: The connection was rejected by the receiver, because the priority is to minor. The returned priority can be read out of the connection list entry (field Priority).
    mostAsyncErrorScale = 0x0205,             ///< Error during establishing the connection: The receiver sent an invalid scale value (0x00).
    mostAsyncErrorNoBuf = 0x0206,             ///< Error during connection establishing or transmission: The connection was terminated because an internal error occurred. After MHP_BUF_RETRY retries no internal wADS buffer was available.
    mostAsyncKilledTx = 0x0208,               ///< The transmission was canceled by the application of the sender prematurely.
    mostAsyncErrorNdf = 0x0209,               ///< Error during establishing the connection: The receiver sent an invalid NDF value (0x00).
    mostAsyncLenZero = 0x0221,                ///< The order is denied ( MhpSend() ), since the packet length has been set to zero. The entry is removed from the connection list.
    mostAsyncEndReady = 0x0222,               ///< The entry is removed from the connection list. This is not an error report, but this event can be used to get the information, when the connection is closed.
    mostAsyncKilledRx = 0x0223,               ///< The receiver device terminated the connection, after the packet has been transmitted completely. The sender is in hold state currently.
    mostAsyncConfigNotOk = 0x0225,            ///< The connection is not established, or is going to be terminated, due to system state NotOK.
    mostAsyncLenTooBig = 0x0226,              ///< The total size of packet exceeds the upper limit. The entry is removed from the connection list.
    mostAsyncClose = 0x0227,                  ///< The connection is closed, due to transition from NetOn to NetOff or re-initialization of the MHP module while the connection was open.
    mostAsyncAddressHandlerBusy = 0x02A1,     ///< Function called failed, as the target address is 0xFFFF, and the Address Handler is busy.
    mostAsyncNcsNotOk = 0x02A2,               ///< Function called failed, as the System Configuration State is NotOK.
    mostAsyncTxLenZero = 0x02A3,              ///< Function called failed, as the Length field is zero. The entry is removed from the connection list.
    mostAsyncInvTotalPacketLength = 0x02A4    ///< Function called failed, as the TotalPacketLength field exceeds the maximum size or the TotalPacketLength field is smaller than the Length field and not zero. The entry is removed from the connection list.
} MostTxStatus;

/// Options to configure for MHP transmission.
/// @ingroup ABI
typedef enum
{
    DefaultMhpOptions = 0,      ///< Default option set: Block acknowledge.
    SingleBlockAcknowledge = 1  ///< Single block acknowledge.
} MostMhpOptions;

/// Delivers more detailed information, why the reception of the MOST message failed.
/// @ingroup ABI
typedef enum
{
    MhpSuccess = 0x0200,        ///< The receipt of one block was successfully.
    MhpErrorNFrame = 0x0201,    ///< Error during transmission: The required Null-Frame is missing.
    MhpErrorFrame = 0x0202,     ///< Error during transmission: Data frames are missing.
    MhpErrorNoReady = 0x0203,   ///< Error during transmission: After sending a "Start Connection" no "Ready for Data" received.
    MhpErrorKilled = 0x0204,    ///< Error during transmission: The sender device terminated the transmission prematurely.
    MhpErrorNoBuf = 0x0205,     ///< Error during transmission: The connection was terminated because an internal error occurred.
    MhpErrorKilledRx = 0x0206,  ///< The connection was canceled by receiver's application.
    MhpClose = 0x0210           ///< The connection is closed (No new data received since the last rx event).
} MostRxStatus;

/// Status of MOST illumination level.
/// @ingroup ABI
typedef enum
{
    IlluminationOff = 0,        ///< Light is switched off.
    IlluminationMinus3dB = 1,   ///< Light is on and dimmed with -3dB.
    IlluminationFull = 2        ///< Light is on.
} MostIlluminationLevel;

/// Device mode of StressNIC.
/// @ingroup ABI
typedef enum StressNicMode
{
    Off = 0,    ///< StressNIC is disconnected from MOST network.
    Bypass = 1, ///< StressNIC is in Bypass mode.
    Slave = 2   ///< StressNIC is in Slave mode.
} StressNicMode;

/// <summary>
/// Options of MEP bridge.
/// </summary>
/// @ingroup ABI
typedef enum MepBridgeOptions
{
    None = 0,           ///< No options activated.
    DmaOptimization = 1 ///<Improve speed of bridge by using DMA from LAN chip to/from MediaLB. (If enabled MOST High and ADS are ignored.)
} MepBridgeOptions;

/// CAN errors.
/// \ingroup ABI
typedef enum
{
    CanTxCounterNormal = 17,        ///< The TX error counter of the CAN cell is in the normal range.
    CanTxCounterWarning = 18,       ///< The TX error counter of the CAN cell is in the warning range.
    CanTxCounterErrorPassive = 19,  ///< The TX error counter of the CAN cell is in the passive range.
    CanTxCounterBusOff = 20,        ///< The TX error counter of the CAN cell is in the bus off range.
    CanRxCounterNormal = 33,        ///< The RX error counter of the CAN cell is in the normal range.
    CanRxCounterWarning = 34,       ///< The RX error counter of the CAN cell is in the warning range.
    CanRxCounterErrorPassive = 35,  ///< The RX error counter of the CAN cell is in the passive range.
    CanProtStuffError = 49,         ///< Signals that a protocol stuffing error occured.
    CanProtFormError = 50,          ///< Signals that a protocol form error occured.
    CanProtAckError = 51,           ///< Signals that a acknowledge error occured.
    CanProtBitError = 52,           ///< Signals that a bit error occured.
    CanProtCrcError = 53            ///< Signals that a CRC error occured.
} TCanError;

/// CAN transceiver.
/// \ingroup ABI
typedef enum
{
    canTransceiverSlow = 1,                 ///< Fault tolerant CAN transceiver (TJA 1080).
    canTransceiverFast = 2,                 ///< Highspeed CAN transceiver (TJA 1054).
    canTransceiverSingleWireNormal = 3,     ///< Signle wire CAN transceiver (NCV7356). Normal mode.
    canTransceiverSingleWireHighSpeed = 4,  ///< Signle wire CAN transceiver (NCV7356). High speed mode.
    canTransceiverSingleWireHighVoltage = 5 ///< Signle wire CAN transceiver (NCV7356). High voltage mode.
} CanTransceiver; 

/// CAN baudrate settings.
/// \ingroup ABI
typedef enum
{
    canSpeed83 = 1,             ///< Baudrate 83,3 kbps.
    canSpeed100 = 2,            ///< Baudrate 100 kbps.
    canSpeed125 = 3,            ///< Baudrate 125 kbps.
    canSpeed500 = 4,            ///< Baudrate 500 kbps.
    canSpeed33 = 5,             ///< Baudrate 33,3 kbps.
    canSpeed66 = 6,             ///< Baudrate 66,6 kbps.
    canSpeed1000 = 7,           ///< Baudrate 1000 kbps.
    canSpeed250 = 8,            ///< Baudrate 250 kbps.
    canSpeedIndividual = 0xFF   ///< Individual Baudrate.
}  CanSpeed;

/// TX Status of CAN.
/// \ingroup ABI
typedef enum {
    canSuccess = 0x00,  ///< Message was sent successful.
    canFailed = 0x01    ///< Message sending has failed.
} CanTxStatus;

/// Adressing type of CAN transport protocol.
/// \ingroup ABI
typedef enum
{
    tpNormal = 1,   ///< ISO CAN TP normal addressing.
    tpExtended = 2  ///< ISO CAN TP extended addressing.
} TPAddressingType;

/// CAN transport protocol errors.
/// \ingroup ABI
typedef enum
{
    tpSuccess = 0,                      ///< Successful TP operation.
    tpOutOfMemory = 1,                  ///< Out of memory during receiving of the first frame.
    tpBadSingleFrame = 2,               ///< Bad single frame.
    tpBadFirstFrameLength = 3,          ///< Bad length of the first frame.
    tpBadFirstFrame = 4,                ///< Bad first frame.
    tpBadConsecutiveFrameLength = 5,    ///< Bad length of the consecutive frame
    tpBadConsecutiveFrameSequence = 6,  ///< Bad sequence number of the consecutive frame.
    tpBadMessageLength = 7,             ///< Bad message length.
    tpBadFlowStatus = 8,                ///< Bad flow status of the TP driver.
    tpUnexpectedFrame = 9,              ///< Unexpected frame received.
    tpUnexpectedCtrlInfo = 10,          ///< Unexpected control information.
    tpConsecutiveFrameTimeout = 11,     ///< Timeout for the consecutive frame.
    tpFlowControlTimeout = 12,          ///< Timeout for the flow control.
    tpFlowControlCTSTimeout = 13        ///< Timeout for the control clear to send.
} TPOpResult;

/// LIN warnings and errors.
/// Members of this enum are used in event OnLinBusEvent().
/// \ingroup ABI
typedef enum
{
    LinUnknownError = 0xFF,                     ///< Unknown type of error.
    LinSyncHeaderError = 0x10,                  ///< No sync field after header.
    LinBreakHeaderError = 0x11,                 ///< Unexpected break in header.
    LinLowLevelFramingHeaderError = 0x21,       ///< Low level framing error in header.
    LinLowLevelOverflowHeaderError = 0x22,      ///< Low level overflow error in header.
    LinOverflowHeaderError = 0x23,              ///< New header started before previous has been read.
    LinParityHeaderError = 0x24,                ///< Header parity error.
    LinBreakResponseError = 0x31,               ///< Unexpected break in response.
    LinLowLevelFramingResponeError = 0x32,      ///< Low level framing error in response.
    LinLowLevelOverflowResponseError = 0x33,    ///< Low level overflow error in response.
    LinLengthResponseError = 0x34,              ///< Response length does not match expected length.
    LinChecksumResponseError = 0x35,            ///< Response checksum error.
    LinOverflowResponseError = 0x36,            ///< New response started before previous has been read.
    LinReadResponseError = 0x37                 ///< Error while try to read the response.
} TLinError;

/// LIN TP warnings and errors.
/// Members of this enum are used in event OnLinTpBusEvent().
/// \ingroup ABI
typedef enum
{
    LinTp_NoError = 0x50,               ///< No LIN-TP error was reported.
    LinTp_TxTimeOut = 0x51,             ///< LIN-TP Transmission timed out.
    LinTp_RxTimeOut = 0x52,             ///< LIN-TP Reception timed out.
    LinTp_TxBusy = 0x53,                ///< Can not send LIN-TP message, because TP communication is in progress.
    LinTp_NadError = 0x54,              ///< Received an other Node Address while a receiption was in progress.  
    LinTp_PciError = 0x55,              ///< Received an invalid Protocol Control Information (PCI) field.
    LinTp_InvalidLength = 0x56,         ///< Received an invalid length field or LIN frame was smaller than 8 Byte. 
    LinTp_InvalidFrameCounter = 0x57,   ///< Received an invalid frame counter value. This may happen if a frame was lost.
    LinTp_InvalidCfFrame = 0x58,        ///< Received an SF or FF frame. Expected to receive a CF frame.
    LinTp_RxStartWithCF = 0x59          ///< Received an CF frame at the begin of the reception.
} TLinTpError;

/// TX Status of LIN.
/// \ingroup ABI
typedef enum 
{
    linSuccess = 0x00,  ///< Message was sent successful.
    linFailed = 0x01    ///< Message sending has failed.
} LinTxStatus;

/// LIN spy event.
/// \ingroup ABI
typedef enum
{
    RxReady = 0x01,         ///< Data are available.
    RxFraming = 0x02,       ///< Frame error occured.
    RxBreak = 0x04,         ///< Break symbol received.
    RxOverrun = 0x08,       ///< Hardware queue overflow.
    RxPacketLength = 0x10   ///< LIN packet length is longer than allowed.
} TLinSpyEvent;

class IABI;

/*============================================================================*/
/// \brief Interface of ABI event listener.
/// \note Abstract base class. So it has to be implemented.
/// \ingroup ABI
/*============================================================================*/
class K2LABI_API IABIListener
{
public:
    virtual ~IABIListener() {}
    
    /// Called when error occured.
    /// \param[out] pAbi ABI instance that received event.
    virtual void OnError(IABI* pAbi) = 0;

    /// Called when message lost.
    /// \param[out] pAbi ABI instance that received event.
    virtual void OnMessageLost(IABI* pAbi) = 0;

    /// This event is raised on the first occurence (once), if a internal queue overflows. To read out the amount of occurrence and to clear the counter refer GetInternalState().
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] tEvent Enumeration identifying the source of problem.
    /// \param[out] param1 Reserved.
    /// \param[out] param2 Reserved.
    virtual void OnInternalEvent
        ( IABI* pAbi
        , unsigned long long timeStamp
        , Parameter tEvent
        , unsigned int param1
        , unsigned int param2
        ) = 0;

    /// This event can be raised for general purposes or costumer specific. The event will hold a string message, describing the event in human language.
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] identifier Channel of the message. Delivers the possibility to group messages by their meanings.
    /// \param[out] payload The string which describes the event in human languange.
    /// \param[out] param2 Length of the string.
    virtual void OnGeneralEvent
        ( IABI* pAbi
        , unsigned long long timeStamp
        , BYTE identifier
        , BYTE* payload
        , int payloadLength
        ) = 0;

    /// This event will be raised, when state of bus is changed by another process.
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method. (Bus context)
    /// \param[out] timeStamp The occurrence of the event, represented in steps of 100 nanoseconds.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] isStarted true: The bus has been started by another process. false: The bus has been stopped by another process.
    /// \param[out] param1 Parameter1.
    /// \param[out] param2 Parameter2.
    /// \note isStarted = false: param1 = 0; param2 = 0
    /// \note isStarted = true:
    /// \note LINx: param1 = 0; param2 = 0
    /// \note CANx: param1 = baudrate (type=CanSpeed); param2 = type of transceiver (type=CanTransceiver)
    /// \note FLEXRAYx: param1 = 0; param2 = 0
    /// \note MOST_CONTROL_AMSx: param1 = device mode (type=MostMode); param2 = 0
    virtual void OnBusRunningStateChanged
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , Bus bus
        , bool isStarted
        ) = 0;

    /// This event will be raised, when state of relay is changed by another process.
    /// \param[out] abi ABI instance that received event.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method. (Bus context)
    /// \param[out] timeStamp The occurrence of the event, represented in steps of 100 nanoseconds.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] isClosed If true the relay is closed. 
    virtual void OnRelayStateChanged
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , Bus bus
        , bool isClosed
        ) = 0;

    
    /// This event will be raised, when state of tx payload filter is changed by another process.
    /// \param[out] abi ABI instance that received event.
    /// \param[out] context Delivers the object, which was stored by the RegisterBusContext() method. (Bus context)
    /// \param[out] timeStamp The occurrence of the event, represented in steps of 100 nanoseconds.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] isEnabled If true no payload is transmitted in tx events otherwise false.
    virtual void OnTxPayloadFilterChanged
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , Bus bus
        , bool isEnabled
        ) = 0;

    /// This event will be raised, when an object has changed its state.
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] context Delivers the object, which was stored by the RegisterBusContext() method. (Bus context)
    /// \param[out] timeStamp The occurrence of the event, represented in steps of 100 nanoseconds.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] type The type of the object.
    /// \param[out] handle The handle.
    /// \param[out] groupId The group id.
    /// \param[out] state The state of the object.
    virtual  void OnObjectStateChanged
        ( IABI* pAbi
            , void* pContext
            , unsigned long long  timeStamp
            , Bus bus
            , ObjectType type
            , unsigned short handle
            , BYTE groupId
            , ObjectState state
            ) = 0;

    /// This event will be raised, when a CAN Message has been received.
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method. (Bus context)
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] messageId The identifier of the message.
    /// \param[out] payload The payload which was attached to the message.
    /// \param[out] payloadLength The length of the attached payload.
    virtual void OnCanRxMessage
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , Bus bus
        , unsigned int messageId
        , bool extended
        , BYTE* payload
        , int payloadLength
        ) = 0;
    
    /// This event will be raised, when a CAN Message has been succesful sent.
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] handle The handle which was given to the correspondic transmitter / receiver.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] messageId The identifier of the message.
    /// \param[out] extended Determinates if the CAN message used a CAN extended 29 bit identifier. True, if 29bit Identifier used. False if 11bit Identifier was used.
    /// \param[out] payload The payload which was attached to the message.
    /// \param[out] payloadLength The length of the attached payload.
    virtual void OnCanTxMessage
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , unsigned short handle
        , Bus bus
        , unsigned int messageId
        , bool extended
        , BYTE* payload
        , int payloadLength
        ) = 0;
    
    /// This event will be raised, when a CAN Message was not transmitted, because of bus errors.
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] handle The handle which was given to the correspondic transmitter / receiver.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] messageId The identifier of the message.
    /// \param[out] extended Determinates if the CAN message used a CAN extended 29 bit identifier. True, if 29bit Identifier used. False if 11bit Identifier was used.
    /// \param[out] payload The payload which was attached to the message.
    /// \param[out] payloadLength The length of the attached payload.
    virtual void OnCanTxFailedMessage
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , unsigned short handle
        , Bus bus
        , unsigned int messageId
        , bool extended
        , BYTE* payload
        , int payloadLength
        ) = 0;
    
    /// This event will be raised, when a CAN bus error has occured.
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] handle The handle which was given to the correspondic transmitter / receiver.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] evt Enumeration identifying the type of bus event.
    /// \param[out] error Enumeration identifying the type of bus error.
    virtual void OnCanBusError
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , Bus bus
        , TBusEvent evt
        , TCanError error
        ) = 0;

    /// This event will be raised, when a MOST Message has been received.
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] rcvType Enumeration identifying the type of receive message.
    /// \param[out] source Address of source.
    /// \param[out] prot MOST protocol.
    /// \param[out] messageId The identifier of the message.
    /// \param[out] fBlock The Function Block identifier.
    /// \param[out] instance The Function Block instance number.
    /// \param[out] function The function identifier of the FBlock.
    /// \param[out] opType The Function Block operation type identifier.
    /// \param[out] payload Message payload.
    /// \param[out] payloadLength Payload length.
    virtual void OnMostRxMessage
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , Bus bus
        , ReceiveType rcvType
        , unsigned short source
        , MostProtocol prot
        , unsigned int messageId
        , BYTE fBlock
        , BYTE instance
        , unsigned short function
        , BYTE opType
        , BYTE* payload
        , int payloadLength
        ) = 0;

    /// This event will be raised, when a MOST Message has been received.
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] status The status of reception.
    /// \param[out] rcvType Enumeration identifying the type of receive message.
    /// \param[out] source Address of source.
    /// \param[out] prot MOST protocol.
    /// \param[out] messageId The identifier of the message.
    /// \param[out] fBlock The Function Block identifier.
    /// \param[out] instance The Function Block instance number.
    /// \param[out] function The function identifier of the FBlock.
    /// \param[out] opType The Function Block operation type identifier.
    /// \param[out] payload Message payload.
    /// \param[out] payloadLength Payload length.
    virtual void OnMostRxFailedMessage
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , Bus bus
        , MostRxStatus status
        , ReceiveType rcvType
        , unsigned short source
        , MostProtocol prot
        , unsigned int messageId
        , BYTE fBlock
        , BYTE instance
        , unsigned short function
        , BYTE opType
        , BYTE* payload
        , int payloadLength) = 0;

    /// This event will be raised, when a MOST High Message has been received (>64kb).
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] status The status of reception.
    /// \param[out] rcvType Enumeration identifying the type of receive message.
    /// \param[out] source Address of source.
    /// \param[out] prot MOST protocol.
    /// \param[out] messageId The identifier of the message.
    /// \param[out] fBlock The Function Block identifier.
    /// \param[out] instance The Function Block instance number.
    /// \param[out] function The function identifier of the FBlock.
    /// \param[out] opType The Function Block operation type identifier.
    /// \param[out] segmentationId The segmentation identifier of this data block.
    /// \param[out] blockCounter The relative block counter of the data block in this packet transmission.
    /// \param[out] payload Message payload.
    /// \param[out] payloadLength Payload length.
    virtual void OnMostHighExtRxMessage
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , Bus bus
        , MostRxStatus status
        , ReceiveType rcvType
        , unsigned short source
        , MostProtocol prot
        , unsigned int messageId
        , BYTE fBlock
        , BYTE instance
        , unsigned short function
        , BYTE opType
        , BYTE segmentationId
        , unsigned short blockCounter
        , BYTE* payload
        , int payloadLength
        ) = 0;

    /// This event will be raised, when a MOST Message has been succesful sent.
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] handle The handle which was given to the correspondic transmitter / receiver.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] destination Address of destination.
    /// \param[out] prot MOST protocol.
    /// \param[out] messageId The identifier of the message.
    /// \param[out] fBlock The Function Block identifier.
    /// \param[out] instance The Function Block instance number.
    /// \param[out] function The function identifier of the FBlock.
    /// \param[out] opType The Function Block operation type identifier.
    /// \param[out] payload Message payload.
    /// \param[out] payloadLength Payload length.
    virtual void OnMostTxMessage
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , unsigned short handle
        , Bus bus
        , unsigned short destination
        , MostProtocol prot
        , unsigned int messageId
        , BYTE fBlock
        , BYTE instance
        , unsigned short function
        , BYTE opType
        , BYTE* payload
        , int payloadLength
        ) = 0;

    /// This event will be raised, when a MOST Message was not transmitted, because the receiver did not correctly acknowledge the message.
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] handle The handle which was given to the correspondic transmitter / receiver.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] destination Address of destination.
    /// \param[out] prot MOST protocol.
    /// \param[out] messageId The identifier of the message.
    /// \param[out] fBlock The Function Block identifier.
    /// \param[out] instance The Function Block instance number.
    /// \param[out] function The function identifier of the FBlock.
    /// \param[out] opType The Function Block operation type identifier.
    /// \param[out] payload Message payload.
    /// \param[out] payloadLength Payload length.
    virtual void OnMostTxNAKMessage
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , unsigned short handle
        , Bus bus
        , unsigned short destination
        , MostProtocol prot
        , unsigned int messageId
        , BYTE fBlock
        , BYTE instance
        , unsigned short function
        , BYTE opType
        , BYTE* payload
        , int payloadLength) = 0;

    /// This event will be raised, when a MOST Message was not transmitted, because of bus errors.
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] status Result status of transmission.
    /// \param[out] handle The handle which was given to the correspondic transmitter / receiver.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] destination Address of destination.
    /// \param[out] prot MOST protocol.
    /// \param[out] messageId The identifier of the message.
    /// \param[out] fBlock The Function Block identifier.
    /// \param[out] instance The Function Block instance number.
    /// \param[out] function The function identifier of the FBlock.
    /// \param[out] opType The Function Block operation type identifier.
    /// \param[out] payload Message payload.
    /// \param[out] payloadLength Payload length.
    virtual void OnMostTxFailedMessage
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , MostTxStatus status
        , unsigned short handle
        , Bus bus
        , unsigned short destination
        , MostProtocol prot
        , unsigned int messageId
        , BYTE fBlock
        , BYTE instance
        , unsigned short function
        , BYTE opType
        , BYTE* payload
        , int payloadLength
        ) = 0;

    /// This event will be raised, when a MOST specific bus event has occured.
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] evt Bus event identifier.
    /// \param[out] param1 Depending on the type of bus event (evt), this parameter holds the main value which belongs to this event. This value is used for MPR, NPR, preset SBC (value >> 16 & 0xff), current SBC (value >> 8 & 0xff), total SBC (value & 0xff), group address (value | 0x300), node address.
    /// \param[out] param2 Depending on the type of bus event (evt), this parameter holds the additional value which belongs to this event. This value is used for the free SBC (value >> 16 & 0xff), packet SBC (value >> 8 & 0xff), used SBC (value & 0xff).
    virtual void OnMostEventMessage
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , Bus bus
        , TBusEvent evt
        , unsigned int param1
        , unsigned int param2
        ) = 0;

    /// This event will be raised, when a MOST High receive status event has occured.
    /// \param[out] abi Used IABI instance.
    /// \param[out] context Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp The occurrence of the event, represented in steps of 100 nanoseconds.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] status Status of event.
    /// \param[out] messageId The identifier of the message.
    /// \param[out] fBlock The Function Block identifier.
    /// \param[out] instance The Function Block instance number.
    /// \param[out] function The function identifier of the FBlock.
    /// \param[out] opType The Function Block operation type identifier.
    /// \param[out] source Address of source.
    virtual void OnMostHighRxStatusEvent
        ( IABI* abi
        , void* context
        , unsigned long long timeStamp
        , Bus bus
        , MostRxStatus status
        , unsigned int messageId
        , BYTE fBlock
        , BYTE instance
        , unsigned short function
        , BYTE opType
        , unsigned short source
        ) = 0;

    /// This event will be raised, when a MOST High transmit status event has occured.
    /// \param[out] abi Used IABI instance.
    /// \param[out] context Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp The occurrence of the event, represented in steps of 100 nanoseconds.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] status Status of event.
    /// \param[out] messageId The identifier of the message.
    /// \param[out] fBlock The Function Block identifier.
    /// \param[out] instance The Function Block instance number.
    /// \param[out] function The function identifier of the FBlock.
    /// \param[out] opType The Function Block operation type identifier.
    /// \param[out] destination Address of destination.
    virtual void OnMostHighTxStatusEvent
        ( IABI* abi
        , void* context
        , unsigned long long timeStamp
        , Bus bus
        , MostTxStatus status
        , unsigned int messageId
        , BYTE fBlock
        , BYTE instance
        , unsigned short function
        , BYTE opType
        , unsigned short destination
        ) = 0;

    /// Called when an action with the audio streaming interface was successful.
    /// \param[out] pAbi Used IABI instance.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] label The connection label.
    /// \param[out] type Type of event.
    /// \param[out] bandwith The bandwith.
    /// \param[out] isOpenEvent If true the action was an open action otherwise it was close.
    /// \param[out] connector The used audio connector.
    virtual void OnMostAudioStreamingEvent
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , unsigned int label
        , AudioConnectionType type
        , AudioBandwidth bandwith
        , bool isOpenEvent
        , AudioConnectorType connector
        ) = 0;

    /// Called when an action with the audio streaming interface has failed.
    /// \param[out] pAbi Used IABI instance.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] error The errorcode.
    /// \param[out] type Type of event.
    /// \param[out] bandwith The bandwith.
    /// \param[out] isOpenEvent If true the action was an open action otherwise it was close.
    /// \param[out] connector The used audio connector.
    virtual void OnMostAudioStreamingError
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , unsigned short error
        , AudioConnectionType type
        , AudioBandwidth bandwith
        , bool isOpenEvent
        , AudioConnectorType connector
        ) = 0;

    /// Called when the allocation table in SPYNIC changed.
    /// \param[out] pAbi Used IABI instance.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] length Length of the lists.
    /// \param[out] connectionLabels List of available connectionlabels.
    /// \param[out] channelWidths List of channel widths of connection labels.
    /// \remarks Only available for MOST50.
    virtual void OnMostAllocationTableChanged
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , unsigned short& length
        , AllocationTableEntry* allocationTable
        ) = 0;

    /// This event will be raised, when a MOST MEP Message has been transmitted.
    /// \param[out] abi Used IABI instance.
    /// \param[out] context Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp The occurrence of the event, represented in steps of 100 nanoseconds.
    /// \param[out] handle The handle which was given to the correspondic transmitter / receiver.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] destination The destination address.
    /// \param[out] payload The payload which was attached to the message.
    /// \param[out] payloadLength The length of the attached payload.
    virtual void OnMostMepTxMessage
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , unsigned short handle
        , Bus bus
        , unsigned long long destination
        , BYTE* payload
        , int payloadLength
        ) = 0;

    /// This event will be raised, when a MOST MEP Message has been received.
    /// \param[out] abi Used IABI instance.
    /// \param[out] context Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp The occurrence of the event, represented in steps of 100 nanoseconds.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] source The source address.
    /// \param[out] payload The payload which was attached to the message.
    /// \param[out] payloadLength The length of the attached payload.
    virtual void OnMostMepRxMessage
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , Bus bus
        , unsigned long long source
        , BYTE* payload
        , int payloadLength
        ) = 0;

    /// Called when MOST MEP bridge status changed.
    /// \param[out] abi Used IABI instance.
    /// \param[out] context Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp The occurrence of the event, represented in steps of 100 nanoseconds.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] isEnabled If set to <c>true</c> bridge is enabled otherwise <c>false</c>.
    /// \param[out] options Activated options.
    virtual void OnMostMepBridgeStatusChanged
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , Bus bus
        , bool isEnabled
        , MepBridgeOptions options
        ) = 0;

    /// Called when MOST MAC address changed.
    /// \param[out] abi Used IABI instance.
    /// \param[out] context Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp The occurrence of the event, represented in steps of 100 nanoseconds.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] macAddress The mac address that have been set.
    virtual void OnMostMacAddressChanged
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , Bus bus
        , unsigned long long macAddress
        ) = 0;

    /// This event will be raised, when a LIN Message has been received.
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] sourceAddress The idententifier identifying the source device.
    /// \param[out] messageId The identifier of the message.
    /// \param[out] payload The payload which was attached to the message.
    /// \param[out] payloadLength The length of the attached payload.
    virtual void OnLinRxMessage
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , Bus bus
        , unsigned int sourceAddress
        , unsigned int messageId
        , BYTE* payload
        , int payloadLength
        ) = 0;

    /// This event will be raised, when a LIN Message has been succesful sent.
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] handle The handle which was given to the correspondic transmitter / receiver.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] targetAddress The idententifier addressing the target device.
    /// \param[out] messageId The identifier of the message.
    /// \param[out] payload The payload which was attached to the message.
    /// \param[out] payloadLength The length of the attached payload.
    virtual void OnLinTxMessage
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , unsigned short handle
        , Bus bus
        , unsigned int targetAddress
        , unsigned int messageId
        , BYTE* payload
        , int payloadLength
        ) = 0;

    /// This event will be raised, when a LIN Message was not transmitted, because of bus errors.
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] handle The handle which was given to the correspondic transmitter / receiver.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] targetAddress The idententifier addressing the target device.
    /// \param[out] messageId The identifier of the message.
    /// \param[out] payload The payload which was attached to the message.
    /// \param[out] payloadLength The length of the attached payload.
    virtual void OnLinTxFailedMessage
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , unsigned short handle
        , Bus bus
        , unsigned int targetAddress
        , unsigned int messageId
        , BYTE* payload
        , int payloadLength
        ) = 0;

    /// This event will be raised, when a LIN bus error has occured.
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] handle The handle which was given to the correspondic transmitter / receiver.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] evt Enumeration identifying the type of bus event.
    /// \param[out] error Enumeration identifying the type of bus error.
    virtual void OnLinBusError
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , Bus bus
        , TBusEvent evt
        , TLinError error
        ) = 0;

    /// This event will be raised, when a LIN TP Message has been received.
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] messageId The identifier of the message.
    /// \param[out] frameId Identifier of message. Request is 60 and response is 61.
    /// \param[out] nodeAddress Address of the target node.
    /// \param[out] serviceId Identifier of the service.
    /// \param[out] payload The payload which was attached to the message.
    /// \param[out] payloadLength The length of the attached payload.
    virtual void OnLinTpRxMessage
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , Bus bus
        , unsigned int messageId
        , BYTE frameId
        , BYTE nodeAddress
        , BYTE serviceid
        , BYTE* payload
        , int payloadLength
        ) = 0;

    /// This event will be raised, when a LIN TP Message has been succesful sent.
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] handle The handle which was given to the correspondic transmitter / receiver.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] messageId The identifier of the message.
    /// \param[out] frameId Identifier of message. Request is 60 and response is 61.
    /// \param[out] nodeAddress Address of the target node.
    /// \param[out] serviceId Identifier of the service.
    /// \param[out] payload The payload which was attached to the message.
    /// \param[out] payloadLength The length of the attached payload.
    virtual void OnLinTpTxMessage
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , unsigned short handle
        , Bus bus
        , unsigned int messageId
        , BYTE frameId
        , BYTE nodeAddress
        , BYTE serviceid
        , BYTE* payload
        , int payloadLength
        ) = 0;

    /// This event will be raised, when a LIN TP Message was not transmitted, because of bus errors.
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] handle The handle which was given to the correspondic transmitter / receiver.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] messageId The identifier of the message.
    /// \param[out] frameId Identifier of message. Request is 60 and response is 61.
    /// \param[out] nodeAddress Address of the target node.
    /// \param[out] serviceId Identifier of the service.
    /// \param[out] payload The payload which was attached to the message.
    /// \param[out] payloadLength The length of the attached payload.
    virtual void OnLinTpTxFailedMessage
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , unsigned short handle
        , Bus bus
        , unsigned int messageId
        , BYTE frameId
        , BYTE nodeAddress
        , BYTE serviceid
        , BYTE* payload
        , int payloadLength
        ) = 0;

    /// This event will be raised, when a LIN TP bus error has occured.
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] handle The handle which was given to the correspondic transmitter / receiver.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] evt Enumeration identifying the type of bus event.
    /// \param[out] error Enumeration identifying the type of bus error.
    virtual void OnLinTpBusError
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , Bus bus
        , TBusEvent evt
        , TLinTpError error
        ) = 0;

    /// This event will be raised, when a LIN Spy event has been received.
    /// \param[out] pAbi Used IABI instance.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp The occurrence of the event, represented in steps of 100 nanoseconds.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] evt Bitmask. Use TLinSpyEvent to decode it.
    virtual void OnLinSpyEvent
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , Bus bus
        , TLinSpyEvent evt
        ) = 0;

    /// This event will be raised, when a LIN Spy message has been received.
    /// \param[out] pAbi Used IABI instance.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp The occurrence of the event, represented in steps of 100 nanoseconds.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] payload The payload.
    virtual void OnLinSpyMessage
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , Bus bus
        , BYTE payload
        ) = 0;

    /// This event will be raised, when a FlexRay Message has been received.
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] messageId The identifier of the message.
    /// \param[out] frameId The frame identifier.
    /// \param[out] offset The offset of the used frame cycle.
    /// \param[out] repetition The repetition of the used frame cycle.
    /// \param[out] frChannel The used FlexRay channel(s).
    /// \param[out] frCycle The cycle slot used for this message.
    /// \param[out] payload The payload which was attached to the message.
    /// \param[out] payloadLength The length of the attached payload.
    virtual void OnFlexrayRxMessage       
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , Bus bus
        , unsigned int messageId
        , unsigned short frameId
        , BYTE offset
        , BYTE repetition
        , FrChannel frChannel
        , unsigned short frCycle
        , BYTE* payload
        , int payloadLength
        ) = 0;

    /// This event will be raised, when a FlexRay Message has been succesful sent.
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] handle The handle which was given to the correspondic transmitter / receiver.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] messageId The identifier of the message.
    /// \param[out] frameId The frame identifier.
    /// \param[out] offset The offset of the used frame cycle.
    /// \param[out] repetition The repetition of the used frame cycle.
    /// \param[out] frChannel The used FlexRay channel(s).
    /// \param[out] frCycle The cycle slot used for this message.
    /// \param[out] payload The payload which was attached to the message.
    /// \param[out] payloadLength The length of the attached payload.
    virtual void OnFlexrayTxMessage       
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , unsigned short handle
        , Bus bus
        , unsigned int messageId
        , unsigned short frameId
        , BYTE offset
        , BYTE repetition
        , FrChannel frChannel
        , unsigned short frCycle
        , BYTE* payload
        , int payloadLength
        ) = 0;

    /// This event will be raised, when a FlexRay Message was not transmitted, because of bus errors.
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] handle The handle which was given to the correspondic transmitter / receiver.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] messageId The identifier of the message.
    /// \param[out] frameId The frame identifier.
    /// \param[out] offset The offset of the used frame cycle.
    /// \param[out] repetition The repetition of the used frame cycle.
    /// \param[out] frChannel The used FlexRay channel(s).
    /// \param[out] frCycle The cycle slot used for this message.
    /// \param[out] payload The payload which was attached to the message.
    /// \param[out] payloadLength The length of the attached payload.
    virtual void OnFlexrayTxFailedMessage 
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , unsigned short handle
        , Bus bus
        , unsigned int messageId
        , unsigned short frameId
        , BYTE offset
        , BYTE repetition
        , FrChannel frChannel
        , unsigned short frCycle
        , BYTE* payload
        , int payloadLength
        ) = 0;

    /// This event will be raised, when a FlexRay specific bus event has occured.
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] handle The handle which was given to the correspondic transmitter / receiver.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] param1 Depending on the type of bus event (evt), this parameter holds the main value which belongs to this event.
    /// \param[out] param2 Depending on the type of bus event (evt), this parameter holds the additional value which belongs to this event.
    virtual void OnFlexrayEventMessage    
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , Bus bus
        , TBusEvent evt
        ) = 0;

    /// This event will be raised, when a CAN TP Message has been received.
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] handle The handle which was given to the correspondic transmitter / receiver.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] srcmessageId The CAN TP source address.
    /// \param[out] destmessageId  The CAN TP destination address.
    /// \param[out] tpAddressingType Determinates the type of CAN TP protocol. 
    /// \param[out] tpOpResult Enumaration determinating the result of the CAN TP transmission.
    /// \param[out] payload The payload which was attached to the message.
    /// \param[out] payloadLength The length of the attached payload.
    virtual void OnCanTpRxMessage 
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , unsigned short handle
        , Bus bus
        , unsigned int srcMessageId
        , unsigned int destMessageId
        , TPAddressingType tpAddressingType
        , TPOpResult tpOpResult
        , BYTE* payload
        , int payloadLength
        ) = 0;

    /// This event will be raised, when a CAN TP Message has been succesful sent.
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] handle The handle which was given to the correspondic transmitter / receiver.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] srcmessageId The CAN TP source address.
    /// \param[out] destmessageId  The CAN TP destination address.
    /// \param[out] tpAddressingType Determinates the type of CAN TP protocol. 
    /// \param[out] tpOpResult Enumaration determinating the result of the CAN TP transmission.
    virtual void OnCanTpTxMessage 
        ( IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , unsigned short handle
        , Bus bus
        , unsigned int srcMessageId
        , unsigned int destMessageId
        , TPAddressingType tpAddressingType
        , TPOpResult tpOpResult
        ) = 0;

    /// This event will be raised, when e.g the configuration of CAN TP has changed.
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] pContext Delivers the object, which was stored by the RegisterBusContext() method.
    /// \param[out] timeStamp Timestamp of event in unit of 100 nanoseconds.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] evt Enumeration identifying the type of bus event.
    /// \param[out] param1 Depending on the type of bus event (evt), this parameter holds the main value which belongs to this event.
    /// \param[out] param2 Depending on the type of bus event (evt), this parameter holds the additional value which belongs to this event.
    virtual void OnCanEventMessage
        (IABI* pAbi
        , void* pContext
        , unsigned long long timeStamp
        , Bus bus
        , TBusEvent evt
        , unsigned int param1
        , unsigned int param2
        ) = 0;
            
    /// This event will be raised, when a message is received by the StressNIC.
    /// \param[out] pAbi ABI instance that received event.
    /// \param[out] context Delivers the object, which was stored by the RegisterBusContext() method. (Bus context)
    /// \param[out] timeStamp The occurrence of the event, represented in steps of 100 nanoseconds.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] deviceid The source device id.
    /// \param[out] fblockId The fblock identifier.
    /// \param[out] instanceId The instance identifier.
    /// \param[out] functionId The function identifier.
    /// \param[out] opType The operation type.
    /// \param[out] payload The payload.
    /// \param[out] payloadLength The payload length.
    virtual void OnStressNicMessageReceived
        (IABI* abi
        , void* context
        , unsigned long long timeStamp
        , Bus bus
        , unsigned short deviceid
        , BYTE fblockId
        , BYTE instanceId
        , unsigned short functionId
        , BYTE opType
        , BYTE* payload
        , int payloadLength) = 0;

    /// Called when trigger level changed.
    /// \param[out] abi ABI instance that received event.
    /// \param[out] context Delivers the object, which was stored by the RegisterBusContext() method. (Bus context)
    /// \param[out] timeStamp The occurrence of the event, represented in steps of 100 nanoseconds.
    /// \param[out] bus The type and instance number of the bus.
    /// \param[out] isActive true if the trigger level is active, otherwise false.
    /// \param[out] isTx true if the trigger level was changed by MOCCA compact, otherwise false.
    virtual void OnTriggerLevelChanged
        (IABI* abi
        , void* context
        , unsigned long long timeStamp
        , Bus bus
        , bool isActive
        , bool isTx) = 0;
};

/*============================================================================*/
/// \brief Default implementation of IABIListener interface. To add custom functionality override the desired function with own code.
/// \ingroup ABI
/*============================================================================*/
class K2LABI_API CABIListener : public IABIListener
{
public:
    virtual ~CABIListener() {}

    virtual void OnError(IABI* pAbi) {}
    virtual void OnMessageLost(IABI* pAbi) {}

    virtual void OnInternalEvent (IABI* pAbi, unsigned long long timeStamp, Parameter tEvent, unsigned int param1, unsigned int param2){}
    virtual void OnGeneralEvent (IABI* pAbi, unsigned long long timeStamp, BYTE identifier, BYTE* payload, int payloadLength){}
    virtual void OnBusRunningStateChanged ( IABI* pAbi, void* pContext, unsigned long long timeStamp, Bus bus, bool isStarted){}
    virtual void OnRelayStateChanged( IABI* pAbi, void* pContext, unsigned long long timeStamp, Bus bus, bool isClosed){}
    virtual void OnTxPayloadFilterChanged( IABI* pAbi, void* pContext, unsigned long long timeStamp, Bus bus, bool isEnabled){}
    virtual void OnObjectStateChanged( IABI* pAbi, void* pContext, unsigned long long  timeStamp, Bus bus, ObjectType type, unsigned short handle, BYTE groupId, ObjectState state){}

    virtual void OnCanRxMessage       (IABI* pAbi, void* pContext, unsigned long long timeStamp, Bus bus, unsigned int messageId, bool extended, BYTE* payload, int payloadLength){}
    virtual void OnCanTxMessage       (IABI* pAbi, void* pContext, unsigned long long timeStamp, unsigned short handle, Bus bus, unsigned int messageId, bool extended, BYTE* payload, int payloadLength){}
    virtual void OnCanTxFailedMessage (IABI* pAbi, void* pContext, unsigned long long timeStamp, unsigned short handle, Bus bus, unsigned int messageId, bool extended, BYTE* payload, int payloadLength){}
    virtual void OnCanBusError        (IABI* pAbi, void* pContext, unsigned long long timeStamp, Bus bus, TBusEvent evt, TCanError error){}
    virtual void OnCanEventMessage    (IABI* pAbi, void* pContext, unsigned long long timeStamp, Bus bus, TBusEvent evt, unsigned int param1, unsigned int param2){}

    virtual void OnMostRxMessage       (IABI* pAbi, void* pContext, unsigned long long timeStamp, Bus bus, ReceiveType rcvType, unsigned short source, MostProtocol prot, unsigned int messageId, BYTE fBlock, BYTE instance, unsigned short function, BYTE opType, BYTE* payload, int payloadLength){}
    virtual void OnMostRxFailedMessage       (IABI* pAbi, void* pContext, unsigned long long timeStamp, Bus bus, MostRxStatus status, ReceiveType rcvType, unsigned short source, MostProtocol prot, unsigned int messageId, BYTE fBlock, BYTE instance, unsigned short function, BYTE opType, BYTE* payload, int payloadLength){}
    virtual void OnMostHighExtRxMessage       (IABI* pAbi, void* pContext, unsigned long long timeStamp, Bus bus, MostRxStatus status, ReceiveType rcvType, unsigned short source, MostProtocol prot, unsigned int messageId, BYTE fBlock, BYTE instance, unsigned short function, BYTE opType, BYTE segmentationId, unsigned short blockCounter, BYTE* payload, int payloadLength){}
    virtual void OnMostTxMessage       (IABI* pAbi, void* pContext, unsigned long long timeStamp, unsigned short handle, Bus bus, unsigned short destination, MostProtocol prot, unsigned int messageId, BYTE fBlock, BYTE instance, unsigned short function, BYTE opType, BYTE* payload, int payloadLength){}
    virtual void OnMostTxNAKMessage       (IABI* pAbi, void* pContext, unsigned long long timeStamp, unsigned short handle, Bus bus, unsigned short destination, MostProtocol prot, unsigned int messageId, BYTE fBlock, BYTE instance, unsigned short function, BYTE opType, BYTE* payload, int payloadLength){}
    virtual void OnMostTxFailedMessage       (IABI* pAbi, void* pContext, unsigned long long timeStamp, MostTxStatus status, unsigned short handle, Bus bus, unsigned short destination, MostProtocol prot, unsigned int messageId, BYTE fBlock, BYTE instance, unsigned short function, BYTE opType, BYTE* payload, int payloadLength){}
    virtual void OnMostEventMessage    (IABI* pAbi, void* pContext, unsigned long long timeStamp, Bus bus, TBusEvent evt, unsigned int param1, unsigned int param2) {}
    virtual void OnMostHighRxStatusEvent(IABI* abi, void* context, unsigned long long timeStamp, Bus bus, MostRxStatus status, unsigned int messageId, BYTE fBlock, BYTE instance, unsigned short function, BYTE opType, unsigned short source) {}     
    virtual void OnMostHighTxStatusEvent(IABI* abi, void* context, unsigned long long timeStamp, Bus bus, MostTxStatus status, unsigned int messageId, BYTE fBlock, BYTE instance, unsigned short function, BYTE opType, unsigned short destination) {}

    virtual void OnMostAudioStreamingEvent(IABI* pAbi, void* pContext, unsigned long long timeStamp, unsigned int label, AudioConnectionType type, AudioBandwidth bandwith, bool isOpenEvent, AudioConnectorType connector){}
    virtual void OnMostAudioStreamingError(IABI* pAbi, void* pContext, unsigned long long timeStamp, unsigned short error, AudioConnectionType type, AudioBandwidth bandwith, bool isOpenEvent, AudioConnectorType connector){}
    virtual void OnMostAllocationTableChanged( IABI* pAbi, void* pContext, unsigned long long timeStamp, unsigned short& length, AllocationTableEntry* allocationTable){}

    virtual void OnLinRxMessage(IABI* pAbi, void* pContext, unsigned long long timeStamp, Bus bus, unsigned int sourceAddress, unsigned int messageId, BYTE* payload, int payloadLength){}
    virtual void OnLinTxMessage(IABI* pAbi, void* pContext, unsigned long long timeStamp, unsigned short handle, Bus bus, unsigned int targetAddress, unsigned int messageId, BYTE* payload, int payloadLength){}
    virtual void OnLinTxFailedMessage(IABI* pAbi, void* pContext, unsigned long long timeStamp, unsigned short handle, Bus bus, unsigned int targetAddress, unsigned int messageId, BYTE* payload, int payloadLength){}
    virtual void OnLinBusError(IABI* pAbi, void* pContext, unsigned long long timeStamp, Bus bus, TBusEvent evt, TLinError error){}
    virtual void OnLinTpRxMessage( IABI* pAbi, void* pContext, unsigned long long timeStamp, Bus bus, unsigned int messageId, BYTE frameId, BYTE nodeAddress, BYTE serviceid, BYTE* payload, int payloadLength) {}
    virtual void OnLinTpTxMessage( IABI* pAbi, void* pContext, unsigned long long timeStamp, unsigned short handle, Bus bus, unsigned int messageId, BYTE frameId, BYTE nodeAddress, BYTE serviceid, BYTE* payload, int payloadLength) {}
    virtual void OnLinTpTxFailedMessage( IABI* pAbi, void* pContext, unsigned long long timeStamp, unsigned short handle, Bus bus, unsigned int messageId, BYTE frameId, BYTE nodeAddress, BYTE serviceid, BYTE* payload, int payloadLength) {}
    virtual void OnLinTpBusError( IABI* pAbi, void* pContext, unsigned long long timeStamp, Bus bus, TBusEvent evt, TLinTpError error) {}
    virtual void OnLinSpyEvent( IABI* pAbi, void* pContext, unsigned long long timeStamp, Bus bus, TLinSpyEvent evt) {}
    virtual void OnLinSpyMessage( IABI* pAbi, void* pContext, unsigned long long timeStamp, Bus bus, BYTE payload) {}

    virtual void OnFlexrayRxMessage       (IABI* pAbi, void* pContext, unsigned long long timeStamp, Bus bus, unsigned int messageId, unsigned short frameId, BYTE offset, BYTE repetition, FrChannel frChannel, unsigned short frCycle, BYTE* payload, int payloadLength){}
    virtual void OnFlexrayTxMessage       (IABI* pAbi, void* pContext, unsigned long long timeStamp, unsigned short handle, Bus bus, unsigned int messageId, unsigned short frameId, BYTE offset, BYTE repetition, FrChannel frChannel, unsigned short frCycle, BYTE* payload, int payloadLength){}
    virtual void OnFlexrayTxFailedMessage (IABI* pAbi, void* pContext, unsigned long long timeStamp, unsigned short handle, Bus bus, unsigned int messageId, unsigned short frameId, BYTE offset, BYTE repetition, FrChannel frChannel, unsigned short frCycle, BYTE* payload, int payloadLength){}
    virtual void OnFlexrayEventMessage    (IABI* pAbi, void* pContext, unsigned long long timeStamp, Bus bus, TBusEvent evt){}

    virtual void OnCanTpRxMessage (IABI* pAbi, void* pContext, unsigned long long timeStamp, unsigned short handle, Bus bus, unsigned int srcMessageId, unsigned int destMessageId, TPAddressingType tpAddressingType, TPOpResult tpOpResult, BYTE* payload, int payloadLength){}
    virtual void OnCanTpTxMessage (IABI* pAbi, void* pContext, unsigned long long timeStamp, unsigned short handle, Bus bus, unsigned int srcMessageId, unsigned int destMessageId, TPAddressingType tpAddressingType, TPOpResult tpOpResult){}

    virtual void OnStressNicMessageReceived (IABI* abi, void* context, unsigned long long timeStamp, Bus bus, unsigned short deviceId, BYTE fblockId, BYTE instanceId, unsigned short functionId, BYTE opType, BYTE* payload, int payloadLength) {}

    virtual void OnMostMepTxMessage ( IABI* pAbi, void* pContext, unsigned long long timeStamp, unsigned short handle, Bus bus, unsigned long long destination, BYTE* payload, int payloadLength ) {}
    virtual void OnMostMepRxMessage( IABI* pAbi, void* pContext, unsigned long long timeStamp, Bus bus, unsigned long long source, BYTE* payload, int payloadLength) {}
    virtual void OnMostMepBridgeStatusChanged( IABI* pAbi, void* pContext, unsigned long long timeStamp, Bus bus, bool isEnabled, MepBridgeOptions options ) {}
    virtual void OnMostMacAddressChanged( IABI* pAbi, void* pContext, unsigned long long timeStamp, Bus bus, unsigned long long macAddress ) {}
    
    virtual void OnTriggerLevelChanged (IABI* abi, void* context, unsigned long long timeStamp, Bus bus, bool isActive, bool isTx) {}
};

/*============================================================================*/
/// \brief Helper class to initialize the ABI device.
/// \ingroup ABI
/*============================================================================*/

typedef void (AbiIpcDevice_RxCB_t)( const uint8_t * data, uint32_t dataLength, void *cbArg);
            
class K2LABI_API AbiIpcDevice
{
public:
    virtual ~AbiIpcDevice() {}
    virtual int Send( const BYTE *data, DWORD length ) { return 0; }
    virtual void SetReceivePriority( int prio ) {};
    virtual void SetReceiveCallback( AbiIpcDevice_RxCB_t receiveCB, void *cbArg ) {};
};
            
/*============================================================================*/
/// \brief Interface of ABI.
/// \ingroup ABI
/*============================================================================*/
class K2LABI_API IABI
{
public:
    virtual ~IABI() { }

    /// Initialize ABI client instance.
    /// \param[in] pInitData Device that should be connected to.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    virtual int Init(AbiIpcDevice* abiDevice) = 0;

    /// Initialize ABI client instance.
    /// \param[in] pInitData Device that should be connected to.
    /// \param[in] priority Priority of the receive thread.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    virtual int Init(AbiIpcDevice* abiDevice, int priority) = 0;
    
    /// Deinitialize ABI client instance.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    virtual int Deinit(void) = 0;
    
    /// Start event processing of the ABI client.
    /// \param[in] Listener that should be process incoming events.
    virtual void Start(IABIListener* pListener) = 0;
    
    /// Stop event processing of the ABI client.
    virtual void Stop(void) = 0;
    
    /// Gives the possibility to assign a object to a specific bus. This object will be given back in every callback which the specific bus was involved.
    virtual void RegisterBusContext(Bus bus, void* context) = 0;
    
    /// Removes a assigned a object from a specific bus.
    virtual void UnregisterBusContext(Bus bus) = 0;
    
    /// Returns the assigned object for the given bus.
    /// \return The assigned object for the given bus, or NULL if there is was nothing registered.
    virtual void* GetBusContext(Bus bus) = 0;
    
    /// Raw send method, which is internaly used by ABI to comunicate with ABI hardware.
    /// \param[in] ifc Addresses the internal processing unit in the ABI device.
    /// \param[in] function Function id which addresses the ABI command.
    /// \param[in] data The request data, which will be send to the ABI device.
    /// \param[in] len The length of the request data.
    /// \param[in] resultLen The length of the response, which was answered from the ABI device.
    /// \param[in] errorCode Errorcode, 0 if successful.
    /// \return Byte array containing the response from the ABI device.
    /// \remarks After this command a EndRequest call is required. 
    virtual BYTE* SendRequest(BYTE ifc, BYTE function, BYTE* data, int len, int& resultLen, BYTE& errorCode) = 0;
    
    /// Raw send method, which is internaly used by ABI to comunicate with ABI hardware.
    /// \param[in] ifc Addresses the internal processing unit in the ABI device.
    /// \param[in] function Function id which addresses the ABI command.
    /// \param[in] data The request data, which will be send to the ABI device.
    /// \param[in] len The length of the request data.
    /// \param[in] resultLen The length of the response, which was answered from the ABI device.
    /// \param[in] errorCode Errorcode, 0 if successful.
    /// \param[in] timeout Timespan to wait for an answer of this request (in milliseconds).
    /// \return Byte array containing the response from the ABI device.
    /// \remarks After this command a EndRequest call is required. 
    virtual BYTE* SendRequest(BYTE ifc, BYTE function, BYTE* data, int len, int& resultLen, BYTE& errorCode, int timeout) = 0;
     
    /// Ends the request.
    /// \remarks This call is required after analyzing the return value of SendRequest. Otherwise next Requests will be blocked.
    /// </remarks>
    virtual void EndRequest(void) = 0;

    /// Determinates if the ABI client is set into spy mode. When active, only harmless ABI commands are accepted. So no influence to the automotive buses is granted.
    /// \return TRUE if spy is active, otherwise FALSE.
    virtual bool  IsSpyMode(void) = 0;
    
    /// Sets the ABI client into spy mode. When active, only harmless ABI commands are accepted. So no influence to the automotive buses is granted.
    /// \param[in] bSpyMode Mode that should be set.
    virtual void  SetSpyMode(bool bSpyMode) = 0;

    /// Adds the id interval filter.
    /// \param[in] bus The bus on that the filter should set.
    /// \param[in] handle The handle that represents the filter.
    /// \param[in] minId The min id.
    /// \param[in] maxId The max id.
    virtual void AddIdIntervalFilter(Bus bus, unsigned short handle, unsigned int minId, unsigned int maxId) = 0;

    /// Adds the masked id filter.
    /// \param[in] bus The bus on that the filter should set.
    /// \param[in] handle The handle that represents the filter.
    /// \param[in] id The id.
    /// \param[in] mask The mask.
    virtual void AddMaskedIdFilter(Bus bus, unsigned short handle, unsigned int id, unsigned int mask) = 0;

    /// Deletes a selected filter.
    /// \param[in] handle The handle that represents the filter.
    virtual void RemoveFilter(unsigned short handle) = 0;
};

/*============================================================================*/
/// \brief Factory to create new ABI instances.
/// \ingroup ABI
/*============================================================================*/
class K2LABI_API CABIFactory
{
public:
    /// Creates new instance of ABI.
    /// \return New ABI instance.
    static IABI* CreateAbi();

    /// Destroys instance of ABI.
    /// \param[in] pAbi ABI instance to destroy.
    static void DestroyAbi(IABI* pAbi);

};

        }
    }
}

/*============================================================================*/
/* C Interface                                                                */
/*============================================================================*/

extern "C"
{
    K2LABI_API K2L::Automotive::ABI::IABI* CreateABI (void);
    K2LABI_API void  DestroyABI (K2L::Automotive::ABI::IABI* pAbi);
}

//@}
/*============================================================================*/
/* END OF FILE                                                                */
/*============================================================================*/

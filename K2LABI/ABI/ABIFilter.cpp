#include "ABIFilter.h"

CABIFilter::CABIFilter()
{
    for(int index = 0; index < 256; index++)
    {
        m_filters[index] = NULL;
    }
}

//-----------------------------------------------------------------------------

CABIFilter::~CABIFilter()
{
    for(int index = 0; index < 256; index++)
    {
        if(m_filters[index] != NULL)
        {
            delete m_filters[index];
            m_filters[index] = NULL;
        }
    }
}

//-----------------------------------------------------------------------------

void CABIFilter::AddIntervalFilter(Bus bus, unsigned short handle, unsigned int min, unsigned int max)
{
    int index = (int) bus;
    if (m_filters[index] == NULL)
    {
        m_filters[index] = new CIdFilter();
    }
    m_filters[index]->AddIntervalFilter(handle, min, max);
}

//-----------------------------------------------------------------------------

void CABIFilter::AddMaskedFilter(Bus bus, unsigned short handle, unsigned int id, unsigned int mask)
{
    int index = (int)bus;
    if (m_filters[index] == NULL)
    {
        m_filters[index] = new CIdFilter();
    }
    m_filters[index]->AddMaskedFilter(handle, id, mask);
}

//-----------------------------------------------------------------------------

void CABIFilter::RemoveFilter(unsigned short handle)
{
    for (int index = 0; index < 256; index++)
    {
        if (m_filters[index] != NULL)
        {
            m_filters[index]->RemoveFilter(handle);
        }
    }
}

//-----------------------------------------------------------------------------

bool CABIFilter::Match(Bus bus, unsigned int id)
{
    int index = (int)bus;
    CIdFilter* filter = m_filters[index];
    if (filter == NULL)
    {
        //if no filters are added all messages will be delivered.
        return true;
    }
    return filter->Match(id);
}

//-----------------------------------------------------------------------------

IntervalIdFilterEntry::IntervalIdFilterEntry(unsigned short handle, unsigned int min, unsigned int max) :
m_handle(handle),
m_min(min),
m_max(max)
{
}

unsigned short IntervalIdFilterEntry::Handle()
{
    return m_handle;
}

//-----------------------------------------------------------------------------

bool IntervalIdFilterEntry::Match(unsigned int id)
{
    return ((m_min <= id) && (id <= m_max));
}

//-----------------------------------------------------------------------------

MaskedIdFilterEntry::MaskedIdFilterEntry(unsigned short handle, unsigned int id, unsigned int mask) :
    m_handle(handle),
    m_id(id),
    m_mask(mask)
{
}

//-----------------------------------------------------------------------------

unsigned short MaskedIdFilterEntry::Handle()
{
    return m_handle;
}

//-----------------------------------------------------------------------------

bool MaskedIdFilterEntry::Match(unsigned int id)
{
    return ((id & m_mask) == (m_id & m_mask));
}

//-----------------------------------------------------------------------------

CIdFilter::CIdFilter() :
m_filters()
{
    ::InitializeCriticalSection(&m_criticalSection);
}

//-----------------------------------------------------------------------------

CIdFilter::~CIdFilter()
{
    ::DeleteCriticalSection(&m_criticalSection);
}

//-----------------------------------------------------------------------------

void CIdFilter::AddIntervalFilter(unsigned short handle, unsigned int min, unsigned int max)
{
    IntervalIdFilterEntry* filter = new IntervalIdFilterEntry(handle, min, max);
    m_filters.push_back(filter);
}

//-----------------------------------------------------------------------------

void CIdFilter::AddMaskedFilter(unsigned short handle, unsigned int id, unsigned int mask)
{
    MaskedIdFilterEntry* filter = new MaskedIdFilterEntry(handle, id, mask);
    m_filters.push_back(filter);
}

//-----------------------------------------------------------------------------

void CIdFilter::RemoveFilter(unsigned short handle)
{
    ::EnterCriticalSection(&m_criticalSection);
    for (unsigned int index = 0; index < m_filters.size(); index++)
    {
        IFilterEntry* filter = m_filters[index];
        if (filter->Handle() == handle)
        {
            m_filters.erase (m_filters.begin() + index);
            delete filter;
            break;
        }
    }
    ::LeaveCriticalSection(&m_criticalSection);
}

//-----------------------------------------------------------------------------

bool CIdFilter::Match(unsigned int id)
{
    //TODO: PORT this unix ("for each" does not exist)
#if 0
    ::EnterCriticalSection(&m_criticalSection);
    bool ret = false;
    for each (IFilterEntry* filterEntry in m_filters)
    {
        if (filterEntry->Match(id))
        {
            ret = true;
            break;
        }
    }
    ::LeaveCriticalSection(&m_criticalSection);
    return ret;
#else
    return true;
#endif
}

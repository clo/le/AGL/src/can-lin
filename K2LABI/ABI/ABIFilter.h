#pragma once

#include "windows-adapter.h"
#include "K2LABI.h"
#include <vector>

using namespace K2L::Automotive::ABI;

class IFilterEntry
{
public:
    virtual ~IFilterEntry(){}

    virtual unsigned short Handle() = 0;
    virtual bool Match(unsigned int id) = 0;
};

class IntervalIdFilterEntry : public IFilterEntry
{
private:
    IntervalIdFilterEntry(){}

    unsigned short m_handle;
    unsigned int m_min;
    unsigned int m_max;
public:
    IntervalIdFilterEntry(unsigned short handle, unsigned int min, unsigned int max);
    virtual ~IntervalIdFilterEntry(){}

    unsigned short Handle();
    bool Match(unsigned int id);
};

class MaskedIdFilterEntry : public IFilterEntry
{
private:
    MaskedIdFilterEntry(){}

    unsigned short m_handle;
    unsigned int m_id;
    unsigned int m_mask;
public:
    MaskedIdFilterEntry(unsigned short handle, unsigned int id, unsigned int mask);
    virtual ~MaskedIdFilterEntry(){}

    unsigned short Handle();
    bool Match(unsigned int id);
};

class CIdFilter
{
private:
    std::vector<IFilterEntry*> m_filters;
    CRITICAL_SECTION m_criticalSection;

public:
    CIdFilter();
    virtual ~CIdFilter();

    void AddIntervalFilter(unsigned short handle, unsigned int min, unsigned int max);
    void AddMaskedFilter(unsigned short handle, unsigned int id, unsigned int mask);
    void RemoveFilter(unsigned short handle);
    
    bool Match(unsigned int id);
};

class CABIFilter
{
private:
    CIdFilter* m_filters[256];

public:
    CABIFilter();
    virtual ~CABIFilter();

    void AddIntervalFilter(Bus bus, unsigned short handle, unsigned int min, unsigned int max);
    void AddMaskedFilter(Bus bus, unsigned short handle, unsigned int id, unsigned int mask);
    void RemoveFilter(unsigned short handle);

    bool Match(Bus bus, unsigned int id);
};

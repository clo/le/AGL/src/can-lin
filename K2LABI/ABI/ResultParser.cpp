#include "k2l-type.h"
#include "ResultParser.h"
#include <string.h>

BYTE* CResultParser::ToByteArray(ParamDef* pParams, int nParamCount, int& nResultLength)
{
    int nLength = 0;
    for (int i = 0; i < nParamCount; i++)
    {
        nLength += pParams[i].m_nLength;
    }

    BYTE* pBuffer = NULL;
    if (nLength)
    {
        nLength = (nLength + 1) & ~1;
        pBuffer = new BYTE[nLength];
        memset(pBuffer, 0, nLength);
        BYTE* pTemBuf = pBuffer;
        for(int i = 0; i < nParamCount; i++)
        {
            memcpy(pTemBuf, pParams[i].m_pData, pParams[i].m_nLength);
            pTemBuf += pParams[i].m_nLength;
        }
    }

    nResultLength = nLength;
    return pBuffer;
}

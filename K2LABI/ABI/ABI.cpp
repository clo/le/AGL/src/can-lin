#include <string.h>
#include "ABI.h"
#include "ABIFunctionDefinition.h"
#include "ABICommands.h"

#define IPC_COMMAND_CHANNEL 0x07
#define IPC_HEADER_LENGTH 0x06
#define MAX_RESULT_BUFFER_SIZE (70 * 1024)


static void IpcReceiveCallback( const uint8_t * data, uint32_t dataLength, void *cbArg)
{
    if (NULL == cbArg)
    {
        fprintf(stderr, "IPC receive callback did not provide the given class pointer\n");
    }
    CABI *that = (CABI *)cbArg;
    if (NULL != that)
    {
        that->m_pIpcEvents->Decode((BYTE*)data, dataLength);
        that->m_pIpcCommands->Decode((BYTE*)data, dataLength);
    }
}

//-----------------------------------------------------------------------------

CABI::CABI() : 
    m_dwUniqueId(0xDEADAFFE),
    m_bSpyMode(false),
    m_pAbiListener(NULL),
    m_bStarted(false),
    m_pResult(NULL),
    m_pIpcCommands(NULL),
    m_pIpcEvents(NULL)

{
    memset(m_objectMap, 0, sizeof(void*) * MAX_CONTEXT_OBJECT);
    m_requestEvent.create();
}

//-----------------------------------------------------------------------------

CABI::~CABI()
{
    m_requestEvent.close();
    CIpcFactory::DestroyIpc(m_pIpcCommands);
    m_pIpcCommands = NULL;
    CIpcFactory::DestroyIpc(m_pIpcEvents);
    m_pIpcEvents = NULL;
}

//-----------------------------------------------------------------------------

int CABI::Init (AbiIpcDevice* abiDevice, int prio)
{
    int ret = Init(abiDevice);
    if(NULL != m_abiDevice)
    {
        m_abiDevice->SetReceivePriority(prio);
    }
    return ret;
}

//-----------------------------------------------------------------------------

int  CABI::Init(AbiIpcDevice* abiDevice)
{
    int nErr = 0;
    m_abiDevice = abiDevice;
    if (NULL == m_abiDevice)
        return -1;
    
    m_abiDevice->SetReceiveCallback(IpcReceiveCallback, this);

    if(m_pIpcCommands == NULL)
    {
        m_pIpcCommands = CIpcFactory::CreateIpc();
        nErr = m_pIpcCommands->Init(this);
        if (nErr)
            return nErr;
    }

    if(m_pIpcEvents == NULL)
    {
        m_pIpcEvents = CIpcFactory::CreateIpc();
        nErr = m_pIpcEvents->Init(this);
        if (nErr)
            return nErr;
    }

    m_dwUniqueId = 0xAffe;

    int resultLen = 0; 
    BYTE error = 0;
    BYTE* pRes = SendRequest((BYTE)Board, (BYTE)FunId__GetSerialNumber, NULL, 0, resultLen, error, 1000);
    EndRequest();

    if (!pRes || error != 0)
        return -1;

    return nErr;
}

//-----------------------------------------------------------------------------

int CABI::Deinit(void)
{
    CSafeLock safeLock(&m_lock);
    delete [] m_pResult;
    m_pResult = NULL;
    m_nResultLength = 0;
    m_errorCode = 0;
    
    if (m_pIpcCommands != NULL)
    {
        m_pIpcCommands->Deinit();
        CIpcFactory::DestroyIpc(m_pIpcCommands);
        m_pIpcCommands = NULL;
    }

    if (m_pIpcEvents != NULL)
    {
        m_pIpcEvents->Deinit();
        CIpcFactory::DestroyIpc(m_pIpcEvents);
        m_pIpcEvents = NULL;
    }

    return 0;
}

//-----------------------------------------------------------------------------

void  CABI::Start(IABIListener* pListener)
{
    CSafeLock safeLock(&m_lock);
    m_bStarted = true;
    m_pAbiListener = pListener;
}

//-----------------------------------------------------------------------------

void  CABI::Stop(void)
{
    CSafeLock safeLock(&m_lock);
    m_bStarted = false;
}

//-----------------------------------------------------------------------------

void  CABI::RegisterBusContext(Bus bus, void* context)
{
    m_objectMap[(int)bus] = context;
}

//-----------------------------------------------------------------------------

void  CABI::UnregisterBusContext (Bus bus)
{
    m_objectMap[(int)bus] = NULL;
}

//-----------------------------------------------------------------------------

void* CABI::GetBusContext(Bus bus)
{
    return m_objectMap[(int)bus];
}

//-----------------------------------------------------------------------------

BYTE* CABI::SendRequest(BYTE ifc, BYTE function, BYTE* pData, int nDataLen, int& resultLen, BYTE& errorCode)
{
    return SendRequest(ifc, function, pData, nDataLen, resultLen, errorCode, 3000);
}

//-----------------------------------------------------------------------------

BYTE* CABI::SendRequest(BYTE ifc, BYTE function, BYTE* pData, int nDataLen, int& resultLen, BYTE& errorCode, int timeout)
{
    BYTE* pResult = NULL;
    resultLen = 0;
    errorCode = 0;
    
    BYTE* pRequest = NULL;

    int nRequestLenNeeded = 0;
    int nRequestLen = 0;
    //CSafeLock safeLock(&m_lock);
    m_lock.lock();
    
    if (!m_abiDevice)
        return NULL;
    
    //calculates length of buffer
    m_pIpcCommands->FormatRequest(IPC_COMMAND_CHANNEL, ifc, function, m_dwUniqueId, pData, nDataLen, pRequest, 0, &nRequestLenNeeded);
    pRequest = new BYTE[nRequestLenNeeded];
    nRequestLen = nRequestLenNeeded;
    
    //fills buffer with content
    int nErr = m_pIpcCommands->FormatRequest(IPC_COMMAND_CHANNEL, ifc, function, m_dwUniqueId, pData, nDataLen, pRequest, nRequestLen, &nRequestLenNeeded);
    
    if (nRequestLen == nErr)
    {
        int written = m_abiDevice->Send(pRequest, nRequestLen);

        if (written == nRequestLen)
        {
            if (!m_requestEvent.waitMsec(timeout))
            {
                pResult = m_pResult;
                resultLen = m_nResultLength;
                errorCode = m_errorCode;
            }
        }
    }

    delete [] pRequest;
    pRequest = NULL;

    return pResult;
}

//-----------------------------------------------------------------------------

void CABI::EndRequest(void)
{
    m_lock.unlock();
}

//-----------------------------------------------------------------------------

bool CABI::IsSpyMode(void)
{
    return m_bSpyMode;
}

//-----------------------------------------------------------------------------

void CABI::SetSpyMode(bool bSpyMode)
{
    m_bSpyMode = bSpyMode;
}

//-----------------------------------------------------------------------------

void CABI::OnMessage(IIpc* pIpc, BYTE channel, BYTE errorCode, BYTE* pPacket, int nLen)
{
    if(pIpc == m_pIpcCommands)
    {
        OnCommandResponse(pIpc, channel, errorCode, pPacket, nLen);
    }
    else if(pIpc == m_pIpcEvents)
    {
        OnEvent(pIpc, channel, errorCode, pPacket, nLen);
    }
    else
    {
        //Should never be reached.
    }
}

void CABI::OnError(IIpc* pIpc)
{
    if(m_pAbiListener == NULL)
        return;
    m_pAbiListener->OnError(this);
}

void CABI::OnMessageLost(IIpc* pIpc)
{
    if(m_pAbiListener == NULL)
        return;
    m_pAbiListener->OnMessageLost(this);
}

//-----------------------------------------------------------------------------

inline void CABI::OnCommandResponse(IIpc* pIpc, BYTE channel, BYTE errorCode, BYTE* pPacket, int nLen)
{
    if (channel != IPC_COMMAND_CHANNEL)
        return;

    int requestId = *reinterpret_cast<int*>(pPacket + 2);

    if (requestId != m_dwUniqueId) //Not our request. Will be handled by event thread.
        return;

    if (!m_pResult)
        m_pResult = new BYTE[MAX_RESULT_BUFFER_SIZE];

    m_errorCode = errorCode;
    m_nResultLength = nLen - IPC_HEADER_LENGTH;
    memcpy(m_pResult, pPacket + IPC_HEADER_LENGTH, m_nResultLength);

    m_requestEvent.signal();
}

//-----------------------------------------------------------------------------

inline void CABI::OnEvent(IIpc* pIpc, BYTE channel, BYTE errorCode, BYTE* pPacket, int nLen)
{
    if (channel == IPC_COMMAND_CHANNEL)
        return;

    if ((!m_bStarted) || (NULL == m_pAbiListener))
    {
        return;
    }

    BYTE protocol = (BYTE) (errorCode & 0xF0);
    BYTE eventType = (BYTE) (errorCode & 0x0F);
    Bus bus = (Bus) (channel + protocol);

    switch(channel & 0xF0u)
    {
    case 0x00: //Common
        DecodeCommon(pPacket, nLen, channel);
        break;
    case 0x10: //CAN
        DecodeCanChannels(pPacket, nLen, bus, protocol, eventType);
        break;
    case 0x20: //MOST
        DecodeMostChannels(pPacket, nLen, bus, protocol, eventType);
        break;
    case 0x40: //FlexRay
        DecodeFlexRayChannels(pPacket, nLen, bus, protocol, eventType);
        break;
    case 0x50: //LIN
        DecodeLinChannels(pPacket, nLen, bus, protocol, eventType);
        break;
    case 0xB0: //Relay
        DecodeRelayChannels(pPacket, nLen, bus, protocol, eventType);
        break;
    case 0xD0: //MOST Ethernet
        DecodeMostEthernetChannels(pPacket, nLen, bus, protocol, eventType);
        break;
    default:
        break;
    }
}

//-----------------------------------------------------------------------------

inline void CABI::DecodeCommon(BYTE* pPacket, int nLen, BYTE channel)
{
    BYTE* pBuffer = pPacket;
    unsigned long long timestamp = ReadUInt64(pBuffer);

    switch(channel)
    {
    case 0x01: //Internal
        {
            Parameter parameter = (Parameter) ReadUInt16(pBuffer);
            unsigned int param1 = (unsigned int)ReadUInt32(pBuffer);
            unsigned int param2 = (unsigned int)ReadUInt32(pBuffer);

            m_pAbiListener->OnInternalEvent(this, timestamp, parameter, param1, param2); 
        }
        break;
    case 0x02: //General
        {
            unsigned short identifier = ReadUInt16(pBuffer);
            unsigned short payloadLength = ReadUInt16(pBuffer);
            BYTE* payload = pBuffer;

            m_pAbiListener->OnGeneralEvent(this, timestamp, identifier & 0xFF, payload, payloadLength);
        }
        break;
    case 0x03: //Object State
        {
            ObjectType type = (ObjectType)ReadByte(pBuffer);
            unsigned short handle = ReadUInt16(pBuffer);
            BYTE groupId = ReadByte(pBuffer);
            ObjectState state = (ObjectState)ReadByte(pBuffer);
            Bus bus = (Bus)ReadByte(pBuffer);

            m_pAbiListener->OnObjectStateChanged(this, GetBusContext(bus), timestamp, bus, type, handle, groupId, state);
        }
        break;
    default:
        break;
    }
}

//-----------------------------------------------------------------------------

inline void CABI::DecodeCanChannels(BYTE* pPacket, int nLen, Bus bus, BYTE protocol, BYTE eventType)
{
    BYTE* pBuffer = pPacket;
    unsigned long long timestamp = ReadUInt64(pBuffer);

    switch(eventType)
    {
    case 0x01: //Event
        {
            TBusEvent evt = (TBusEvent) ReadByte(pBuffer);

            unsigned int param1 = (unsigned int)ReadUInt32(pBuffer);
            unsigned int param2 = (unsigned int)ReadUInt32(pBuffer);
            switch (evt)
            {
            case beCANBusError:
                m_pAbiListener->OnCanBusError(this, GetBusContext(bus), timestamp, bus, evt, (TCanError)param1);
                break;
            default:
                m_pAbiListener->OnCanEventMessage(this, GetBusContext(bus), timestamp, bus, evt, param1, param2);
                break;
            }
        } 
        break;
    case 0x02: //Tx
        {
            bool isExtendedCAN = false;
            if(0 < protocol)
                isExtendedCAN = true;

            BYTE status = ReadByte(pBuffer);
            /*BYTE param2 = */ReadByte(pBuffer);
            unsigned short handle = ReadUInt16(pBuffer);
            /*unsigned short target = */ ReadUInt16(pBuffer);
            unsigned int messageId = (unsigned int)ReadUInt32(pBuffer);

            if (!m_filter.Match(bus, messageId))
            {
                return;
            }

            unsigned short payloadLength = ReadUInt16(pBuffer);
            BYTE* payload = pBuffer;

            switch(status)
            {
            case canSuccess:
                m_pAbiListener->OnCanTxMessage(this, GetBusContext(bus), timestamp, handle, bus, messageId, isExtendedCAN, payload, payloadLength);
                break;
            default:
                m_pAbiListener->OnCanTxFailedMessage(this, GetBusContext(bus), timestamp, handle, bus, messageId, isExtendedCAN, payload, payloadLength);
                break;
            }
        }
        break;
    case 0x03: //Rx
        {
            bool isExtendedCAN = false;
            if(0 < protocol)
                isExtendedCAN = true;

            /*BYTE param1 = */ReadByte(pBuffer);
            /*BYTE param2 = */ReadByte(pBuffer);
            /*unsigned short source = */ReadUInt16(pBuffer);
            unsigned int messageId = (unsigned int)ReadUInt32(pBuffer);

            if (!m_filter.Match(bus, messageId))
            {
                return;
            }

            unsigned short payloadLength = ReadUInt16(pBuffer);
            BYTE* payload = pBuffer;

            m_pAbiListener->OnCanRxMessage(this, GetBusContext(bus), timestamp, bus, messageId, isExtendedCAN, payload, payloadLength);
        }
        break;
    case 0x04: //Bus Changed Event
        {
            bool isStarted = (ReadByte(pBuffer) > 0 ? true : false);
            m_pAbiListener->OnBusRunningStateChanged(this, GetBusContext(bus), timestamp, bus, isStarted);
        }
        break;
    case 0x05: //Tx Filter Payload Event
        {
            bool isEnabled = (ReadByte(pBuffer) > 0 ? true : false);
            m_pAbiListener->OnTxPayloadFilterChanged(this, GetBusContext(bus), timestamp, bus, isEnabled);
        }
        break;
    case 0x0A: //Tx CAN TP
        {
            unsigned short handle = ReadUInt16(pBuffer);
            unsigned int source = (unsigned int)ReadUInt32(pBuffer);
            unsigned int destination = (unsigned int)ReadUInt32(pBuffer);
            TPAddressingType addrType = (TPAddressingType) ReadByte(pBuffer);
            TPOpResult opTypeResult = (TPOpResult) ReadByte(pBuffer);

            m_pAbiListener->OnCanTpTxMessage(this, GetBusContext(bus), timestamp, handle, bus, source, destination, addrType, opTypeResult);
        }
        break;
    case 0x0B: //Rx CAN TP
        {
            unsigned short handle = ReadUInt16(pBuffer);
            unsigned int source = (unsigned int)ReadUInt32(pBuffer);
            unsigned int destination = (unsigned int)ReadUInt32(pBuffer);
            TPAddressingType addrType = (TPAddressingType) ReadByte(pBuffer);
            TPOpResult opTypeResult = (TPOpResult) ReadByte(pBuffer);
            unsigned short payloadLength = ReadUInt16(pBuffer);
            BYTE* payload = pBuffer;

            m_pAbiListener->OnCanTpRxMessage(this, GetBusContext(bus), timestamp, handle, bus, source, destination, addrType, opTypeResult, payload, payloadLength);
        }
        break;
    default:
        break;
    }    
}

//-----------------------------------------------------------------------------

inline void CABI::DecodeMostChannels(BYTE* pPacket, int nLen, Bus bus, BYTE protocol, BYTE eventType)
{
    BYTE* pBuffer = pPacket;
    unsigned long long timestamp = ReadUInt64(pBuffer);

    switch(eventType)
    {
    case 0x01: //Event
        {
            TBusEvent evt = (TBusEvent) ReadByte(pBuffer);
            unsigned int param1 = (unsigned int)ReadUInt32(pBuffer);
            unsigned int param2 = (unsigned int)ReadUInt32(pBuffer);

            m_pAbiListener->OnMostEventMessage(this, GetBusContext(bus), timestamp, bus, evt, param1, param2);
        }
        break;
    case 0x02: //Tx
        {
            unsigned short status = ReadByte(pBuffer);
            /*BYTE param2 = */ReadByte(pBuffer);
            unsigned short handle = ReadUInt16(pBuffer);
            unsigned short target = ReadUInt16(pBuffer);
            unsigned int messageId = (unsigned int)ReadUInt32(pBuffer);

            if (!m_filter.Match(bus, messageId))
            {
                return;
            }

            unsigned short payloadLength = ReadUInt16(pBuffer);
            BYTE* payload = pBuffer;

            switch(protocol)
            {
            case 0x00: //AMS
                status += 0x0100;
                switch(status)
                {
                case mostControlSuccess:
                    m_pAbiListener->OnMostTxMessage(this, GetBusContext(bus), timestamp, handle, bus, target, AMS, messageId, (BYTE)(messageId >> 24), (BYTE)((messageId >> 16) & 0xFF), (unsigned short)((messageId >> 4) & 0xFFF), (BYTE)(messageId & 0xF), payload, payloadLength);
                    break;
                case mostControlRetry:
                    m_pAbiListener->OnMostTxNAKMessage(this, GetBusContext(bus), timestamp, handle, bus, target, AMS, messageId, (BYTE)(messageId >> 24), (BYTE)((messageId >> 16) & 0xFF), (unsigned short)((messageId >> 4) & 0xFFF), (BYTE)(messageId & 0xF), payload, payloadLength);
                    break;
                default:
                    m_pAbiListener->OnMostTxFailedMessage(this, GetBusContext(bus), timestamp, (MostTxStatus)status, handle, bus, target, AMS, messageId, (BYTE)(messageId >> 24), (BYTE)((messageId >> 16) & 0xFF), (unsigned short)((messageId >> 4) & 0xFFF), (BYTE)(messageId & 0xF), payload, payloadLength);
                    break;        
                }
                break;

            case 0x60: //CMS
                status += 0x0100;
                switch(status)
                {
                case mostControlSuccess:
                    m_pAbiListener->OnMostTxMessage(this, GetBusContext(bus), timestamp, handle, bus, target, CMS, messageId, (BYTE)(messageId >> 24), (BYTE)((messageId >> 16) & 0xFF), (unsigned short)((messageId >> 4) & 0xFFF), (BYTE)(messageId & 0xF), payload, payloadLength);
                    break;
                case mostControlRetry:
                    m_pAbiListener->OnMostTxNAKMessage(this, GetBusContext(bus), timestamp, handle, bus, target, CMS, messageId, (BYTE)(messageId >> 24), (BYTE)((messageId >> 16) & 0xFF), (unsigned short)((messageId >> 4) & 0xFFF), (BYTE)(messageId & 0xF), payload, payloadLength);
                    break;
                default:
                    m_pAbiListener->OnMostTxFailedMessage(this, GetBusContext(bus), timestamp, (MostTxStatus)status, handle, bus, target, CMS, messageId, (BYTE)(messageId >> 24), (BYTE)((messageId >> 16) & 0xFF), (unsigned short)((messageId >> 4) & 0xFFF), (BYTE)(messageId & 0xF), payload, payloadLength);
                    break;
                }
                break;

            case 0x10: //High
                status += 0x0200;
                switch(status)
                {
                case mostAsyncSuccess:
                    m_pAbiListener->OnMostTxMessage(this, GetBusContext(bus), timestamp, handle, bus, target, HIGH, messageId, (BYTE)(messageId >> 24), (BYTE)((messageId >> 16) & 0xFF), (unsigned short)((messageId >> 4) & 0xFFF), (BYTE)(messageId & 0xF), payload, payloadLength);
                    break;
                case mostAsyncErrorNac:
                    m_pAbiListener->OnMostTxNAKMessage(this, GetBusContext(bus), timestamp, handle, bus, target, HIGH, messageId, (BYTE)(messageId >> 24), (BYTE)((messageId >> 16) & 0xFF), (unsigned short)((messageId >> 4) & 0xFFF), (BYTE)(messageId & 0xF), payload, payloadLength);
                    break;
                default:
                    m_pAbiListener->OnMostTxFailedMessage(this, GetBusContext(bus), timestamp, (MostTxStatus)status, handle, bus, target, HIGH, messageId, (BYTE)(messageId >> 24), (BYTE)((messageId >> 16) & 0xFF), (unsigned short)((messageId >> 4) & 0xFFF), (BYTE)(messageId & 0xF), payload, payloadLength);
                    break;
                }
                break;

            case 0x70: //ADS
                status += 0x0200;
                switch(status)
                {
                case mostAsyncSuccess:
                    m_pAbiListener->OnMostTxMessage(this, GetBusContext(bus), timestamp, handle, bus, target, ADS, messageId, (BYTE)(messageId >> 24), (BYTE)((messageId >> 16) & 0xFF), (unsigned short)((messageId >> 4) & 0xFFF), (BYTE)(messageId & 0xF), payload, payloadLength);
                    break;
                case mostAsyncErrorNac:
                    m_pAbiListener->OnMostTxNAKMessage(this, GetBusContext(bus), timestamp, handle, bus, target, ADS, messageId, (BYTE)(messageId >> 24), (BYTE)((messageId >> 16) & 0xFF), (unsigned short)((messageId >> 4) & 0xFFF), (BYTE)(messageId & 0xF), payload, payloadLength);
                    break;
                default:
                    m_pAbiListener->OnMostTxFailedMessage(this, GetBusContext(bus), timestamp, (MostTxStatus)status, handle, bus, target, ADS, messageId, (BYTE)(messageId >> 24), (BYTE)((messageId >> 16) & 0xFF), (unsigned short)((messageId >> 4) & 0xFFF), (BYTE)(messageId & 0xF), payload, payloadLength);
                    break;
                }
                break;

            default:
                break;
            }
        }
        break;
    case 0x03: //Rx
        {
            ReceiveType rcvType = (ReceiveType) ReadByte(pBuffer);
            BYTE param2 = ReadByte(pBuffer);
            unsigned short source = ReadUInt16(pBuffer);
            unsigned int messageId = (unsigned int)ReadUInt32(pBuffer);

            if (!m_filter.Match(bus, messageId))
            {
                return;
            }

            unsigned short payloadLength = ReadUInt16(pBuffer);
            BYTE* payload = pBuffer;

            MostRxStatus status = MhpSuccess;

            switch(protocol)
            {
            case 0x00: //AMS
                m_pAbiListener->OnMostRxMessage(this, GetBusContext(bus), timestamp, bus, rcvType, source, AMS, messageId, (BYTE)(messageId >> 24), (BYTE)((messageId >> 16) & 0xFF), (unsigned short)((messageId >> 4) & 0xFFF), (BYTE)(messageId & 0xF), payload, payloadLength);
                break;

            case 0x60: //CMS
                m_pAbiListener->OnMostRxMessage(this, GetBusContext(bus), timestamp, bus, rcvType, source, CMS, messageId, (BYTE)(messageId >> 24), (BYTE)((messageId >> 16) & 0xFF), (unsigned short)((messageId >> 4) & 0xFFF), (BYTE)(messageId & 0xF), payload, payloadLength);
                break;

            case 0x10: //High
                status = (MostRxStatus) (param2 + 0x0200);
                switch((MostRxStatus) status)
                {
                case MhpSuccess:
                    m_pAbiListener->OnMostRxMessage(this, GetBusContext(bus), timestamp, bus, rcvType, source, HIGH, messageId, (BYTE)(messageId >> 24), (BYTE)((messageId >> 16) & 0xFF), (unsigned short)((messageId >> 4) & 0xFFF), (BYTE)(messageId & 0xF), payload, payloadLength);
                    break;
                default:
                    m_pAbiListener->OnMostRxFailedMessage(this, GetBusContext(bus), timestamp, bus, (MostRxStatus) status, rcvType, source, HIGH, messageId, (BYTE)(messageId >> 24), (BYTE)((messageId >> 16) & 0xFF), (unsigned short)((messageId >> 4) & 0xFFF), (BYTE)(messageId & 0xF), payload, payloadLength);
                    break;
                }
                break;

            case 0x70: //ADS
                m_pAbiListener->OnMostRxMessage(this, GetBusContext(bus), timestamp, bus, rcvType, source, ADS, messageId, (BYTE)(messageId >> 24), (BYTE)((messageId >> 16) & 0xFF), (unsigned short)((messageId >> 4) & 0xFFF), (BYTE)(messageId & 0xF), payload, payloadLength);
                break;

            default:
                break;
            }
        }
        break;
    case 0x04: //Bus Changed Event
        {
            bool isStarted = (ReadByte(pBuffer) > 0 ? true : false);
            m_pAbiListener->OnBusRunningStateChanged(this, GetBusContext(bus), timestamp, bus, isStarted);
        }
        break;
    case 0x05: //Tx Filter Payload Event
        {
            bool isEnabled = (ReadByte(pBuffer) > 0 ? true : false);
            m_pAbiListener->OnTxPayloadFilterChanged(this, GetBusContext(bus), timestamp, bus, isEnabled);
        }
        break;
    case 0x0A: //Rx MOST High extended (>64kb)
        {
            ReceiveType rcvType = (ReceiveType) ReadByte(pBuffer);
            BYTE stat = ReadByte(pBuffer);
            unsigned short source = ReadUInt16(pBuffer);
            unsigned int messageId = (unsigned int)ReadUInt32(pBuffer);

            if (!m_filter.Match(bus, messageId))
            {
                return;
            }

            BYTE segmentationId = ReadByte(pBuffer);
            unsigned short blockCounter = ReadUInt16(pBuffer);
            /*BYTE dummy = */ReadByte(pBuffer);

            unsigned short payloadLength = ReadUInt16(pBuffer);
            BYTE* payload = pBuffer;

            MostRxStatus status = (MostRxStatus) (stat + 0x0200);

            m_pAbiListener->OnMostHighExtRxMessage(this, GetBusContext(bus), timestamp, bus, status, rcvType, source, HIGH, messageId, (BYTE)(messageId >> 24), (BYTE)((messageId >> 16) & 0xFF), (unsigned short)((messageId >> 4) & 0xFFF), (BYTE)(messageId & 0xF), segmentationId, blockCounter, payload, payloadLength);
        }
        break;
    case 0x0B: //Allocation event
        {
            unsigned int connectionLabel = (unsigned int)ReadUInt32(pBuffer);
            AudioConnectionType type = (AudioConnectionType) ReadByte(pBuffer);
            AudioBandwidth bandwith = (AudioBandwidth) ReadByte(pBuffer);
            bool isOpenEvent = (ReadByte(pBuffer) > 0 ? true : false);
            AudioConnectorType connector = (AudioConnectorType) ReadByte(pBuffer);
            m_pAbiListener->OnMostAudioStreamingEvent(this, GetBusContext(bus), timestamp, connectionLabel, type, bandwith, isOpenEvent, connector);
        }
        break;
    case 0x0C: //Allocation error event
        {
            unsigned short errorCode = ReadUInt16(pBuffer);
            AudioConnectionType type = (AudioConnectionType) ReadByte(pBuffer);
            AudioBandwidth bandwith = (AudioBandwidth) ReadByte(pBuffer);
            bool isOpenEvent = (ReadByte(pBuffer) > 0 ? true : false);
            AudioConnectorType connector = (AudioConnectorType) ReadByte(pBuffer);
            m_pAbiListener->OnMostAudioStreamingError(this, GetBusContext(bus), timestamp, errorCode, type, bandwith, isOpenEvent, connector);
        }
        break;
    case 0x0D: //Allocation table event
        {
            unsigned short length = ReadUInt16(pBuffer);
            AllocationTableEntry* allocationTable = NULL;
            if(length > 0)
            {
                allocationTable = (AllocationTableEntry*) pBuffer;
            }
            m_pAbiListener->OnMostAllocationTableChanged(this, GetBusContext(bus), timestamp, length, allocationTable);
        }
        break;
    case 0x0E: //MHP status
        {
            bool isTx = (ReadByte(pBuffer) > 0 ? true : false);
            unsigned short status = ReadByte(pBuffer);
            unsigned int messageId = (unsigned int)ReadUInt32(pBuffer);
            unsigned short address = ReadUInt16(pBuffer);
            status = (unsigned short)(status + 0x200); //Add 0x200 for signaling Async Event type
            if(isTx)
                m_pAbiListener->OnMostHighTxStatusEvent(this, GetBusContext(bus), timestamp, MOST_ASYNC1_HIGH, (MostTxStatus)status, messageId, (BYTE)(messageId >> 24), (BYTE)((messageId >> 16) & 0xFF), (unsigned short)((messageId >> 4) & 0xFFF), (BYTE)(messageId & 0xF), address);
            else
                m_pAbiListener->OnMostHighRxStatusEvent(this, GetBusContext(bus), timestamp, MOST_ASYNC1_HIGH, (MostRxStatus)status, messageId, (BYTE)(messageId >> 24), (BYTE)((messageId >> 16) & 0xFF), (unsigned short)((messageId >> 4) & 0xFFF), (BYTE)(messageId & 0xF), address);
        }
        break;
    case 0x0F: //StressNIC state changed                        
        {                     
            unsigned short deviceId = ReadUInt16(pBuffer);
            BYTE fblockId = ReadByte(pBuffer);
            BYTE instanceId = ReadByte(pBuffer);
            unsigned short functionId = ReadUInt16(pBuffer);
            BYTE opType = ReadByte(pBuffer);
            /*BYTE reserved = */ReadByte(pBuffer);
            unsigned short payloadLength = ReadUInt16(pBuffer);
            BYTE* payload = pBuffer;
            m_pAbiListener->OnStressNicMessageReceived(this, GetBusContext(bus), timestamp, bus, deviceId, fblockId, instanceId, functionId, opType, payload, payloadLength);
        }
        break;
    default:
        break;
    }
}

//-----------------------------------------------------------------------------

inline void CABI::DecodeLinChannels(BYTE* pPacket, int nLen, Bus bus, BYTE protocol, BYTE eventType)
{
    BYTE* pBuffer = pPacket;
    unsigned long long timestamp = ReadUInt64(pBuffer);

    switch(eventType)
    {
    case 0x01: //Event
        {
            TBusEvent evt = (TBusEvent) ReadByte(pBuffer);
            unsigned int param = (unsigned int)ReadUInt32(pBuffer);
            //ignored: ReadUInt32(pBuffer);

            switch(evt)
            {
            case beLINBusError:
                {
                    TLinError linError = (TLinError) (param & 0xFFu);
                    m_pAbiListener->OnLinBusError(this, GetBusContext(bus), timestamp, bus, evt, linError);
                }
                break;
            case beLINTPBusError:
                {
                    TLinTpError lintpError = (TLinTpError) (param & 0xFFu);
                    m_pAbiListener->OnLinTpBusError(this, GetBusContext(bus), timestamp, bus, evt, lintpError);
                }
                break;       
            default:
                break;
            }     
        }
        break;
    case 0x02: //Tx
        {
            BYTE status = ReadByte(pBuffer);
            /*BYTE param2 = */ReadByte(pBuffer);
            unsigned short handle = ReadUInt16(pBuffer);
            unsigned short target = ReadUInt16(pBuffer);
            unsigned int messageId = (unsigned int)ReadUInt32(pBuffer);

            if (!m_filter.Match(bus, messageId))
            {
                return;
            }

            unsigned short payloadLength = ReadUInt16(pBuffer);
            BYTE* payload = pBuffer;

            switch(bus)
            {
            case LIN1:
            case LIN2:
            case LIN3:
            case LIN4:
            case LIN5:
            case LIN6:
                switch(status)
                {
                case linSuccess:
                    m_pAbiListener->OnLinTxMessage(this, GetBusContext(bus), timestamp, handle, bus, target, messageId, payload, payloadLength);
                    break;
                default:
                    m_pAbiListener->OnLinTxFailedMessage(this, GetBusContext(bus), timestamp, handle, bus, target, messageId, payload, payloadLength);
                    break;
                }
                break;
            case LINTP1:
            case LINTP2:
            case LINTP3:
            case LINTP4:
            case LINTP5:
            case LINTP6:
                switch(status)
                {
                case linSuccess:
                    m_pAbiListener->OnLinTpTxMessage(this, GetBusContext(bus), timestamp, handle, bus, messageId, (BYTE)(messageId & 0xFF), (BYTE)((messageId >> 8) & 0xFF), (BYTE)((messageId >> 16) & 0xFF), payload, payloadLength);
                    break;
                default:
                    m_pAbiListener->OnLinTpTxFailedMessage(this, GetBusContext(bus), timestamp, handle, bus, messageId, (BYTE)(messageId & 0xFF), (BYTE)((messageId >> 8) & 0xFF), (BYTE)((messageId >> 16) & 0xFF), payload, payloadLength);
                    break;
                }
                break;
            default:
                break;
            }
        }
        break;
    case 0x03: //Rx
        {
            /*BYTE param1 = */ReadByte(pBuffer);
            /*BYTE param2 = */ReadByte(pBuffer);
            unsigned short source = ReadUInt16(pBuffer);
            unsigned int messageId = (unsigned int)ReadUInt32(pBuffer);
            if (!m_filter.Match(bus, messageId))
            {
                return;
            }

            unsigned short payloadLength = ReadUInt16(pBuffer);
            BYTE* payload = pBuffer;

            switch(bus)
            {
            case LIN1:
            case LIN2:
            case LIN3:
            case LIN4:
            case LIN5:
            case LIN6:
                m_pAbiListener->OnLinRxMessage(this, GetBusContext(bus), timestamp, bus, source, messageId, payload, payloadLength);
                break;
            case LINTP1:
            case LINTP2:
            case LINTP3:
            case LINTP4:
            case LINTP5:
            case LINTP6:
                m_pAbiListener->OnLinTpRxMessage(this, GetBusContext(bus), timestamp, bus, messageId, (BYTE)(messageId & 0xFF), (BYTE)((messageId >> 8) & 0xFF), (BYTE)((messageId >> 16) & 0xFF), payload, payloadLength);
                break;
            default:
                break;
            }
        }
        break;
    case 0x04: //Bus Changed Event
        {
            bool isStarted = (ReadByte(pBuffer) > 0 ? true : false);
            m_pAbiListener->OnBusRunningStateChanged(this, GetBusContext(bus), timestamp, bus, isStarted);
        }
        break;
    case 0x05: //Tx Filter Payload Event
        {
            bool isEnabled = (ReadByte(pBuffer) > 0 ? true : false);
            m_pAbiListener->OnTxPayloadFilterChanged(this, GetBusContext(bus), timestamp, bus, isEnabled);
        }
        break;
    case 0x0A: //Spy Event
        {
            TLinSpyEvent spyEvent = (TLinSpyEvent) ReadByte(pBuffer);
            m_pAbiListener->OnLinSpyEvent(this, GetBusContext(bus), timestamp, bus, spyEvent);
        }
        break;
    case 0x0B: //Spy Message
        {
            BYTE payload = ReadByte(pBuffer);
            m_pAbiListener->OnLinSpyMessage(this, GetBusContext(bus), timestamp, bus, payload);
        }
        break;
    default:
        break;
    }
}

//-----------------------------------------------------------------------------

inline void CABI::DecodeFlexRayChannels(BYTE* pPacket, int nLen, Bus bus, BYTE protocol, BYTE eventType)
{
    BYTE* pBuffer = pPacket;
    unsigned long long timestamp = ReadUInt64(pBuffer);

    switch(eventType)
    {
    case 0x01: //Event
        {
            TBusEvent evt = (TBusEvent)ReadByte(pBuffer);
            //ignored: ReadUInt32(pBuffer);
            //ignored: ReadUInt32(pBuffer);

            m_pAbiListener->OnFlexrayEventMessage(this, GetBusContext(bus), timestamp, bus, evt);
        }
        break;
    case 0x02: //Tx
        {
            BYTE status = ReadByte(pBuffer);
            /*BYTE param2 = */ReadByte(pBuffer);
            unsigned short handle = ReadUInt16(pBuffer);
            unsigned short target = ReadUInt16(pBuffer);
            unsigned int messageId = (unsigned int)ReadUInt32(pBuffer);

            if (!m_filter.Match(bus, messageId))
            {
                return;
            }

            unsigned short payloadLength = ReadUInt16(pBuffer);
            BYTE* payload = pBuffer;

            switch(status)
            {
            case flexraySuccess:
                m_pAbiListener->OnFlexrayTxMessage(this, GetBusContext(bus), timestamp, handle, bus, messageId, (unsigned short)(messageId & 0x7FF), (BYTE)((messageId >> 16) & 0x3F), (BYTE)(((messageId >> 24) & 0x3F) + 1), (FrChannel)((messageId >> 12) & 0x3), target, payload, payloadLength);
                break;
            default:
                m_pAbiListener->OnFlexrayTxFailedMessage(this, GetBusContext(bus), timestamp, handle, bus, messageId, (unsigned short)(messageId & 0x7FF), (BYTE)((messageId >> 16) & 0x3F), (BYTE)(((messageId >> 24) & 0x3F) + 1), (FrChannel)((messageId >> 12) & 0x3), target, payload, payloadLength);
                break;
            }
        }
        break;
    case 0x03: //Rx
        {
            /*BYTE param1 = */ReadByte(pBuffer);
            /*BYTE param2 = */ReadByte(pBuffer);
            unsigned short source = ReadUInt16(pBuffer);
            unsigned int messageId = (unsigned int)ReadUInt32(pBuffer);

            if (!m_filter.Match(bus, messageId))
            {
                return;
            }

            unsigned short payloadLength = ReadUInt16(pBuffer);
            BYTE* payload = pBuffer;

            m_pAbiListener->OnFlexrayRxMessage(this, GetBusContext(bus), timestamp, bus, messageId, (unsigned short)(messageId & 0x7FF), (BYTE)((messageId >> 16) & 0x3F), (BYTE)(((messageId >> 24) & 0x3F) + 1), (FrChannel)((messageId >> 12) & 0x3), source, payload, payloadLength);
        }
        break;
    case 0x04: //Bus Changed Event
        {
            bool isStarted = (ReadByte(pBuffer) > 0 ? true : false);
            m_pAbiListener->OnBusRunningStateChanged(this, GetBusContext(bus), timestamp, bus, isStarted);
        }
    case 0x05: //Tx Filter Payload Event
        {
            bool isEnabled = (ReadByte(pBuffer) > 0 ? true : false);
            m_pAbiListener->OnTxPayloadFilterChanged(this, GetBusContext(bus), timestamp, bus, isEnabled);
        }
        break;
        break;
    default:
        break;
    }
}

//-----------------------------------------------------------------------------

inline void CABI::DecodeRelayChannels(BYTE* pPacket, int nLen, Bus bus, BYTE protocol, BYTE eventType)
{
    BYTE* pBuffer = pPacket;
    unsigned long long timestamp = ReadUInt64(pBuffer);

    switch (eventType)
    {
        case 0x04: //Bus Changed Event
            {
                bool isClosed = ReadByte(pBuffer) > 0;
                m_pAbiListener->OnRelayStateChanged(this, GetBusContext(bus), timestamp, bus, isClosed);
            }
            break;
        case 0x0A: //Trigger level changed
            {
                bool isActive = ReadByte(pBuffer) > 0;
                bool isTx = ReadByte(pBuffer) > 0;
                m_pAbiListener->OnTriggerLevelChanged(this, GetBusContext(bus), timestamp, bus, isActive, isTx);
            }
            break;            
        //case 0x0B: //Timer Elapsed
        //    {
        //        unsigned short handle = ReadUInt16(pBuffer);
        //        m_pAbiListener->OnTimerElapsed(this, GetBusContext(bus), timestamp, bus, handle);
        //    }
        //    break;
        default:
            break;
    }
}

//-----------------------------------------------------------------------------

inline void CABI::DecodeMostEthernetChannels(BYTE* pPacket, int nLen, Bus bus, BYTE protocol, BYTE eventType)
{
    BYTE* pBuffer = pPacket;
    unsigned long long timestamp = ReadUInt64(pBuffer);
    
    switch (eventType)
    {
    case 0x02: //Tx
        {
            unsigned short handle = ReadUInt16(pBuffer);
            unsigned long long target = ReadUInt64(pBuffer);
            //if (!Filter.MatchAddress(bus, target, ABIFilter.FILTER_TX))
            //{
            //    return;
            //}
            unsigned short payloadLength = ReadUInt16(pBuffer);
            BYTE* payload = pBuffer;
            m_pAbiListener->OnMostMepTxMessage(this, GetBusContext(bus), timestamp, handle, bus, target, payload, payloadLength);
        }
        break;
    case 0x03: //Rx
        {
            unsigned long long source = ReadUInt64(pBuffer);
            //if (!Filter.MatchAddress(bus, source, ABIFilter.FILTER_RX))
            //{
            //    return;
            //}
            unsigned short payloadLength = ReadUInt16(pBuffer);
            BYTE* payload = pBuffer;
            m_pAbiListener->OnMostMepRxMessage(this, GetBusContext(bus), timestamp, bus, source, payload, payloadLength);
        }
        break;
    case 0x04: //Bus Changed Event
        {
            bool isStarted = ReadByte(pBuffer) > 0;
            MepBridgeOptions options = (MepBridgeOptions)ReadByte(pBuffer);
            m_pAbiListener->OnMostMepBridgeStatusChanged(this, GetBusContext(bus), timestamp, bus, isStarted, options);
        }
        break;
    case 0x05: //Tx Filter Payload Event
        {
            bool isEnabled = ReadByte(pBuffer) > 0;
            m_pAbiListener->OnTxPayloadFilterChanged(this, GetBusContext(bus), timestamp, bus, isEnabled);
        }
        break;
    case 0x0A: //MAC address changed Event
        {
            unsigned long long macAddress = ReadUInt64(pBuffer);
            m_pAbiListener->OnMostMacAddressChanged(this, GetBusContext(bus), timestamp, bus, macAddress);
        }
        break;
    default:
        break;
    }
}

//-----------------------------------------------------------------------------

BYTE CABI::ReadByte(BYTE*& pBuffer)
{
    BYTE ret = pBuffer[0];
    pBuffer++;
    return ret;
}

//-----------------------------------------------------------------------------

unsigned short CABI::ReadUInt16(BYTE*& pBuffer)
{
    unsigned short ret = *(uint16_t*)pBuffer;
    pBuffer += sizeof(uint16_t);
    return ret;
}

//-----------------------------------------------------------------------------

unsigned long CABI::ReadUInt32(BYTE*& pBuffer)
{
    unsigned long ret = *(uint32_t*)pBuffer;
    pBuffer += sizeof(uint32_t);
    return ret;
}

//-----------------------------------------------------------------------------

unsigned long long CABI::ReadUInt64(BYTE*& pBuffer)
{
    unsigned long long ret = *(unsigned long long*)pBuffer;
    pBuffer += sizeof(unsigned long long);
    return ret;
}

//-----------------------------------------------------------------------------

void CABI::AddIdIntervalFilter(Bus bus, unsigned short handle, unsigned int minId, unsigned int maxId)
{
    m_filter.AddIntervalFilter(bus, handle, minId, maxId);
}

//-----------------------------------------------------------------------------

void CABI::AddMaskedIdFilter(Bus bus, unsigned short handle, unsigned int id, unsigned int mask)
{
    m_filter.AddMaskedFilter(bus, handle, id, mask);
}

//-----------------------------------------------------------------------------

void CABI::RemoveFilter(unsigned short handle)
{
    m_filter.RemoveFilter(handle);
}

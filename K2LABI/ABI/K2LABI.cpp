#include "k2l-type.h"
#include "K2LABI.h"
#include "ABI.h"

//static 
IABI* CABIFactory::CreateAbi()
{
    return new CABI();
}

//static 
void CABIFactory::DestroyAbi(IABI* pAbi)
{
    delete pAbi;
}

K2LABI_API IABI* CreateABI  (void)
{
    return new CABI();
}

K2LABI_API void  DestroyABI (IABI* pAbi)
{
    delete pAbi;
}
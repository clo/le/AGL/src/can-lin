/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// K2L GmbH
///
/// \file
///          This file contains the api of all abi commands.
///
/// \ingroup ABICommands
///
/// \par     COPYRIGHT (c) 2000-2011 by K2L GmbH
///          All rights reserved.
///
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///          Alle Rechte an der Software verbleiben bei der K2L GmbH.
///
///          Die K2L GmbH raeumt dem Lizenznehmer nach vollstaendiger
///          Bezahlung der Lizenzgebuehr ein nicht ausschliessliches,
///          unwiderrufliches, nicht uebertragbares, zeitlich und geografisch
///          nicht beschraenktes Recht zur Nutzung der jeweils
///          auftragsgegenstaendlichen Software innerhalb des Lizenznehmers ein.
///
///          Die Weitergabe des Nutzungsrechts durch den Lizenznehmer ist auf
///          dessen Zulieferer beschraenkt. Die Zulieferer sind zu verpflichten,
///          die Software nur im Rahmen von Projekten fuer den Lizenznehmer
///          zu verwenden; weitere Rechte der Zulieferer sind auszuschliessen.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//@{

#pragma once

#include "K2LABI.h"
#include <string>

using namespace std;
    
namespace K2L
{
    namespace Automotive
    {
        namespace ABI
        {

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief This class contains some commands for controlling the device.
/// \note Use CABICommands instead, so you have access to all commands include of CABICommandsEx.
/// \ingroup ABICommands
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class K2LABI_API CABICommandsEx
{

public:
    
    /// Adds receiving rule for the CAN TP bus into the current group.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] handle Handle of the receiving rule.
    /// \param[in] bus Receiving bus.
    /// \param[in] messageIdSrc Source message id.
    /// \param[in] messageIdDst Destination message id.
    /// \param[in] addressingType TP addressing type.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup OBJ
    static int AddCanTpReceiver(IABI* pAbi, unsigned short handle, Bus bus, unsigned int messageIdSrc, unsigned int messageIdDst, TPAddressingType addressingType);
    
    /// Adds CAN TP transmitting rule to the list of the rules from the current group.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] handle Handle of transmitting rule.
    /// \param[in] bus Transmitting bus.
    /// \param[in] srcMessageId Source message id.
    /// \param[in] dstMessageId Destination message id.
    /// \param[in] addressingType TP addressing type.
    /// \param[in] cycleTime Cycle time in milliseconds for cyclic message sending.
    /// \param[in] totalNumber Number of time the message is sent, 0 means endless transmission.
    /// \param[in] payloadLength Length of payload.
    /// \param[in] payload Payload data.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup OBJ
    static int AddCanTpTransmitter(IABI* pAbi, unsigned short handle, Bus bus, unsigned int srcMessageId, unsigned int dstMessageId, TPAddressingType addressingType, unsigned short cycleTime, unsigned int totalNumber, unsigned short payloadLength, BYTE* payload);
    
    /// Adds receiving rule into the current group.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] handle Handle of the receiving rule.
    /// \param[in] bus Receiving bus.
    /// \param[in] param1 Parameter 1.
    /// \param[in] param2 Parameter 2.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note MOST: param1: mask (0 means all ids; 0xFFFFFFFF means this id). param2: message identifier value.
    /// \note FlexRay: param1: mask (0 means all ids; 0xFFFFFFFF means this id). param2: message identifier value.
    /// \note CAN: param1: minimal message identifier. param2: maximal identifier.
    /// \note LIN: param1: message identifier (0xFF means all ids). param2: Not used.
    /// \note LINTP: param1: Node address (0xFF means all NADs). param2: Not used.
    /// \note MEP: param1: High part of MAC address. param2: Low part of MAC address. If param1 is 0xFFFFFFF and param2 are 0xFFFF then all messages will be let through.
    /// \ingroup OBJ
    static int AddReceiver(IABI* pAbi, unsigned short handle, Bus bus, unsigned int param1, unsigned int param2);
    
    /// Adds transmitting rule to the list of the rules from the current group.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] handle Handle of transmitting rule.
    /// \param[in] bus Transmitting bus.
    /// \param[in] targetAddress Target address.
    /// \param[in] messageId Message identifier.
    /// \param[in] delay Time in milliseconds between starting of transmitter and sending of the first message.
    /// \param[in] cycle Cycle time in milliseconds for cyclic message sending.
    /// \param[in] totalNumber Number of time the message is sent, 0 means endless transmission.
    /// \param[in] payloadLength Length of payload.
    /// \param[in] payload Payload data.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup OBJ
    static int AddTransmitterEx(IABI* pAbi, unsigned short handle, Bus bus, unsigned short targetAddress, unsigned int messageId, unsigned int delay, unsigned short cycle, unsigned int totalNumber, unsigned short payloadLength, BYTE* payload);
    
    /// Adds sweep signal for the transmitting rule.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] handle Handle of the transceiver you want to add signal to.
    /// \param[in] signalType Signal type. There are several types of a signals.
    /// \param[in] firstBitPos First bit position of the sweep signal.
    /// \param[in] lastBitPos Last bit position of the sweep signal.
    /// \param[in] stepsNum Number of steps between minimal and maximal value of the signal sequence. This parameter is ignored for the Bit Shift signal type.
    /// \param[in] step Step between two signals. Negative value descends signal value. This parameter is ignored for the Bit Shift signal type.
    /// \param[in] initialValue Initial value of the signal. This parameter is ignored for the Bit Shift signal type.
    /// \param[in] repetitionsNumber Number of messages sent before this signal is incremented according to signal type.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup OBJ
    static int AddTxSweepSignal(IABI* pAbi, unsigned short handle, SignalType signalType, unsigned short firstBitPos, unsigned short lastBitPos, unsigned short stepsNum, int step, unsigned int initialValue, unsigned int repetitionsNumber);
    
    /// Changes cycle time of the given transmitting rule.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] handle Handle of transmitting rule.
    /// \param[in] cycle Cycle time in milliseconds for cyclic message sending.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup OBJ
    static int ChangeCycleTime(IABI* pAbi, unsigned short handle, unsigned short cycle);
    
    /// Changes data of existing transmitter.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] handle Handle of the transmitting rule.
    /// \param[in] payloadLength Length of new payload.
    /// \param[in] payload New payload data.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup OBJ
    static int ChangeTransmitterData(IABI* pAbi, unsigned short handle, unsigned short payloadLength, BYTE* payload);
    
    /// Clears CAN TP pattern.
    /// \param[in] pAbi Used IABI instance.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup CANTP
    static int ClearCanTpPattern(IABI* pAbi);
    
    /// Deletes one or more transmitting or receiving rules.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] handle Handle of object you want to delete.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note If handle is zero all transmitting and receiving rules from the active group are deleted.
    /// \ingroup OBJ
    static int Delete(IABI* pAbi, unsigned short handle);
    
    /// Deletes all sweep signals from an transmitting rule.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] handle Handle of transmitting rule where the sweep signals should be deleted.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup OBJ
    static int DeleteTxSweepSignals(IABI* pAbi, unsigned short handle);
    
    /// Enables FlexRay tx messages.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] enabled TRUE if FlexRay tx messages should be enabled, otherwise FALSE.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup FR
    static int EnableFlexRayTx(IABI* pAbi, bool enabled);
    
    /// Enables/Disables MEP<->Ethernet Bridge.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] isEnabled TRUE if bridge should be activated otherwise FALSE.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note Only available for MOST 150.
    /// \ingroup MOST
    static int EnableMepEthernetBridge(IABI* pAbi, bool isEnabled);
    
    /// Enables/Disables MEP<->Ethernet Bridge.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] isEnabled TRUE if bridge should be activated otherwise FALSE.
    /// \param[in] options Options to activate.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note Only available for MOST 150.
    /// \ingroup MOST
    static int EnableMepEthernetBridgeExtended(IABI* pAbi, bool isEnabled, MepBridgeOptions options);
    
    /// Set the channels of MOST Spy that are activated.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] status Status channel.
    /// \param[in] control Control channel.
    /// \param[in] async Asynchronous channel.
    /// \param[in] sync Synchronous channel.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int EnableMostSpy(IABI* pAbi, bool status, bool control, bool async, bool sync);
    
    /// Gets the current value of the MOST boundary.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result Boundary value (SBC).
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int GetBoundary(IABI* pAbi, BYTE& result);
    
    /// Gets base frqeuency of CAN cell.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bus Selected channel.
    /// \param[out] result Returns base frquency of CAN cell.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup CAN
    static int GetCanBaseFrequency(IABI* pAbi, Bus bus, unsigned int& result);
    
    /// Retrieves the count of available CAN buses.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result Count of available CAN buses.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup CAN
    static int GetCanBusCount(IABI* pAbi, unsigned int& result);
    
    /// Gets the actual selected can speed for the given CAN bus.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bus Selected CAN channel.
    /// \param[out] result Selected baudrate.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup CAN
    static int GetCanSpeed(IABI* pAbi, Bus bus, CanSpeed& result);
    
    /// Gets the actual selected transceiver type for the given CAN bus.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bus Selected CAN channel.
    /// \param[out] result Selected transcevier type.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup CAN
    static int GetCanTransceiver(IABI* pAbi, Bus bus, CanTransceiver& result);
    
    /// Gets the MOST mode of the device.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result Actual MOST mode, determinates the mode of the INIC.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int GetDeviceMode(IABI* pAbi, MostMode& result);
    
    /// Gets the firmware version of device.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result Firmware version of device.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup HW
    static int GetFirmwareVersion(IABI* pAbi, char*& result);
    
    /// Retrieves the count of available FlexRay buses.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result Count of available FlexRay buses.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup FR
    static int GetFlexRayBusCount(IABI* pAbi, unsigned int& result);
    
    /// Gets the MOST group address of device.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result Group address of device.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int GetGroupAddr(IABI* pAbi, BYTE& result);
    
    /// Gets the Standard gateway IP address of device.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result Standard gateway address.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup HW
    static int GetGwAddress(IABI* pAbi, char*& result);
    
    /// Gets the hardware version of device.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result Hardware version of device.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup HW
    static int GetHardwareVersion(IABI* pAbi, char*& result);
    
    /// Retrieves various information from the Automotive Bus Interface.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] info Identifier of requested information.
    /// \param[out] result Requested information.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup HW
    static int GetInformation(IABI* pAbi, BoardInformation info, char*& result);
    
    /// Retrieves internal amount of error events occurred in the the ABI firmware. Calling this method, will automatically clear the error counter to zero.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] parameter Module to retrieve state of.
    /// \param[out] result Value of the selected parameter.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup OBJ
    static int GetInternalState(IABI* pAbi, Parameter parameter, unsigned int& result);
    
    /// Gets the IP address of device.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result IP address of device.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup HW
    static int GetIpAddress(IABI* pAbi, char*& result);
    
    /// Retrieves the count of available LIN buses.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result Count of available LIN buses.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup LIN
    static int GetLinBusCount(IABI* pAbi, unsigned int& result);
    
    /// Returns name of current used LIN configuration.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bus Selected channel.
    /// \param[out] result Returns name of current used LIN configuration. If no config is used an empty string will be returned.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup LIN
    static int GetLinConfigurationName(IABI* pAbi, Bus bus, char*& result);
    
    /// Retrieves the baud rate configured for the chossen LIN bus.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bus Selected LIN channel.
    /// \param[out] result Baud rate of the LIN channel, represented in bit per second.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup LIN
    static int GetLinSpeed(IABI* pAbi, Bus bus, unsigned int& result);
    
    /// Gets the logical MOST node address of device.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result Logical node address of device.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int GetLogicalNodeAddr(IABI* pAbi, unsigned short& result);
    
    /// Get the count of high level retries.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result Actual count of high level retries.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int GetMostControlChannelHighLevelRetry(IABI* pAbi, BYTE& result);
    
    /// Get the count of low level retries.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result Actual count of low level retries.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int GetMostControlChannelLowLevelRetry(IABI* pAbi, BYTE& result);
    
    /// Get the count of mid level retries.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result Actual count of mid level retries.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int GetMostControlChannelMidLevelRetry(IABI* pAbi, BYTE& result);
    
    /// Get the frequency of MOST.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result Actual MOST frequency.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int GetMostFrequency(IABI* pAbi, MostSamplingFrequency& result);
    
    /// Retrieves the MOST illumination level.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result MOST illumination level.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int GetMostIlluminationLevel(IABI* pAbi, MostIlluminationLevel& result);
    
    /// Get MAC address of MOST node.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result MAC address of MOST node.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int GetMostMacAddress(IABI* pAbi, unsigned long long& result);
    
    /// Get the shutdown reason of INIC.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result Shutdown reason of INIC.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note Only available for MOST 150.
    /// \ingroup MOST
    static int GetMostShutdownReason(IABI* pAbi, BYTE& result);
    
    /// Retrieves the supported MOST speed grade.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result Speed grade of MOST. Returns 0 if not available. E.g. 50 means MOST50.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int GetMostSpeedGrade(IABI* pAbi, unsigned int& result);
    
    /// Gets the maximum position range of the MOST network.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result Maximum position range of the network (MPR).
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int GetMPR(IABI* pAbi, BYTE& result);
    
    /// Get the mute state of the selected audio connector.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] connector The selected audio connector type.
    /// \param[in] isInput true if input connector should be modified, otherwise output connector is modified.
    /// \param[out] result true if channel is muted, otherwise false.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int GetMuteState(IABI* pAbi, AudioConnectorType connector, bool isInput, bool& result);
    
    /// Gets the MAC address of device.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result MAC address of device.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup HW
    static int GetNetMask(IABI* pAbi, char*& result);
    
    /// Gets the MOST node position of device.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result Node position of device (NPR).
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int GetNPR(IABI* pAbi, BYTE& result);
    
    /// Retrieves a value by a given key.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] key Selected key.
    /// \param[out] result Value of selected key.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup EXT
    static int GetParameter(IABI* pAbi, char* key, char*& result);
    
    /// Retrieves all available keys as comma separated string.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result All available keys.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup EXT
    static int GetParameterKeys(IABI* pAbi, char*& result);
    
    /// Retrieves all available values as comma separated string by a given key.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] key Selected key.
    /// \param[out] result All available values.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup EXT
    static int GetParameterValues(IABI* pAbi, char* key, char*& result);
    
    /// Gets the running time of the device. The time is represented in steps of 100 nanoseconds.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result Time that passed since starting the device.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup HW
    static int GetRunningTime(IABI* pAbi, unsigned long long& result);
    
    /// Gets the serial number of the device.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result result Serial number of device.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup HW
    static int GetSerialNumber(IABI* pAbi, char*& result);
    
    /// Gets the actual timestamp. The timestamp is represented in steps of 100 nanoseconds.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result Actual timestamp.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup HW
    static int GetTimeStamp(IABI* pAbi, unsigned long long& result);
    
    /// Get the mode of the hardware synchronisation unit.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result Actual mode of hardware synchronisation unit.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup HW
    static int GetTimingSynchronisationMode(IABI* pAbi, TimingSynchronisationMode& result);
    
    /// Gets the voltage level of trigger line.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result TRUE means active (voltage low), FALSE means inactive (voltage high).
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup HW
    static int GetTriggerLevel(IABI* pAbi, bool& result);
    
    /// Determinates if the streaming module provides an allocation table.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result TRUE if supported, FALSE otherwise.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int HasAllocationTableSupport(IABI* pAbi, bool& result);
    
    /// Determinates if the streaming module provides support to stream the input to multiple labels.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result TRUE if supported, FALSE otherwise.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int HasMultipleStreamInSupport(IABI* pAbi, bool& result);
    
    /// Determinates if the streaming module provides support to stream the own input signal to the out connector.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result TRUE if supported, FALSE otherwise.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int HasRedirectStreamInToOutSupport(IABI* pAbi, bool& result);
    
    /// Identifies a device by toggling hectically the active LED for a few seconds.
    /// \param[in] pAbi Used IABI instance.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup HW
    static int Identify(IABI* pAbi);
    
    /// Determinates if the selected bus is started.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bus Selected channel.
    /// \param[out] result Running state, TRUE if the selected bus is started, FALSE otherwise.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup OBJ
    static int IsBusStarted(IABI* pAbi, Bus bus, bool& result);
    
    /// Check if the desired CAN transceiver type is supported by the hardware.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bus CAN channel.
    /// \param[in] transceiver Type that should be checked.
    /// \param[out] result TRUE if supported, FALSE if not.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup CAN
    static int IsCanTransceiverSupported(IABI* pAbi, Bus bus, CanTransceiver transceiver, bool& result);
    
    /// Determinates if the choosen FlexRay bus is in a synchronized state.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bus Selected FlexRay channel.
    /// \param[out] result Synchron State, TRUE if synchronized. False if not the bus is not synchronized.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup FR
    static int IsFlexRaySynchronized(IABI* pAbi, Bus bus, bool& result);
    
    /// Retrieves if FlexRay tx messages are enabled.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result TRUE if FlexRay tx messages are enabled, otherwise FALSE.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup FR
    static int IsFlexRayTxEnabled(IABI* pAbi, bool& result);
    
    /// Determinates if the choosen LIN bus is configured to act as Master.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bus Selected LIN channel.
    /// \param[out] result Master state, TRUE if acting as Master, FALSE if acting as Slave.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup LIN
    static int IsLinMaster(IABI* pAbi, Bus bus, bool& result);
    
    /// Checks if the MEP<->Ethernet Bridge is activated.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result TRUE if bridge is activated otherwise FALSE.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note Only available for MOST 150.
    /// \ingroup MOST
    static int IsMepEthernetBridgeEnabled(IABI* pAbi, bool& result);
    
    /// Retrieves if MOST node (INIC) is supported.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result TRUE means device supports a MOST node and FALSE means not.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int IsMostNodeSupported(IABI* pAbi, bool& result);
    
    /// Retrieves if MOST spy (SpyNIC) is supported.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result TRUE means device supports a MOST spy and FALSE means not.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int IsMostSpySupported(IABI* pAbi, bool& result);
    
    /// Retrieves if MOST stress (StressNIC) is supported.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result TRUE means device supports a MOST stress and FALSE means not.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int IsMostStressSupported(IABI* pAbi, bool& result);
    
    /// Determinates if the MOST bus is in state Net On.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result Net On state, TRUE if the Most bus is in state Net On, FALSE otherwise.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int IsNetOn(IABI* pAbi, bool& result);
    
    /// Gets the state of the relay.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result State of relay. TRUE means close and FALSE means open.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup HW
    static int IsRelayClosed(IABI* pAbi, bool& result);
    
    /// Determinates if the MOST bus is in state Stable Lock.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result Stable Lock state, TRUE if the Most bus is in state Stable Lock, FALSE otherwise.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int IsStableLock(IABI* pAbi, bool& result);
    
    /// Checks if transmission of payload in tx events is filtered.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bus Bus that is selected for this command.
    /// \param[out] result FALSE if tx events have payload otherwise TRUE.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup OBJ
    static int IsTxPayloadFilterEnabled(IABI* pAbi, Bus bus, bool& result);
    
    /// Allocate a new audio streaming channel.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bandwidth Desired bandwidth of the allocated audio channel.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note This command is asynchronous.
    /// \attention Will be removed in further versions. Use MostAudioTxAllocateConnect or MostAudioTxAllocateConnectSync instead.
    /// \ingroup MOST
    static int MostAudioAllocate(IABI* pAbi, AudioBandwidth bandwidth);
    
    /// Stream an audio stream from the MOST ring to the out connectors of the MOCCA compact.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bandwidth Desired bandwidth of the audio channel.
    /// \param[in] label Label that is used.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note This command is asynchronous.
    /// \ingroup MOST
    static int MostAudioRxConnect(IABI* pAbi, AudioBandwidth bandwidth, unsigned int label);
    
    /// Stream an audio stream from the MOST ring to the out connectors of the MOCCA compact. If the SPDIF connector is already used for another label it is disconnected and connected to the given label.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bandwidth Desired bandwidth of the audio channel.
    /// \param[in] label Label that is used.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note If no label is used before it acts like MostAudioRxConnect.
    /// \note This command is asynchronous.
    /// \ingroup MOST
    static int MostAudioRxConnectExt(IABI* pAbi, AudioBandwidth bandwidth, unsigned int label);
    
    /// Disconnect the audio streaming to the out connectors.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] label Label that is used.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note This command is asynchronous.
    /// \ingroup MOST
    static int MostAudioRxDisconnect(IABI* pAbi, unsigned int label);
    
    /// Allocate a new audio streaming channel and stream the SPDIF in connector of the MOCCA compact to the MOST ring.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bandwidth Desired bandwidth of the allocated audio channel.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note This command is asynchronous.
    /// \ingroup MOST
    static int MostAudioTxAllocateConnect(IABI* pAbi, AudioBandwidth bandwidth);
    
    /// Allocate a new audio streaming channel and stream the selected IN connector of the MOCCA compact to the MOST ring.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bandwidth Desired bandwidth of the allocated audio channel.
    /// \param[in] connector The connector that should be used.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note This command is asynchronous.
    /// \ingroup MOST
    static int MostAudioTxAllocateConnect(IABI* pAbi, AudioBandwidth bandwidth, AudioConnectorType connector);
    
    /// Stream the SPDIF input connector of the MOCCA compact to the MOST ring on a preallocated label.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bandwidth Desired bandwidth of the allocated audio channel.
    /// \param[in] label Label that is used.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note This command is asynchronous.
    /// \attention Will be removed in further versions. Use MostAudioTxAllocateConnect or MostAudioTxAllocateConnectSync instead.
    /// \ingroup MOST
    static int MostAudioTxConnect(IABI* pAbi, AudioBandwidth bandwidth, unsigned int label);
    
    /// Disconnect the audio streaming from SPDIF input from channel.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] label Label that is used.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note This command is asynchronous.
    /// \ingroup MOST
    static int MostAudioTxDisconnect(IABI* pAbi, unsigned int label);
    
    /// Reboots the device.
    /// \param[in] pAbi Used IABI instance.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup HW
    static int RebootFirmware(IABI* pAbi);
    
    /// Reset the shutdown reason of INIC.
    /// \param[in] pAbi Used IABI instance.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note Only available for MOST 150.
    /// \ingroup MOST
    static int ResetMostShutdownReason(IABI* pAbi);
    
    /// Resets timestamp unit of device.
    /// \param[in] pAbi Used IABI instance.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note This command is only working if the timestamp synchronisation is set to standalone. This command will be ignored, if timestamp synchronisation is set to master os slave mode.
    /// \ingroup HW
    static int ResetTimeStamp(IABI* pAbi);
    
    /// Selects active group.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] group Selected group.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup OBJ
    static int SelectGroup(IABI* pAbi, BYTE group);
    
    /// Send a message via CAN Transport Protocol.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] handle Handle of tx message.
    /// \param[in] bus Transmitting bus.
    /// \param[in] source Source address.
    /// \param[in] destination Destination address.
    /// \param[in] type TP addressing type.
    /// \param[in] payloadLength Length of payload.
    /// \param[in] payload Payload data.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note The maximum length of message is 4095 bytes.
    /// \ingroup CANTP
    static int SendCanTpMessage(IABI* pAbi, unsigned short handle, Bus bus, unsigned int source, unsigned int destination, TPAddressingType type, unsigned short payloadLength, BYTE* payload);
    
    /// Send a message immediately to the selected bus.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] handle Handle of tx message.
    /// \param[in] bus Transmitting bus.
    /// \param[in] targetAddress Target address.
    /// \param[in] messageId Message identifier.
    /// \param[in] payloadLength Length of payload.
    /// \param[in] payload Payload data.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note MessageID CAN: CanId
    /// \note MessageID FlexRay: (CycleRepetition - 1) << 24 | CycleOffset << 16  | FlexrayLogicalChannel << 12 | FrameId
    /// \note MessageID LIN: LinId
    /// \note MessageID MOST: FBlockId << 24 | (InstanceId & 0xFF) << 16) | (FunctionId & 0xFFF) << 4 | OpTypeId & 0x0F
    /// \note There are bus specific send commands for every field bus, which takes the needed parameters directly, so no transformation is needed.
    /// \ingroup OBJ
    static int SendMessage(IABI* pAbi, unsigned short handle, Bus bus, unsigned short targetAddress, unsigned int messageId, unsigned short payloadLength, BYTE* payload);
    
    /// Send a message immediately to MOST asynchronous channel with MOST HIGH protocol using MOST High.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] handle Handle of tx message.
    /// \param[in] targetAddress Target address.
    /// \param[in] fblockID FBlock identifier.
    /// \param[in] instanceID Instance identifier.
    /// \param[in] functionID Function identifier.
    /// \param[in] opType Operation type.
    /// \param[in] minDelay Minimum delay for speed regulation.
    /// \param[in] maxDelay Maximum delay for speed regulation.
    /// \param[in] priority Priority value used for this message.
    /// \param[in] options Options for the transmission.
    /// \param[in] totalPacketLength Length of complete package.
    /// \param[in] payloadLength Length of payload.
    /// \param[in] payload Payload data.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note This function is only for usage of MOST High bigger than 64kByte.
    /// \note The maximum length of message is 65535 bytes.
    /// \ingroup MOST
    static int SendMostHighExtendedMessage(IABI* pAbi, unsigned short handle, unsigned short targetAddress, BYTE fblockID, BYTE instanceID, unsigned short functionID, BYTE opType, unsigned short minDelay, unsigned short maxDelay, BYTE priority, MostMhpOptions options, unsigned int totalPacketLength, unsigned short payloadLength, BYTE* payload);
    
    /// Send a message immediately to MOST asynchronous channel with MOST HIGH protocol using MOST High bigger 64kByte.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] handle Handle of tx message.
    /// \param[in] targetAddress Target address.
    /// \param[in] fblockID FBlock identifier.
    /// \param[in] instanceID Instance identifier.
    /// \param[in] functionID Function identifier.
    /// \param[in] opType Operation type.
    /// \param[in] totalPacketLength Length of complete package.
    /// \param[in] payloadLength Length of payload.
    /// \param[in] payload Payload data.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note This function is only for usage of MOST High bigger than 64kByte.
    /// \note The maximum length of message is 65535 bytes.
    /// \ingroup MOST
    static int SendMostHighExtMessage(IABI* pAbi, unsigned short handle, unsigned short targetAddress, BYTE fblockID, BYTE instanceID, unsigned short functionID, BYTE opType, unsigned int totalPacketLength, unsigned short payloadLength, BYTE* payload);
    
    /// Sends a message over MOST MEP.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] handle Handle of tx message.
    /// \param[in] retryCount Retry count of tx message.
    /// \param[in] priority Priority of tx message.
    /// \param[in] payload Message to be send.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note Only available for MOST 150.
    /// \note The message have to be a valid ethernet package.
    /// \ingroup MOST
    static int SendMostMepExtMessage(IABI* pAbi, unsigned short handle, BYTE retryCount, BYTE priority, unsigned short payloadLength, BYTE* payload);
    
    /// Sends a message over MOST MEP.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] handle Handle of tx message.
    /// \param[in] payload Message to be send.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note Only available for MOST 150.
    /// \note The message have to be a valid ethernet package.
    /// \ingroup MOST
    static int SendMostMepMessage(IABI* pAbi, unsigned short handle, unsigned short payloadLength, BYTE* payload);
    
    /// Sets the boundary of the MOST bus.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] boundary New boundary value (SBC).
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int SetBoundary(IABI* pAbi, BYTE boundary);
    
    /// Sets CAN TP pattern to fill CAN telegrams up to 8 bytes.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] pattern Content of fill bytes.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup CANTP
    static int SetCanTpPattern(IABI* pAbi, BYTE pattern);
    
    /// Initializes global parameters for CAN TP Rx.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] blockSize Flow Control block size.
    /// \param[in] separationTime Minimum time gap between the transmission of CF.
    /// \param[in] consecutiveFrameTimeout Timeout for the next consecutive frame.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup CANTP
    static int SetCanTpRxConfig(IABI* pAbi, BYTE blockSize, BYTE separationTime, unsigned short consecutiveFrameTimeout);
    
    /// Initializes global parameters for CAN TP Tx.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] flowControlTimeout Defines timeout interval for flow control.
    /// \param[in] clearToSendTimeout Defines minimum time between two tx messages.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup CANTP
    static int SetCanTpTxConfig(IABI* pAbi, unsigned short flowControlTimeout, unsigned short clearToSendTimeout);
    
    /// Set the device mode of the INIC. The network is not started.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] deviceMode Device mode that is used.
    /// \param[in] options Options that are attended at startup (See DeviceModeOptions).
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note Multiple options can be combined.
    /// \ingroup MOST
    static int SetDeviceMode(IABI* pAbi, MostMode deviceMode, DeviceModeOptions options);
    
    /// Sets the MOST group address address of device.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] groupAddress New group address of device.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int SetGroupAddr(IABI* pAbi, unsigned short groupAddress);
    
    /// Sets the logical MOST node address of device.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] nodeAddress New logical node address of device.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int SetLogicalNodeAddr(IABI* pAbi, unsigned short nodeAddress);
    
    /// Set the count of high level retries. These retries are visible on Spy and Node (TNAK).
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] retryCount Count of high level retries.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int SetMostControlChannelHighLevelRetry(IABI* pAbi, BYTE retryCount);
    
    /// Set the count of low level retries. These retries are only visible on Spy.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] retryCount Count of low level retries.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int SetMostControlChannelLowLevelRetry(IABI* pAbi, BYTE retryCount);
    
    /// Set the count of mid level retries. These retries are only visible on Spy.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] retryCount Count of mid level retries.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int SetMostControlChannelMidLevelRetry(IABI* pAbi, BYTE retryCount);
    
    /// Set the frequency of MOST.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] frequency Frequency of MOST.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note Only available for MOST 25 and MOST 50.
    /// \ingroup MOST
    static int SetMostFrequency(IABI* pAbi, MostSamplingFrequency frequency);
    
    /// Set the MOST illumination level.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] level MOST illumination level.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int SetMostIlluminationLevel(IABI* pAbi, MostIlluminationLevel level);
    
    /// Set MAC address of MOST node.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] macAddress MAC address that should be set in MOST node.
    /// \param[in] isPersistent TRUE if MAC address should be available after reset otherwise FALSE.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note Only available for MOST 150.
    /// \ingroup MOST
    static int SetMostMacAddress(IABI* pAbi, unsigned long long macAddress, bool isPersistent);
    
    /// Set the mute state of the selected audio connector.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] connector The selected audio connector type.
    /// \param[in] isInput true if input connector should be modified, otherwise output connector is modified.
    /// \param[in] mute Mute if true, otherwise unmute.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int SetMuteState(IABI* pAbi, AudioConnectorType connector, bool isInput, bool mute);
    
    /// Sets a software or hardware property by a given key/value pair.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] key Selected key.
    /// \param[in] newValue Value that should be set.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup EXT
    static int SetParameter(IABI* pAbi, char* key, char* newValue);
    
    /// Sets the state of the relay.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] switchOn State of relay. TRUE means close and FALSE means open.
    /// \param[out] result Timestamp of action.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup HW
    static int SetRelay(IABI* pAbi, bool switchOn, unsigned long long& result);
    
    /// Sets the actual timestamp. The timestamp is represented in steps of 100 nanoseconds.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] Actual timestamp.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup HW
    static int SetTimeStamp(IABI* pAbi, unsigned long long timestamp);
    
    /// Set the mode of the hardware synchronisation unit.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] mode Timing mode of hardware synchronisation unit.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup HW
    static int SetTimingSynchronisationMode(IABI* pAbi, TimingSynchronisationMode mode);
    
    /// Sets the voltage level of trigger line.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] isActive TRUE means active (voltage low), FALSE means inactive (voltage high).
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup HW
    static int SetTriggerLevel(IABI* pAbi, bool isActive);
    
    /// Enables/Disables transmission of payload in tx events.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bus Bus that is selected for this command.
    /// \param[in] isEnabled TRUE if payload is not transmitted on tx events otherwise FALSE.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup OBJ
    static int SetTxPayloadFilter(IABI* pAbi, Bus bus, bool isEnabled);
    
    /// Set the volume of the selected audio connector.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] connector The selected audio connector type.
    /// \param[in] isInput true if input connector should be modified, otherwise output connector is modified.
    /// \param[in] modifyLeft Modify left channel.
    /// \param[in] modifyRight Modify right channel.
    /// \param[in] volume Attenuation(-)/Gain(+) in db.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int SetVolume(IABI* pAbi, AudioConnectorType connector, bool isInput, bool modifyLeft, bool modifyRight, int volume);
    
    /// Shutdown the MOST network. The INIC is staying in the current mode.
    /// \param[in] pAbi Used IABI instance.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note This function could only be called if the device is in a master mode.
    /// \ingroup MOST
    static int ShutdownMost(IABI* pAbi);
    
    /// Starts test or one transmitting or receiving rule.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] handle Handle of object you want to start.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup OBJ
    static int Start(IABI* pAbi, unsigned short handle);
    
    /// Starts the CAN channel with selected transceiver type and baud rate.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bus Selected channel.
    /// \param[in] transceiverType Selected transceiver type.
    /// \param[in] speed Selected baud rate.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup CAN
    static int StartCan(IABI* pAbi, Bus bus, CanTransceiver transceiverType, CanSpeed speed);
    
    /// Starts the CAN channel with selected transceiver type and with individual baud rate.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bus Selected channel.
    /// \param[in] transceiverType Selected transceiver type.
    /// \param[in] brp Prescaler value to divide base frequency.
    /// \param[in] tseg1 Time segment 1 value.
    /// \param[in] tseg2 Time segment 2 value.
    /// \param[in] sjw Synchronization jump width.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note The base frequency for MOCCA compact 2.x is for CAN1-4: 16MHz, CAN5-6: 25MHz. For MOCCA compact 3.x all CANs have the base frequency 16Mhz.
    /// \ingroup CAN
    static int StartCanExt(IABI* pAbi, Bus bus, CanTransceiver transceiverType, BYTE brp, BYTE tseg1, BYTE tseg2, BYTE sjw);
    
    /// Starts the FlexRay channel with an individual configuration file.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] blob Configuration file.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup FR
    static int StartFlexRay(IABI* pAbi, unsigned short payloadLength, BYTE* blob);
    
    /// Starts the Lin channel with an individual configuration file.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bus Selected channel.
    /// \param[in] blob Configuration file.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note A LIN channel can only be used as node or spy!
    /// \ingroup LIN
    static int StartLin(IABI* pAbi, Bus bus, unsigned short payloadLength, BYTE* blob);
    
    /// Starts LIN Spy channel.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bus Selected channel.
    /// \param[in] baudrate Baudrate that runs in the LIN master.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note A LIN channel can only be used as node or spy!
    /// \ingroup LIN
    static int StartLinSpy(IABI* pAbi, Bus bus, unsigned int baudrate);
    
    /// Starts the MOST channel.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] mode Selected MOST mode.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int StartMost(IABI* pAbi, MostMode mode);
    
    /// Startup the network with the preselected device mode.
    /// \param[in] pAbi Used IABI instance.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note In case of slave mode a wakeup is done.
    /// \note A bypass could not make a startup.
    /// \ingroup MOST
    static int StartupMost(IABI* pAbi);
    
    /// Stops test or one transmitting or receiving rule.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] handle Handle of object you want to stop.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup OBJ
    static int Stop(IABI* pAbi, unsigned short handle);
    
    /// Stops the CAN channel.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bus Selected channel.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup CAN
    static int StopCan(IABI* pAbi, Bus bus);
    
    /// Stops the FlexRay channel.
    /// \param[in] pAbi Used IABI instance.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup FR
    static int StopFlexRay(IABI* pAbi);
    
    /// Stops the LIN channel.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bus Selected channel.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note A LIN channel can only be used as node or spy!
    /// \ingroup LIN
    static int StopLin(IABI* pAbi, Bus bus);
    
    /// Stops LIN Spy channel.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bus Selected channel.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note A LIN channel can only be used as node or spy!
    /// \ingroup LIN
    static int StopLinSpy(IABI* pAbi, Bus bus);
    
    /// Stops the MOST channel.
    /// \param[in] pAbi Used IABI instance.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int StopMost(IABI* pAbi);
    
    /// Gets the device mode of StressNIC.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result Device mode.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int StressNicDeviceModeGet(IABI* pAbi, StressNicMode& result);
    
    /// Sets the device mode of StressNIC.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] mode Device mode.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int StressNicDeviceModeSet(IABI* pAbi, StressNicMode mode);
    
    /// Sends a set command to the StressNIC.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] fblockId FBlock identifier of command.
    /// \param[in] functionId Function identifier of command.
    /// \param[in] payload Payload of function.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int StressNicFunctionSet(IABI* pAbi, BYTE fblockId, unsigned short functionId, unsigned short payloadLength, BYTE* payload);
    
    /// Gets the group address of StressNIC.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result Group address.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int StressNicGroupAddressGet(IABI* pAbi, unsigned short& result);
    
    /// Sets the group address of StressNIC.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] groupAddress Group address.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int StressNicGroupAddressSet(IABI* pAbi, unsigned short groupAddress);
    
    /// Gets the node address of StressNIC.
    /// \param[in] pAbi Used IABI instance.
    /// \param[out] result Node address.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int StressNicNodeAddressGet(IABI* pAbi, unsigned short& result);
    
    /// Sets the node address of StressNIC.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] nodeAddress Node address.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int StressNicNodeAddressSet(IABI* pAbi, unsigned short nodeAddress);
    
    /// Sends a message from the StressNic.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] deviceId Target device id.
    /// \param[in] fblockId FBlock id.
    /// \param[in] instance Instance id.
    /// \param[in] functionId Function identifier of command.
    /// \param[in] opType Operation type.
    /// \param[in] payload Payload of function.
    /// \param[out] result There is no indication if the message has been send successful or not.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int StressNicSendMessage(IABI* pAbi, unsigned short deviceId, BYTE fblockId, BYTE instance, unsigned short functionId, BYTE opType, unsigned short payloadLength, BYTE* payload);
    
    /// Switches the device into bypass mode so that is not participating in MOST network.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bypassMode Selected mode.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \ingroup MOST
    static int SwitchBypassMode(IABI* pAbi, BypassMode bypassMode);
    
    /// Writes a binary startup configuration in to the EEPROM of the device. This configuration will be executed every time the device is powered on.
    /// \param[in] pAbi Used IABI instance.
    /// \param[in] bus Bus which the startup configuration belongs to.
    /// \param[in] configuration Bus configuration as binary stream.
    /// \return Status message if operation succeeded. For more informations see \ref TMErrorCode.
    /// \note Configuration FlexRay: Binary output from the K2L FlexRay Toolchain.
    /// \note Configuration LIN: Binary output from the K2L LIN Toolchain.
    /// \ingroup HW
    static int WriteBusConfiguration(IABI* pAbi, Bus bus, unsigned short payloadLength, BYTE* configuration);
    
    
};
        }
    }
}
//@}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// END OF FILE                                                                
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

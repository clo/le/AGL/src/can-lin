#pragma once

typedef enum 
{
    TestMaster = 0x01,
    Board = 0x02,
    NetServices = 0x03,
    StressNic = 0x04
} Interfaces;

typedef enum
{
    FunId__SelectGroup = 2,

    FunId__Start = 4,

    FunId__Stop = 6,

    FunId__Delete = 8,

    FunId__SetCanTpRxConfig = 10,
    FunId__GetCanTpRxConfig = 11,
    FunId__SetCanTpTxConfig = 12,
    FunId__GetCanTpTxConfig = 13,
    FunId__SetCanTpPattern = 14,
    FunId__GetCanTpPattern = 15,
    FunId__ClearCanTpPattern = 16,

    FunId__SendMessage = 20,
    FunId__SendMostHighExtMessage = 21,
    FunId__SendMostHighExtendedMessage = 22,
    FunId__SendMostMepMessage            = 23,
    FunId__SendMostMepExtMessage         = 24,
    FunId__EnableMepEthernetBridge       = 25,
    FunId__IsMepEthernetBridgeEnabled    = 26,
    FunId__SendCanTpMessage = 27u,
    FunId__EnableMepEthernetBridgeExtended = 28,

    FunId__AddCanTpTransmitter = 32,

    FunId__AddTxSweepSignal = 34,
    FunId__DeleteTxSweepSignals = 35,
    FunId__AddTransmitterEx = 36,
    FunId__ChangeCycleTime = 37,
    FunId__ChangeTransmitterData = 38,

    FunId__AddReceiver = 40,

    FunId__AddCanTpReceiver = 44,

    //FunId__AddLinReceiver = 46, //DEPRECATED

    FunId__WriteBusConfiguration = 50,
    FunId__ReadBusConfiguration = 51,
    //FunId__GetReceiverHandles = 52, //DEPRECATED
    //FunId__GetTransmitterHandles = 53, //DEPRECATED
    FunId__GetAllUsedHandles = 54,
    FunId__GetInformationOfObject = 55,

    FunId__GetInternalState = 100,
    FunId__SetTxPayloadFilter = 101,
    FunId__IsTxPayloadFilterEnabled = 102

    //TM__MIN_EVENT_ID = 128, //DEPRECATED
    //EventId__OnTxMessage = TM__MIN_EVENT_ID + 1, //DEPRECATED
    //EventId__OnRxMessage = TM__MIN_EVENT_ID + 2, //DEPRECATED
    //EventId__OnTxNAK = TM__MIN_EVENT_ID + 4, //DEPRECATED
    //EventId__OnTxFailed = TM__MIN_EVENT_ID + 5, //DEPRECATED
    //EventId__OnCanTpTxMessage = TM__MIN_EVENT_ID + 6, //DEPRECATED
    //EventId__OnCanTpRxMessage = TM__MIN_EVENT_ID + 7, //DEPRECATED
    //EventId__OnBusEvent = TM__MIN_EVENT_ID + 8, //DEPRECATED
    //EventId__OnInternalEvent = TM__MIN_EVENT_ID + 9, //DEPRECATED
    //EventId__OnGeneralEvent = TM__MIN_EVENT_ID + 10,//DEPRECATED
    //EventId__OnTxMessageExt = TM__MIN_EVENT_ID + 11, //DEPRECATED
    //EventId__OnRxMessageExt = TM__MIN_EVENT_ID + 12 //DEPRECATED
} TestMasterFunctions;

typedef enum
{
    FunId__GetFirmwareVersion = 0x01,
    FunId__GetHardwareVersion = 0x03,
    FunId__GetSerialNumber = 0x05,
    FunId__GetRunningTime = 0x07,
    FunId__RebootFirmware = 0x09,
    FunId__SetRelay = 0x0B,
    FunId__IsRelayClosed = 0x0C,
    FunId__GetHardwareRevision = 0x0D,

    //FunId__SetCanSpeed = 0x0F,//DEPRECATED

    //FunId__ReInitCAN = 0x11,//DEPRECATED

    FunId__GetCurrentConfiguration = 0x13,

    FunId__FlashConfiguration = 0x15,

    FunId__ResetTimeStamp = 0x17,

    FunId__GetTimeStamp = 0x19,

    FunId__SetTimeStamp = 0x1A,

    FunId__Identify = 0x1B,
    
    FunId__StartFlexRay = 0x1D,

    FunId__StopFlexRay = 0x1F,
    
    FunId__GetIpAddress = 0x21,

    FunId__GetNetMask = 0x23,

    FunId__GetGwAddress = 0x25,

    FunId__SetIpAddress = 0x27,

    FunId__SetNetMask = 0x29,

    FunId__SetGwAddress = 0x2B,
    
    //FunId__SetCanTransceiver = 0x2F,//DEPRECATED
    
    FunId__StartLin = 0x31,

    FunId__StopLin = 0x33,
    FunId__GetLinConfigurationName = 0x34,
    FunId__GetInformation = 0x35,
    FunId__SendCanRemoteFrame = 0x36,
    FunId__StartCan = 0x37,
    FunId__StartCanExt = 0x39,

    FunId__StopCan = 0x3B,
    FunId__GetCanBaseFrequency = 0x3C,
    FunId__GetExtendedBaudrate = 0x3D,
    FunId__GetParameterKeys = 0x3E,
    FunId__GetParameterValues = 0x3F,
    FunId__GetParameter = 0x40,
    FunId__SetParameter = 0x41,    
    FunId__GetCanTransceiver = 0x42,
    FunId__GetCanSpeed = 0x43,
    FunId__IsFlexRaySynchronized = 0x44,
    FunId__IsLinMaster = 0x45,
    FunId__GetLinSpeed = 0x46,
    FunId__IsBusStarted = 0x47,
    FunId__StartLinSpy = 0x48,
    FunId__StopLinSpy = 0x49,

    FunId__SetTimingSynchronisationMode = 0x50,
    FunId__GetTimingSynchronisationMode = 0x51,
    FunId__IsCanTransceiverSupported = 0x52,
    FunId__GetSupportedCanSpeeds = 0x53,
    FunId__GetSupportedCanTransceivers = 0x54,
    FunId__SendEchoCommand = 0x55,
    FunId__GetLinBusCount = 0x56,
    FunId__GetCanBusCount = 0x57,
    FunId__GetFlexRayBusCount = 0x58,
    FunId__EnableFlexRayTx = 0x59,
    FunId__IsFlexRayTxEnabled = 0x5A,

    FunId__SendRelativeTriggerSequence = 0x60,
    FunId__SendAbsoluteTriggerSequence = 0x61,
    FunId__SetTriggerLevel = 0x62,
    FunId__GetTriggerLevel = 0x63,
    FunId__SetTimeout = 0x64,
} BoardFunctions;

typedef enum
{
    FunId__GetLogicalNodeAddr = 0x07,

    FunId__StartMost = 0x09,
    FunId__GetDeviceMode = 0x0A,
    FunId__StopMost = 0x0B,

    FunId__SetLogicalNodeAddr = 0x0D,
    FunId__GetMPR = 0x0E,
    FunId__GetNPR = 0x0F,
    
    FunId__SwitchBypassMode = 0x11,
    
    FunId__SetGroupAddr = 0x13,

    FunId__GetGroupAddr = 0x15,

    FunId__SetBoundary = 0x17,

    FunId__GetBoundary = 0x19,

    FunId__IsNetOn = 0x30,
    FunId__IsStableLock = 0x31,
    FunId__SetMostIlluminationLevel = 0x32,
    FunId__GetMostIlluminationLevel = 0x33,
    FunId__HasAllocationTableSupport = 0x34,
    FunId__HasMultipleStreamInSupport = 0x35,
    FunId__HasRedirectStreamInToOutSupport = 0x36,

    FunId__MostAudioAllocate = 0x40,
    FunId__MostAudioTxAllocateConnect = 0x41,
    FunId__MostAudioTxConnect = 0x42,
    FunId__MostAudioTxDisconnect = 0x43,
    FunId__MostAudioRxConnect = 0x44,
    FunId__MostAudioRxDisconnect = 0x45,
    FunId__MostAudioRxConnectExt = 0x46,
    FunId__MostAudioTxAllocateConnectSync = 0x47,
    FunId__MostAudioTxDisconnectSync = 0x48,
    FunId__MostAudioRxConnectSync = 0x49,
    FunId__MostAudioRxDisconnectSync = 0x4A,
    FunId__GetMostAudioSupportedTxConnectors = 0x4B,
    FunId__SetVolume = 0x4C,
    FunId__GetVolume = 0x4D,
    FunId__GetSupportedVolumeRange = 0x4E,

    FunId__SetDeviceMode = 0x50,
    FunId__StartupMost = 0x51,
    FunId__ShutdownMost = 0x52,
    FunId__SetMostControlChannelLowLevelRetry = 0x53,
    FunId__GetMostControlChannelLowLevelRetry = 0x54,
    FunId__SetMostControlChannelMidLevelRetry = 0x55,
    FunId__GetMostControlChannelMidLevelRetry = 0x56,
    FunId__SetMostControlChannelHighLevelRetry = 0x57,
    FunId__GetMostControlChannelHighLevelRetry = 0x58,
    FunId__SetMostFrequency = 0x59,
    FunId__GetMostFrequency = 0x5A,
    FunId__GetMostAllocationTable = 0x5B,
    FunId__GetMostAudioRxLabels = 0x5C,
    FunId__GetMostAudioTxLabels = 0x5D,
    FunId__SetMostMacAddress = 0x5E,
    FunId__GetMostMacAddress = 0x5F,
    FunId__IsMostNodeSupported = 0x60,
    FunId__IsMostSpySupported = 0x61,
    FunId__IsMostStressSupported = 0x62,
    FunId__GetMostSpeedGrade = 0x63,

    FunId__EnableMostSpy = 0x70,
    FunId__MostSpyEnabled = 0x71,
    FunId__GetMostShutdownReason = 0x72,
    FunId__ResetMostShutdownReason = 0x73,
    FunId__GetMuteState = 0x74,
    FunId__SetMuteState = 0x75,
} NetServicesFunctions;

typedef enum 
{
    FunId__StressNicDeviceModeSet = 1,
    FunId__StressNicDeviceModeGet = 2,
    FunId__StressNicNodeAddressSet = 3,
    FunId__StressNicNodeAddressGet = 4,
    FunId__StressNicGroupAddressSet = 5,
    FunId__StressNicGroupAddressGet = 6,
    FunId__StressNicFunctionSet = 7,
    FunId__StressNicFunctionGet = 8,
    FunId__StressNicFunctionGetExtended = 9,
    FunId__StressNicSendMessage = 10
} StressNicFunctions;
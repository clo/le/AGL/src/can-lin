#!/bin/sh

export CROSS_COMPILE=$AGL_TOP/build/tmp/sysroots/x86_64-linux/usr/bin/arm-poky-linux-gnueabi/arm-poky-linux-gnueabi-
export ARCH=arm
export CC=${CROSS_COMPILE}gcc
export KDIR=$AGL_TOP/build/tmp/work/porter-poky-linux-gnueabi/linux-renesas/3.10+gitb8ca8c397343f4233f9f68fc3a5bf8e1c9b88251-r0/sysroot-destdir/usr/src/kernel
export CXX_FLAGS="-D__ARM_PCS_VFP -mfloat-abi=hard"
export BOOST_ROOT="$AGL_TOP/build/tmp/work/cortexa15hf-vfp-neon-poky-linux-gnueabi/boost/1.56.0-r0"
export LIBRARY_PATH=""
export CPATH=""
export INCLUDE_PATH="" 
export LIBRARIES_FLAGS="-lm -lz -lcrypt"



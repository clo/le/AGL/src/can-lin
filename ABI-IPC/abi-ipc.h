//
//  abi-ipc.h
//  AmbientLight
//
//  Created by Thorsten Kummermehr on 10/9/13.
//
//

#ifndef AmbientLight_abi_ipc_h
#define AmbientLight_abi_ipc_h

#include "K2LABI.h"

#include <pthread.h>

namespace K2L
{
    namespace Automotive
    {
        namespace ABI_IPC
        {

            class K2LABI_API AbiUsbIpc :
            public K2L::Automotive::ABI::AbiIpcDevice
            {
            public:
                AbiUsbIpc();
                virtual
                ~AbiUsbIpc();
                virtual bool Connect( const char *cdevRx, const char *cdevTx );
                virtual bool Connect( const char *cdevRxTx );
                virtual void Disconnect( void );
                virtual bool IsConnected();
                virtual int Send( const BYTE *data, DWORD length );
                virtual void SetReceivePriority( int prio );
                virtual void SetReceiveCallback( ABI::AbiIpcDevice_RxCB_t receiveCB, void *cbArg );

            protected:
                static void *ReceiveThread( void *param );

                bool _isConnected;
                bool _receiveThreadIsRunning;
                ABI::AbiIpcDevice_RxCB_t *_receiveCB;
                void *_receiveCBArgument;
                pthread_t _receiveThread;
                int _rxHandle;
                int _txHandle;
                uint8_t receiveBuffer[3000];
            };
        }
    }
}


#endif
